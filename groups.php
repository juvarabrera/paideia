<?php
$toRoot = "";
require_once('library/Config.php');
if($status == "Login") {
	header("Location: index.php");
}
$bTitle = "Schoolbook - De La Salle University Dasmari&ntilde;as";
if(isset($_GET['id']) && isset($_GET['show'])) {
	$show = $_GET['show'];
	$id = $_GET['id'];
	if($library['group']->CheckIfGroupExist($id)) {
		if($library['group']->IsMember($loggedUser, $id)) {
			$isGuest = false;
			$userType_Group = $library['group']->GetUserType($loggedUser, $id);
		} else {
			$isGuest = true;
		}
		if($isGuest) {
			$pages = array('People');
			if(!in_array($show, $pages))
				header("Location: groups.php?id=$id");
		} else {
			$pages = array('Resources', 'People', 'Calendar', 'Request');
			// 0 - member
			// 1 - admin
			// 2 - both
			$page_authorization = array(
				"Resources" => 2,
				"People" => 2,
				"Calendar" => 2,
				"Request" => 1
			);
			if(in_array($show, $pages)) {
				if($page_authorization[$show] == 0) {
					if($userType_Group != "Member")
						header("Location: groups.php?id=$id");
				} elseif ($page_authorization[$show] == 1) {
					if($userType_Group != "Admin")
						header("Location: groups.php?id=$id");
					if($library['group']->GetGroupType($id) != "Open" && $show == "Request")
						header("Location: groups.php?id=$id");
				} elseif ($page_authorization[$show] == 2) {
					//
				}
			} else {
				header("Location: groups.php?id=$id");
			}
		}
	} else {
		header("Location: groups.php");
	}
	$bTitle = $library['group']->GetGroupName($id);
} elseif(isset($_GET['id'])) {
	$id = $_GET['id'];
	if($library['group']->CheckIfGroupExist($id)) {
		if($library['group']->IsMember($loggedUser, $id)) {
			$isGuest = false;
			$userType_Group = $library['group']->GetUserType($loggedUser, $id);
			if(isset($_GET['post']))  {
				$postID = $_GET['post'];
				if(!$library['group']->CheckIfGroupPost($id, $postID)) {
					header("Location: groups.php?id=$id");
				}
			}
		} else {
			$isGuest = true;
		}
	} else {
		header("Location: groups.php");
	}
	$bTitle = $library['group']->GetGroupName($id);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $bTitle; ?></title>
	<link rel="stylesheet" href="styles/skin/<?php echo $skin; ?>/style.php">
	<?php require_once('scripts/skin/'.$skin.'/scripts.php'); ?>
	<?php
	if($status == "LoggedIn") 
		if(file_exists("users/$loggedUser/cover.jpg"))
			echo '<style type="text/css">#bodyBg {background-image: url(users/'.$loggedUser.'/cover.jpg)}</style>';
	?>
</head>
<body onload="init();">
<div id="bodyBg"></div>
<div id="container">
	<div id="main">
		<?php 
		if(isset($_GET['id']) && isset($_GET['show'])) {
			$show = $_GET['show'];
			require_once($toRoot.'template/Groups/body_GroupsPage.php');
		} elseif(isset($_GET['id'])) {
			require_once($toRoot.'template/Groups/body_GroupsIndex.php');
		} else {
			require_once($toRoot.'template/Groups/body_Groups.php');
		}
		?>
	</div>
</div>
<div id="top">
	<div class="base">
		<a href="index.php" class="logo"></a>
		<?php
		require_once('template/top_'.$status.'.php');
		?>
	</div>
</div>
<?php /*
if(isset($userType_Group)) {
	if($userType_Group == "Admin") {
		require_once($toRoot.'template/Groups/popup_AddFolder.php');
		require_once($toRoot.'template/Groups/popup_AddFile.php');
		require_once($toRoot.'template/Groups/popup_EditGroup.php');
		require_once($toRoot.'template/Groups/popup_AddEvent.php');
	} elseif($userType_Group == "Member") {
		//
	}
}
require_once($toRoot.'template/Groups/popup_CreateGroup.php');
require_once($toRoot.'template/Groups/popup_JoinGroup.php'); */
require_once($toRoot.'popup.php'); 
if(isset($_GET['id']))
echo '<script>

$(function(){
	$("#txtSearch").val("'.$library['group']->GetGroupName($_GET['id']).'");
});

</script>';
?>
</body>
</html>