<script>
$(document).ready(function() {
	// save post to database
	$('#btnPost').click(function() {
		$text = $('#txtStatus').val();
		if($text != "") {
			$('#btnPost').css({
				"display": "none"
			});
			$('#postLoadingGif').css({
				"display": "block"
			});
			<?php 
			if(isset($viewProfileOf)) {
			?>
			$.ajax({
				type: "POST",
				cache: false,
				url: "process.php?action=savepost",
				data: {postMsg: $text, fromwhat: 'Newsfeed'<?php if($viewProfileOf != $loggedUser) echo ", id: $viewProfileOf"; ?>},
				success: function(html) {
					$postSuccess = 1;
					$("#btnRefreshNewsfeed").css({
						"height": "0px",
						"padding": "0px 0px",
						"margin-bottom": "00px",
					});
					$('#checkIfError').html(html);
				}
			});
			<?php
			} else {
			?>
			$.ajax({
				type: "POST",
				cache: false,
				url: "process.php?action=savepost",
				data: {postMsg: $text, fromwhat: 'Newsfeed'},
				success: function(html) {
					$postSuccess = 1;
					$("#btnRefreshNewsfeed").css({
						"height": "0px",
						"padding": "0px 0px",
						"margin-bottom": "00px",
					});
					$('#checkIfError').html(html);
				}
			});
			<?php
			}
			?>
		} else {
			$('#msgPost_Error').css({
				"height": "auto",
				"padding": "10px 10px"
			})
			setTimeout(function() {
				$('#msgPost_Error').css({
				"height": "0px",
				"padding": "0px 10px"
				});
			}, 3000);
		}
	});
});
</script>
			<div class="content">
				<table class="title"><tr>
					<td>Post</td>
					<td></td>
				</tr></table>
				<span class="error" id="msgPost_Error">Cannot submit post if status is empty</span>
				<textarea class="transtext resizabletext" id="txtStatus" placeholder="Post a status"></textarea>
				<table class="postform"><tr valign="top">
					<td><div class="profpic" style="background-image: url(<?php echo $library['user']->GetProfilePicture($loggedUser, 50); ?>);"></div><span><?php echo $library['user']->GetName($loggedUser); ?></span></td>
					<td width="70px" align="right"><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" id="postLoadingGif" class="loadingGif" style="display: none;"><input type="submit" id="btnPost" value="Post"></td>
				</tr></table>
			</div>