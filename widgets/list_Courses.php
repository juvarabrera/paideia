<?php
$popup["popup_CreateCourse"] = true;
$popup["popup_EnrollCourse"] = true;
?>
			<div class="content">
				<table class="title"><tr>
					<td><a href="courses.php">Courses</a></td>
					<td></td>
				</tr></table>
				<hr>
				<ul class="contentlist">
					<?php
					$row = $library['course']->GetAllCoursesHandledBy($loggedUser);
					$max = 5;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$numStudents = $library['course']->GetNumberOfStudentsIn($row[$i]);
						$numProfessors = $library['course']->GetNumberOfProfessorsIn($row[$i]);
						$dp = $library['course']->GetProfilePicture($row[$i], 50);
						if($numStudents == 0 || $numStudents == 1)
							$numStudents .= " student";
						else
							$numStudents .= " students";
						if($numProfessors == 0 || $numProfessors == 1)
							$numProfessors .= " professor";
						else
							$numProfessors .= " professors";
						$coursename = $library['course']->GetCourseName($row[$i]);
						if(strlen($coursename) >= 15)
							$coursename = substr($coursename, 0, 15).'...';
						echo '<a href="courses.php?id='.$row[$i].'"><div class="profpic" style="background-image: url('.$dp.')"></div><b class="color">'.$coursename.'</b><br><small><i>'.$numProfessors.', '.$numStudents.'</i></small></a>';
					}
					if(sizeof($row) > $max)
						echo '<a href="courses.php" align="right"><small>See all courses...</small></a>';
					if(sizeof($row) == 0)
						echo '<a>No courses to display.</a>';
				echo '
				</ul>';
					if($userType == "Faculty") {
					?>
					<div class="bottommenu">
						<script>
						$(document).ready(function() {
							$('#btnPopup_CreateCourse').click(function() {
								showPopup();
								$showPopup = "CreateCourse";
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=showpopup",
									data: {popup: 'course_'+$showPopup},
									success: function(html) {
										$('#Popup').html(html);
										$heightPopup = $('div#popup_'+$showPopup).height()+5;
										$('#Popup').css({
											"width": "450px",
											"height": ($heightPopup)+"px",
											"margin-left": "-225px",
											"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
										});
										popup = 1;
									}
								});
							});
						});
						</script>
						<a class="add" title="Create a course" id="btnPopup_CreateCourse">Create</a>
						<script>
						$(document).ready(function() {
							$('#btnPopup_EnrollCourse').click(function() {
								showPopup();
								$showPopup = "EnrollCourse";
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=showpopup",
									data: {popup: 'course_'+$showPopup},
									success: function(html) {
										$('#Popup').html(html);
										$heightPopup = $('div#popup_'+$showPopup).height()+5;
										$('#Popup').css({
											"width": "450px",
											"height": ($heightPopup)+"px",
											"margin-left": "-225px",
											"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
										});
										popup = 1;
									}
								});
							});
						});
						</script>
						<a id="btnPopup_EnrollCourse" class="add" title="Enroll a course">Enroll</a>
					</div>
					<?php
					} elseif ($userType == "Student") {
					?>
					<div class="bottommenu">
						<script>
						$(document).ready(function() {
							$('#btnPopup_EnrollCourse').click(function() {
								showPopup();
								$showPopup = "EnrollCourse";
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=showpopup",
									data: {popup: 'course_'+$showPopup},
									success: function(html) {
										$('#Popup').html(html);
										$heightPopup = $('div#popup_'+$showPopup).height()+5;
										$('#Popup').css({
											"width": "450px",
											"height": ($heightPopup)+"px",
											"margin-left": "-225px",
											"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
										});
										popup = 1;
									}
								});
							});
						});
						</script>
						<a id="btnPopup_EnrollCourse" class="add" title="Enroll a course">Enroll</a>
					</div>
					<?php
					}
					?>
			</div>