<script>
$(document).ready(function() {
	// save post to database
	$('#btnPost').click(function() {
		$text = $('#txtStatus').val();
		$('#btnPost').css({
			"display": "none"
		});
		$('#postLoadingGif').css({
			"display": "block"
		});
		if($text != "") {
			$.ajax({
				type: "POST",
				cache: false,
				url: "process.php?action=savepost",
				data: {postMsg: $text, fromwhat: 'Group', id: <?php echo $id; ?>},
				success: function(data) {
					$postSuccess = 1;
					$("#btnRefreshNewsfeed").css({
						"height": "0px",
						"padding": "0px 0px",
						"margin-bottom": "00px",
					});
				}
			});
		} else {
			$('#msgPost_Error').css({
				"height": "auto",
				"padding": "10px 10px"
			})
			setTimeout(function() {
				$('#msgPost_Error').css({
				"height": "0px",
				"padding": "0px 10px"
				});
			}, 3000);
		}
	});
});
</script>
<div class="content">
	<table class="title"><tr>
		<td>Post</td>
		<td></td>
	</tr></table>
	<span class="error" id="msgPost_Error">Cannot submit post if status is empty</span>
	<textarea class="transtext resizabletext" id="txtStatus" placeholder="Post a status"></textarea>
	<table class="postform"><tr valign="top">
		<td><div class="profpic" style="background-image: url(<?php echo $library['user']->GetProfilePicture($loggedUser, 50); ?>);"></div><span><?php echo $library['user']->GetName($loggedUser); ?></span></td>
		<td width="70px" align="right"><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" id="postLoadingGif" class="loadingGif" style="display: none;"><input type="submit" id="btnPost" value="Post"></td>
	</tr></table>
</div>

<script>

// loads newsfeed
$(document).ready(function() {
	$.ajax({
		type: "POST",
		url: "process.php?action=refreshnewsfeed",
		cache: false,
		data: {fromwhat: 'Group', id: <?php echo $id; ?>},
		success: function(html){
			$(".newsfeedContainer").html(html);
		}
	});
});

// automatically load newsfeed when postSuccess = 1, checks every second
$postSuccess = 0;
$(document).everyTime(1000, function() {
	if($postSuccess == 1) {
		$.ajax({
			type: "POST",
			url: "process.php?action=refreshnewsfeed",
			cache: false,
			data: {fromwhat: 'Group', id: <?php echo $id; ?>},
			success: function(html){
				$(".newsfeedContainer").html(html);
				$("#txtStatus").val('');
				$("#btnRefreshNewsfeed").css({
					"height": "0px",
					"padding": "0px 0px",
					"margin-bottom": "00px",
				});
				init();
				$('#btnPost').css({
					"display": "block"
				});
				$('#postLoadingGif').css({
					"display": "none"
				});
			}
		});
		$postSuccess = 0;
	}
})

$(document).ready(function(){
	
	// refresh newsfeed 
	$('#btnRefreshNewsfeed').click(function() {
		$.ajax({
			type: "POST",
			url: "process.php?action=refreshnewsfeed",
			cache: false,
			data: {fromwhat: 'Group', id: <?php echo $id; ?>},
			success: function(html){
				$(".newsfeedContainer").html(html);
				$("#btnRefreshNewsfeed").css({
					"height": "0px",
					"padding": "0px 0px",
					"margin-bottom": "00px",
				});
			},
			error: function() {
				$('#msgPost_Error').css({
					"height": "auto",
					"padding": "10px 10px"
				})
				setTimeout(function() {
					$('#msgPost_Error').css({
					"height": "0px",
					"padding": "0px 10px"
					});
				}, 3000);
			}
		});
	});
});

</script>
<div class="newsfeedContainer">
	<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" alt="" class="loadingGif"></center>
</div>