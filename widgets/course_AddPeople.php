			<script>
			$(document).ready(function () {
				var xhr;
				$('#txtSearchToInvite').keyup(function(event) {
					$text = $('#txtSearchToInvite').val();
					if($text != "") {
						$('#listInvitationResults').html('<center><br><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></center>');
						if(xhr && xhr.readyState != 4) {
							xhr.abort();
						}
						xhr = $.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=getpeopletoinvite",
							data: {id: <?php echo $id; ?>, fromwhat: 'Course', q: $text},
							success: function(html) {
								$('#listInvitationResults').html(html);
							}
						});
					} else {
						$('#listInvitationResults').html('<a><small><i>You can invite other people to enroll this course.</i></small></a>');
					}
				});	
			});

			$(document).everyTime(1, function () {
				$text = $('#txtSearchToInvite').val();
				if($text == "") 
					$('#listInvitationResults').html('<a><small><i>You can invite other people to enroll this course.</i></small></a>');
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>Enroll People</td>
						<td></td>
					</tr>
				</table>
				<p class="default"><input type="text" class="transtext transwhitetext" id="txtSearchToInvite" placeholder="Search by name"></p>
				<hr>
				<ul class="contentlist" id="listInvitationResults">
					<a><small><i>You can invite other people to enroll this course.</i></small></a>
				</ul>
			</div>