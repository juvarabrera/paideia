			<div class="content">
				<table class="title"><tr>
					<td><a href="courses.php?id=<?php echo $id; ?>&show=People">People</a></td>
					<td></td>
				</tr></table>
				<hr>
				<p class="text"><b>Professors</b></p>
				<hr>
				<ul class="contentlist">
					<?php
					$row = $library['course']->GetAllProfessors($id);
					$max = 2;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$dp = $library['user']->GetProfilePicture($row[$i],50);
						$name = $library['user']->GetName($row[$i]);
						if(strlen($name) > 20)
							$name = substr($name, 0, 20).'...';
						echo '<a href="index.php?-_-='.$library['user']->GetUsername($row[$i]).'" class="small" title="'.$library['user']->GetName($row[$i]).'"><div class="profpic small" style="background-image: url('.$dp.');"></div><span>'.$name.'</span></a>';
					}
					if(sizeof($row) > $max) 
						echo '<a href="courses.php?id='.$id.'&show=People" align="right"><small>See all professors...</small></a>';
					if(sizeof($row) == 0) 
						echo '<a>There are no professors handling this course.</a>';
					?>
				</ul>
				<p class="text"><b>Students</b></p>
				<hr>
				<ul class="contentlist">
					<?php
					$row = $library['course']->GetAllStudents($id);
					$max = 2;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$dp = $library['user']->GetProfilePicture($row[$i],50);
						$name = $library['user']->GetName($row[$i]);
						if(strlen($name) > 20)
							$name = substr($name, 0, 20).'...';
						echo '<a href="index.php?-_-='.$library['user']->GetUsername($row[$i]).'" class="small" title="'.$library['user']->GetName($row[$i]).'"><div class="profpic small" style="background-image: url('.$dp.');"></div><span>'.$name.'</span></a>';
					}
					if(sizeof($row) > $max) 
						echo '<a href="courses.php?id='.$id.'&show=People" align="right"><small>See all students...</small></a>';
					if(sizeof($row) == 0) 
						echo '<a>There are no students enrolled.</a>';
					?>
				</ul>
			</div>