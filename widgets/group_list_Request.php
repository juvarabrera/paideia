			<div class="content">
				<table class="title"><tr>
					<td><a href="groups.php?id=<?php echo $id; ?>&show=Request">Requests</a></td>
					<td></td>
				</tr></table>
				<hr>
				<ul class="contentlist">
					<?php
					$row = $library['group']->GetRequest($id);
					$max = 3;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$dp = $library['user']->GetProfilePicture($row[$i],50);
						$name = $library['user']->GetName($row[$i]);
						if(strlen($name) > 20)
							$name = substr($name, 0, 20).'...';
						echo '<a href="index.php?-_-='.$library['user']->GetUsername($row[$i]).'" class="small" title="'.$library['user']->GetName($row[$i]).'"><div class="profpic" style="background-image: url('.$dp.');"></div><span>'.$name.'</span></a>';
					}
					if(sizeof($row) > $max) 
						echo '<a href="groups.php?id='.$id.'&show=Request" align="right"><small>'.(sizeof($row)-$max).' more requests...</small></a>';
					elseif (sizeof($row) != 0) {
						echo '<a href="groups.php?id='.$id.'&show=Request" align="right"><small>Accept or reject them here.</small></a>';
					}
					if(sizeof($row) == 0) 
						echo '<a>There are no requests.</a>';
					?>
				</ul>
			</div>