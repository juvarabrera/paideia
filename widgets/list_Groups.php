<?php
$popup["popup_CreateGroup"] = true;
$popup["popup_JoinGroup"] = true;
?>
			<div class="content">
				<table class="title"><tr>
					<td><a href="groups.php">Groups</a></td>
					<td></td>
				</tr></table>
				<hr>
				<ul class="contentlist">
					<?php
					$row = $library['group']->GetAllGroupsOf($loggedUser);
					$max = 5;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$numMembers = $library['group']->GetNumberOfMembersIn($row[$i]);
						$dp = $library['group']->GetProfilePicture($row[$i], 50);
						if($numMembers == 0 || $numMembers == 1)
							$numMembers .= " member";
						else
							$numMembers .= " members";
						$groupname = $library['group']->GetGroupName($row[$i]);
						if(strlen($groupname) >= 15)
							$groupname = substr($groupname, 0, 15).'...';
						echo '<a href="groups.php?id='.$row[$i].'"><div class="profpic" style="background-image: url('.$dp.')"></div><b class="color">'.$groupname.'</b><br><small><i>'.$numMembers.'</i></small></a>';
					}
					if(sizeof($row) > $max)
						echo '<a href="groups.php" align="right"><small>See all groups...</small></a>';
					if(sizeof($row) == 0)
						echo '<a>No groups to display.</a>';
					?>
				</ul>
				<div class="bottommenu">
					<script>
					$(document).ready(function() {
						$('#btnPopup_CreateGroup').click(function() {
							showPopup();
							$showPopup = "CreateGroup";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'group_'+$showPopup},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<a id="btnPopup_CreateGroup" class="add">Create</a>
					<script>
					$(document).ready(function() {
						$('#btnPopup_JoinGroup').click(function() {
							showPopup();
							$showPopup = "JoinGroup";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'group_'+$showPopup},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<a id="btnPopup_JoinGroup" class="add">Join</a>
				</div>
			</div>