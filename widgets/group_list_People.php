			<div class="content">
				<table class="title"><tr>
					<td><a href="groups.php?id=<?php echo $id; ?>&show=People">People</a></td>
					<td></td>
				</tr></table>
				<hr>
				<ul class="contentlist">
					<?php
					$row = $library['group']->GetAllMembers($id);
					$max = 2;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$dp = $library['user']->GetProfilePicture($row[$i],50);
						$name = $library['user']->GetName($row[$i]);
						if(strlen($name) > 20)
							$name = substr($name, 0, 20).'...';
						echo '<a href="index.php?-_-='.$library['user']->GetUsername($row[$i]).'" class="small" title="'.$library['user']->GetName($row[$i]).'"><div class="profpic small" style="background-image: url('.$dp.');"></div><span>'.$name.'</span></a>';
					}
					if(sizeof($row) > $max) 
						echo '<a href="courses.php?id='.$id.'&show=People" align="right"><small>See all members...</small></a>';
					if(sizeof($row) == 0) 
						echo '<a>There are no members in this group.</a>';
					?>
				</ul>
			</div>