			
<script type="text/javascript">

// loads newsfeed
$(document).ready(function() {
	$.ajax({
		type: "POST",
		url: "process.php?action=refreshnewsfeed",
		cache: false,
		data: {fromwhat: "Newsfeed", viewProfileOf: <?php echo $viewProfileOf; ?>},
		success: function(html){
			$(".newsfeedContainer").html(html);
		}
	});

	// automatically load newsfeed when postSuccess = 1, checks every second
	$postSuccess = 0;
	$(document).everyTime(1, function() {
		if($postSuccess == 1) {
			$.ajax({
				type: "POST",
				url: "process.php?action=refreshnewsfeed",
				cache: false,
				data: {fromwhat: "Newsfeed", viewProfileOf: <?php echo $viewProfileOf; ?>},
				success: function(html){
					$(".newsfeedContainer").html(html);
					$("#txtStatus").val('');
					init();
					$('#btnPost').css({
						"display": "block"
					});
					$('#postLoadingGif').css({
						"display": "none"
					});
				}
			});
			$postSuccess = 0;
		}
	});

	// refresh newsfeed 
	$('#btnRefreshNewsfeed').click(function() {
		$.ajax({
			type: "POST",
			url: "process.php?action=refreshnewsfeed",
			cache: false,
			data: {fromwhat: "Newsfeed", viewProfileOf: <?php echo $viewProfileOf; ?>},
			success: function(html){
				$(".newsfeedContainer").html(html);
				$("#btnRefreshNewsfeed").css({
					"height": "0px",
					"padding": "0px 0px",
					"margin-bottom": "00px",
				});
			},
			error: function() {
				$('#msgPost_Error').css({
					"height": "auto",
					"padding": "10px 10px"
				})
				setTimeout(function() {
					$('#msgPost_Error').css({
					"height": "0px",
					"padding": "0px 10px"
					});
				}, 3000);
			}
		});
	});

});

</script>
			<div class="newsfeedContainer">
				<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></center>
			</div>