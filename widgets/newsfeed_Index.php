<script>

// loads newsfeed
$(document).ready(function() {
	$.ajax({
		type: "POST",
		url: "process.php?action=refreshnewsfeed",
		cache: false,
		data: {fromwhat: 'Newsfeed'},
		success: function(html){
			$(".newsfeedContainer").html(html);
		}
	});
});


// automatically load newsfeed when postSuccess = 1, checks every second
$postSuccess = 0;
$(document).everyTime(1000, function() {
	if($postSuccess == 1) {
		$.ajax({
			type: "POST",
			url: "process.php?action=refreshnewsfeed",
			cache: false,
			data: {fromwhat: 'Newsfeed'},
			success: function(html){
				$(".newsfeedContainer").html(html);
				$("#txtStatus").val('');
				$("#btnRefreshNewsfeed").css({
					"height": "0px",
					"padding": "0px 0px",
					"margin-bottom": "00px",
				});
				$('#btnPost').css({
					"display": "block"
				});
				$('#postLoadingGif').css({
					"display": "none"
				});
				init();
			}
		});
		$postSuccess = 0;
	}
})

$(document).ready(function(){

	// refresh newsfeed 
	$('#btnRefreshNewsfeed').click(function() {
		$.ajax({
			type: "POST",
			url: "process.php?action=refreshnewsfeed",
			cache: false,
			data: {fromwhat: 'Newsfeed'},
			success: function(html){
				$(".newsfeedContainer").html(html);
				$("#btnRefreshNewsfeed").css({
					"height": "0px",
					"padding": "0px 0px",
					"margin-bottom": "00px",
				});
			},
			error: function() {
				$('#msgPost_Error').css({
					"height": "auto",
					"padding": "10px 10px"
				})
				setTimeout(function() {
					$('#msgPost_Error').css({
					"height": "0px",
					"padding": "0px 10px"
					});
				}, 3000);
			}
		});
	});
});

</script>
			<div class="newsfeedContainer">
				<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" alt="" class="loadingGif"></center>
			</div>