<?php
require_once('library/Config.php');
date_default_timezone_set('Asia/Manila');
$dateToday = date('Y-m-d');
$timeToday = date('H:i:s');
if(isset($_GET['action'])) {
	$action = $_GET['action'];
	if($action == "login") {
		if(isset($_POST['username']) && isset($_POST['password'])) {
			$user = new User();
			if($user->Login($_POST['username'], $_POST['password'])) {
				$_SESSION['golem_username'] = $_POST['username'];
				$ip = $_SERVER['REMOTE_ADDR'];
				$datevisit = date('Y-m-d');
				$timevisit = date('H:i:s');
				$userid = $user->GetID($_POST['username']);
				mysql_query("INSERT INTO SessionLogged(ext_ip_address,date_visit,time_visit,UserID) VALUES ('$ip','$datevisit','$timevisit','$userid')");
			}
		} else
			header("Location: index.php?error");
	} elseif($action == "register") {
		if(isset($_POST['submit'])) {
			$user = new User();
			$user->Register($_POST['username'], $_POST['password'], $_POST['firstname'], $_POST['lastname'], $_POST['type'], $_POST['gender']);
			header("Location: index.php");
		} else
			header("Location: index.php?error");
	} elseif($action == "createcourse") {
		if(isset($_POST['coursename'])) {
			if($_POST['coursename'] != "") {
				$name = $_POST['coursename'];
				$desc = $_POST['coursedescription'];
				$type = $_POST['type'];
				if($_FILES['profilepicture']['size'] != 0 && $_FILES['profilepicture']['error'] == 0) {
					$photo = $_FILES['profilepicture'];
					$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
					$detectedType = exif_imagetype($_FILES['profilepicture']['tmp_name']);
					if(in_array($detectedType, $allowedTypes)) {
						$courseid = $library['course']->CreateCourse($loggedUser, $name, $desc, $type);
						move_uploaded_file($photo['tmp_name'], "courses/".$courseid."/dp/orig.jpg");
						$library['imageh']->resizeImage("courses/".$courseid."/dp/orig.jpg", 800, "courses/".$courseid."/dp/800.jpg");
						$library['imageh']->resizeImage("courses/".$courseid."/dp/orig.jpg", 200, "courses/".$courseid."/dp/200.jpg");
						$library['imageh']->resizeImage("courses/".$courseid."/dp/orig.jpg", 50, "courses/".$courseid."/dp/50.jpg");
						header("Location: courses.php?id=".$courseid);
					} else {
						header("Location: courses.php");
					}
				} else {
					$courseid = $library['course']->CreateCourse($loggedUser, $name, $desc, $type);
					header("Location: courses.php?id=".$courseid);
				}
			} else {
				header("Location: courses.php");
			}
		}
	} elseif($action == "editcourse") {
		if(isset($_POST['coursename'])) {
			if($_POST['coursename'] != "") {
				$courseid = $_POST['courseid'];
				$name = $_POST['coursename'];
				$desc = $_POST['coursedescription'];
				$type = $_POST['type'];
				if($_FILES['profilepicture']['size'] != 0 && $_FILES['profilepicture']['error'] == 0) {
					$photo = $_FILES['profilepicture'];
					$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
					$detectedType = exif_imagetype($_FILES['profilepicture']['tmp_name']);
					if(in_array($detectedType, $allowedTypes)) {
						$library['course']->EditCourse($courseid, $name, $desc, $type);
						if(file_exists("courses/".$courseid."/dp/orig.jpg")) {
							unlink("courses/".$courseid."/dp/orig.jpg");
							unlink("courses/".$courseid."/dp/800.jpg");
							unlink("courses/".$courseid."/dp/200.jpg");
							unlink("courses/".$courseid."/dp/50.jpg");
						}
						move_uploaded_file($photo['tmp_name'], "courses/".$courseid."/dp/orig.jpg");
						$library['imageh']->resizeImage("courses/".$courseid."/dp/orig.jpg", 800, "courses/".$courseid."/dp/800.jpg");
						$library['imageh']->resizeImage("courses/".$courseid."/dp/orig.jpg", 200, "courses/".$courseid."/dp/200.jpg");
						$library['imageh']->resizeImage("courses/".$courseid."/dp/orig.jpg", 50, "courses/".$courseid."/dp/50.jpg");
						header("Location: courses.php?id=".$courseid);
					} else {
						header("Location: courses.php?id=$courseid");
					}
				} else {
					$library['course']->EditCourse($courseid, $name, $desc, $type);
					header("Location: courses.php?id=".$courseid);
				}
			} else {
				header("Location: courses.php");
			}
		} else {
			header("Location: courses.php");
		}
	} elseif($action == "enrollcourse") {
		if(isset($_POST['coursekey'])) {
			$ck = $_POST['coursekey'];
			if($library['course']->CheckIfCourseExistsWithCourseKey($ck)) {
				$courseID = $library['course']->GetCourseIDFromKey($ck);
				if(isset($_POST['courseid'])) {
					if($courseID == $_POST['courseid']) {
						if(!$library['course']->CheckIfEnrolled($loggedUser, $courseID)) {
							$newfromUserID = "$loggedUser-others({".$loggedUser."})";
							$query = mysql_query("SELECT * FROM Notification WHERE Action = 'joined' AND ActionFromWhat = 'Course' AND DateNotif = '$dateToday' AND WhereID = '$courseID'");
							while($row = mysql_fetch_array($query)) {
								$notifID = $row['NotificationID'];
								$fromUserID = $row['FromUserID'];
								$fromUserID = substr($fromUserID, 0, -2);
								$fromUserID = explode('-others({', $fromUserID);
								$andmore = explode(',', $fromUserID[1]);
								$isNewPoster = false;
								if(!in_array($loggedUser, $andmore)) {
									$isNewPoster = true;
								}
								if($isNewPoster) {
									if($fromUserID[1] == "") {
										$fromUserID[1] = $loggedUser;
									} else {
										$fromUserID[1] = $fromUserID[1].','.$loggedUser;
									}
								}
								$fromUserID[0] = $loggedUser;
								$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
								mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
							}

							$query = mysql_query("SELECT * FROM CourseUser WHERE CourseID = $courseID AND UserID IN (SELECT ID FROM Account WHERE Type = 'Faculty')");
							while($row = mysql_fetch_array($query)) {
								$facultyID = $row['UserID'];
								mysql_query("INSERT INTO Notification 
									(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
									('$newfromUserID', 'joined', '$courseID', 'Course', '$courseID', 'Request', 0, '$facultyID', '$dateToday', '$timeToday')");
							}
							$library['course']->EnrollToACourse($loggedUser, $courseID);
							header("Location: courses.php?id=".$_POST['courseid']."&enrolled");
						} else {
							header("Location: courses.php?id=".$_POST['courseid']."&error");
						}
					} else {
						header("Location: courses.php?id=".$_POST['courseid']."&error");
					}
				} else {
					if(!$library['course']->CheckIfEnrolled($loggedUser, $courseID)) {
						$newfromUserID = "$loggedUser-others({".$loggedUser."})";
						$query = mysql_query("SELECT * FROM Notification WHERE Action = 'joined' AND ActionFromWhat = 'Course' AND DateNotif = '$dateToday' AND WhereID = '$courseID'");
						while($row = mysql_fetch_array($query)) {
							$notifID = $row['NotificationID'];
							$fromUserID = $row['FromUserID'];
							$fromUserID = substr($fromUserID, 0, -2);
							$fromUserID = explode('-others({', $fromUserID);
							$andmore = explode(',', $fromUserID[1]);
							$isNewPoster = false;
							if(!in_array($loggedUser, $andmore)) {
								$isNewPoster = true;
							}
							if($isNewPoster) {
								if($fromUserID[1] == "") {
									$fromUserID[1] = $loggedUser;
								} else {
									$fromUserID[1] = $fromUserID[1].','.$loggedUser;
								}
							}
							$fromUserID[0] = $loggedUser;
							$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
							mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
						}

						$query = mysql_query("SELECT * FROM CourseUser WHERE CourseID = $courseID AND UserID IN (SELECT ID FROM Account WHERE Type = 'Faculty')");
						while($row = mysql_fetch_array($query)) {
							$facultyID = $row['UserID'];
							mysql_query("INSERT INTO Notification 
								(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
								('$newfromUserID', 'joined', '$courseID', 'Course', '$courseID', 'Request', 0, '$facultyID', '$dateToday', '$timeToday')");
						}
						$library['course']->EnrollToACourse($loggedUser, $courseID);
						header("Location: courses.php?id=".$courseID."&enrolled");
					} else {
						header("Location: courses.php?id=".$courseID."&enrolled");
					}
				}
			} else {
				header("Location: courses.php?error");
			}
		}
	} elseif($action == "editdescription") {
		if(isset($_POST['coursedescription'])) {
			$cd = $_POST['coursedescription'];
			$id = $_POST['courseid'];
			$library['course']->UpdateCourseDescription($id, $cd);
			header("Location: courses.php?id=".$id);
		}
	} elseif($action == "addevent") {
		if(isset($_POST['courseID'])) {
			$name = $_POST['eventname'];
			$startdate = $_POST['startdate'];
			$starttime = $_POST['starttime'];
			$enddate = $_POST['enddate'];
			$endtime = $_POST['endtime'];
			$courseID = $_POST['courseID'];
			if($name != "" && $startdate != "" && $starttime != "" && $enddate != "" && $endtime != "") {
				$library['calendar']->AddEvent($name, $startdate, $starttime, $enddate, $endtime, $courseID, 'Course');	
			}
			header("Location: courses.php?id=$courseID&show=Calendar");
		} elseif(isset($_POST['groupID'])) {
			$name = $_POST['eventname'];
			$startdate = $_POST['startdate'];
			$starttime = $_POST['starttime'];
			$enddate = $_POST['enddate'];
			$endtime = $_POST['endtime'];
			$groupID = $_POST['groupID'];
			$fromwhat = $_POST['fromwhat'];
			if($name != "" && $startdate != "" && $starttime != "" && $enddate != "" && $endtime != "") {
				$library['calendar']->AddEvent($name, $startdate, $starttime, $enddate, $endtime, $groupID, 'Group');	
			}
			header("Location: groups.php?id=$groupID&show=Calendar");
		}
	} elseif($action == "deleteevent") {
		if(isset($_POST['deleteevent'])) {
			$eid = $_POST['eventid'];
			$idfromwhat = $_GET['id'];
			$fromwhat = $_GET['fromwhat'];
			$library['calendar']->DeleteEvent($eid);
			if($fromwhat == "Course")
				header("Location: courses.php?id=$idfromwhat&show=Calendar");
			elseif ($fromwhat == "Group")
				header("Location: groups.php?id=$idfromwhat&show=Calendar");
		}
	} elseif($action == "deletecourse") {
		if(isset($_POST['idToDelete'])) {
			$idtodelete = $_POST['idToDelete'];
			$library['course']->DeleteCourse($idtodelete);
			header("Location: index.php");
		}
	} elseif($action == "downloadfile") {
		if(isset($_POST['filedir'])) {
			$filedownload = $_POST['filedir'];
			$h = $library['resources']->GenerateHash();
			header("Location: process.php?action=downloadfile&filedownload=$filedownload&hash=$h");
		} elseif(isset($_GET['filedownload']) && isset($_GET['hash'])) {
			$h = $_GET['hash'];
			$filedownload = $_GET['filedownload'];
			$datedl = date('Y-m-d');
			$timedl = date('H:i:s');
			$library['resources']->DownloadFile($loggedUser, $filedownload, $datedl, $timedl, $h);
			header("Location: $filedownload");
		}
	} elseif($action == "addfolder") {
		if(isset($_POST['courseID'])) {
			$foldername = $_POST['foldername'];
			$parent = $_POST['parent'];
			if($parent == "/") 
				$parent = "";
			$courseID = $_POST['courseID'];
			$newpath = "courses/".$courseID."/resources$parent/$foldername";
			if($foldername != "") {
				if(!file_exists($newpath)) {
					mkdir($newpath,0777);
					header("Location: courses.php?id=$courseID&show=Resources");
				} else {
					$folderExists = true;
					$newfoldername = $foldername;
					while($folderExists) {
						$newfoldername = $newfoldername.' copy';
						$newpath = "courses/".$courseID."/resources$parent/".$newfoldername;
						if(!file_exists($newpath)) {
							$folderExists = false;
						}
					}
					mkdir($newpath,0777);
					header("Location: courses.php?id=$courseID&show=Resources");
				}
			} else {
				header("Location: courses.php?id=$courseID&show=Resources&error=2");
			}
		} elseif(isset($_POST['groupID'])) {
			$foldername = $_POST['foldername'];
			$parent = $_POST['parent'];
			if($parent == "/") 
				$parent = "";
			$groupID = $_POST['groupID'];
			$newpath = "groups/".$groupID."/resources$parent/$foldername";
			if($foldername != "") {
				if(!file_exists($newpath)) {
					mkdir($newpath,0777);
					header("Location: groups.php?id=$groupID&show=Resources");
				} else {
					$folderExists = true;
					$newfoldername = $foldername;
					while($folderExists) {
						$newfoldername = $newfoldername.' copy';
						$newpath = "groups/".$groupID."/resources$parent/".$newfoldername;
						if(!file_exists($newpath)) {
							$folderExists = false;
						}
					}
					mkdir($newpath,0777);
					header("Location: groups.php?id=$groupID&show=Resources");
				}
			} else {
				header("Location: groups.php?id=$groupID&show=Resources&error=2");
			}
		}
	}  elseif($action == "addfile") {
		if(isset($_POST['courseID'])) {
			$courseID = $_POST['courseID'];
			$parent = $_POST['parent'];
			if($parent == "/") 
				$parent = "";
			if($_FILES['filetoupload']['size'] != 0 && $_FILES['filetoupload']['error'] == 0) {
				$file = $_FILES['filetoupload'];
				$newfilepath = "courses/".$courseID."/resources$parent/".$file['name'];
				$fileExists = false;
				if(file_exists($newfilepath)) {
					$fileExists = true;
					$newfilename = $file['name'];
					while($fileExists) {
						$filename = substr(basename($newfilename, pathinfo($newfilename, PATHINFO_EXTENSION)), 0, -1);
						$newfilename = $filename.' copy';
						$newfilename = $newfilename.'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
						$newfilepath = "courses/".$courseID."/resources$parent/".$newfilename;
						if(!file_exists($newfilepath)) {
							$fileExists = false;
						}
					}
				}
				move_uploaded_file($file['tmp_name'], $newfilepath);
				header("Location: courses.php?id=$courseID&show=Resources");
			} else {
				header("Location: courses.php?id=$courseID&show=Resources");
			}
		} elseif(isset($_POST['groupID'])) {
			$groupID = $_POST['groupID'];
			$parent = $_POST['parent'];
			if($parent == "/") 
				$parent = "";
			if($_FILES['filetoupload']['size'] != 0 && $_FILES['filetoupload']['error'] == 0) {
				$file = $_FILES['filetoupload'];
				$newfilepath = "groups/".$groupID."/resources$parent/".$file['name'];
				$fileExists = false;
				if(file_exists($newfilepath)) {
					$fileExists = true;
					$newfilename = $file['name'];
					while($fileExists) {
						$filename = substr(basename($newfilename, pathinfo($newfilename, PATHINFO_EXTENSION)), 0, -1);
						$newfilename = $filename.' copy';
						$newfilename = $newfilename.'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
						$newfilepath = "groups/".$groupID."/resources$parent/".$newfilename;
						if(!file_exists($newfilepath)) {
							$fileExists = false;
						}
					}
				}
				move_uploaded_file($file['tmp_name'], $newfilepath);
				header("Location: groups.php?id=$groupID&show=Resources");
			} else {
				header("Location: groups.php?id=$groupID&show=Resources");
			}
		}
	} elseif($action == "deletefolder") {
		if(isset($_POST['courseID'])) {
			$dir = $_POST['dir'];
			$library['course']->deleteDir($dir);
			$courseID = $_POST['courseID'];
			header("Location: courses.php?id=$courseID&show=Resources");
		} elseif(isset($_POST['groupID'])) {
			$dir = $_POST['dir'];
			$library['group']->deleteDir($dir);
			$groupID = $_POST['groupID'];
			header("Location: groups.php?id=$groupID&show=Resources");
		}
	} elseif($action == "deletefile") {
		if(isset($_POST['courseID'])) {
			$filedir = $_POST['filedir'];
			$courseID = $_POST['courseID'];
			unlink($filedir);
			header("Location: courses.php?id=$courseID&show=Resources");
		} elseif(isset($_POST['groupID'])) {
			$filedir = $_POST['filedir'];
			$groupID = $_POST['groupID'];
			unlink($filedir);
			header("Location: groups.php?id=$groupID&show=Resources");
		}
	} elseif($action == "creategroup") {
		if(isset($_POST['groupname'])) {
			if($_POST['groupname'] != "") {
				$name = $_POST['groupname'];
				$desc = $_POST['groupdescription'];
				$type = $_POST['type'];
				if($_FILES['profilepicture']['size'] != 0 && $_FILES['profilepicture']['error'] == 0) {
					$photo = $_FILES['profilepicture'];
					$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
					$detectedType = exif_imagetype($_FILES['profilepicture']['tmp_name']);
					if(in_array($detectedType, $allowedTypes)) {
						$groupid = $library['group']->CreateGroup($loggedUser, $name, $desc, $type);
						move_uploaded_file($photo['tmp_name'], "groups/".$groupid."/dp/orig.jpg");
						$library['imageh']->resizeImage("groups/".$groupid."/dp/orig.jpg", 800, "groups/".$groupid."/dp/800.jpg");
						$library['imageh']->resizeImage("groups/".$groupid."/dp/orig.jpg", 200, "groups/".$groupid."/dp/200.jpg");
						$library['imageh']->resizeImage("groups/".$groupid."/dp/orig.jpg", 50, "groups/".$groupid."/dp/50.jpg");
						header("Location: groups.php?id=".$groupid);
					} else {
						header("Location: groups.php");
					}
				} else {
					$groupid = $library['group']->CreateGroup($loggedUser, $name, $desc, $type);
					header("Location: groups.php?id=".$groupid);
				}
			} else {
				header("Location: groups.php");
			}
		}
	} elseif($action == "joingroup") {
		if(isset($_POST['groupkey'])) {
			$gk = $_POST['groupkey'];
			if($library['group']->CheckIfGroupExistsWithGroupKey($gk)) {
				$groupID = $library['group']->GetGroupIDFromKey($gk);
				if(isset($_POST['groupid'])) {
					if($groupID == $_POST['groupid']) {
						if(!$library['group']->CheckIfJoined($loggedUser, $groupID)) {
							$newfromUserID = "$loggedUser-others({".$loggedUser."})";
							$query = mysql_query("SELECT * FROM Notification WHERE Action = 'joined' AND ActionFromWhat = 'Group' AND DateNotif = '$dateToday' AND WhereID = '$groupID'");
							while($row = mysql_fetch_array($query)) {
								$notifID = $row['NotificationID'];
								$fromUserID = $row['FromUserID'];
								$fromUserID = substr($fromUserID, 0, -2);
								$fromUserID = explode('-others({', $fromUserID);
								$andmore = explode(',', $fromUserID[1]);
								$isNewPoster = false;
								if(!in_array($loggedUser, $andmore)) {
									$isNewPoster = true;
								}
								if($isNewPoster) {
									if($fromUserID[1] == "") {
										$fromUserID[1] = $loggedUser;
									} else {
										$fromUserID[1] = $fromUserID[1].','.$loggedUser;
									}
								}
								$fromUserID[0] = $loggedUser;
								$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
								mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
							}

							$query = mysql_query("SELECT * FROM GroupUser WHERE GroupID = $groupID AND Type = 'Admin'");
							while($row = mysql_fetch_array($query)) {
								$facultyID = $row['UserID'];
								mysql_query("INSERT INTO Notification 
									(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
									('$newfromUserID', 'joined', '$groupID', 'Group', '$groupID', 'Request', 0, '$facultyID', '$dateToday', '$timeToday')");
							}
							$library['group']->JoinToAGroup($loggedUser, $groupID);
							header("Location: groups.php?id=".$groupID."&enrolled");
						} else {
							header("Location: groups.php?id=".$groupID."&error");
						}
					} else {
						header("Location: groups.php?id=".$_POST['groupid']."&error");
					}
				} else {
					if(!$library['group']->CheckIfJoined($loggedUser, $groupID)) {
						$newfromUserID = "$loggedUser-others({".$loggedUser."})";
						$query = mysql_query("SELECT * FROM Notification WHERE Action = 'joined' AND ActionFromWhat = 'Group' AND DateNotif = '$dateToday' AND WhereID = '$groupID'");
						while($row = mysql_fetch_array($query)) {
							$notifID = $row['NotificationID'];
							$fromUserID = $row['FromUserID'];
							$fromUserID = substr($fromUserID, 0, -2);
							$fromUserID = explode('-others({', $fromUserID);
							$andmore = explode(',', $fromUserID[1]);
							$isNewPoster = false;
							if(!in_array($loggedUser, $andmore)) {
								$isNewPoster = true;
							}
							if($isNewPoster) {
								if($fromUserID[1] == "") {
									$fromUserID[1] = $loggedUser;
								} else {
									$fromUserID[1] = $fromUserID[1].','.$loggedUser;
								}
							}
							$fromUserID[0] = $loggedUser;
							$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
							mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
						}

						$query = mysql_query("SELECT * FROM GroupUser WHERE GroupID = $groupID AND Type = 'Admin'");
						while($row = mysql_fetch_array($query)) {
							$facultyID = $row['UserID'];
							mysql_query("INSERT INTO Notification 
								(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
								('$newfromUserID', 'joined', '$groupID', 'Group', '$groupID', 'Request', 0, '$facultyID', '$dateToday', '$timeToday')");
						}
						$library['group']->JoinToAGroup($loggedUser, $groupID);
						header("Location: groups.php?id=".$groupID."&enrolled");
					} else {
						header("Location: groups.php?id=".$groupID."&error");
					}
				}
			} else {
				header("Location: groups.php?error");
			}
		}
	} elseif($action == "leavegroup") {
		if(isset($_POST['leave']) && isset($_POST['groupID'])) {
			$groupID = $_POST['groupID'];
			mysql_query("DELETE FROM GroupUser WHERE GroupID = $groupID AND UserID = $loggedUser");
			header("Location: index.php");
		}
	} elseif($action == "editgroup") {
		if(isset($_POST['groupname'])) {
			if($_POST['groupname'] != "") {
				$groupid = $_POST['groupid'];
				$name = $_POST['groupname'];
				$desc = $_POST['groupdescription'];
				$type = $_POST['type'];
				if($_FILES['profilepicture']['size'] != 0 && $_FILES['profilepicture']['error'] == 0) {
					$photo = $_FILES['profilepicture'];
					$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
					$detectedType = exif_imagetype($_FILES['profilepicture']['tmp_name']);
					if(in_array($detectedType, $allowedTypes)) {
						$library['group']->EditGroup($courseid, $name, $desc, $type);
						if(file_exists("groups/".$groupid."/dp/orig.jpg")) {
							unlink("groups/".$groupid."/dp/orig.jpg");
							unlink("groups/".$groupid."/dp/800.jpg");
							unlink("groups/".$groupid."/dp/200.jpg");
							unlink("groups/".$groupid."/dp/50.jpg");
						}
						move_uploaded_file($photo['tmp_name'], "groups/".$groupid."/dp/orig.jpg");
						$library['imageh']->resizeImage("groups/".$groupid."/dp/orig.jpg", 800, "groups/".$groupid."/dp/800.jpg");
						$library['imageh']->resizeImage("groups/".$groupid."/dp/orig.jpg", 200, "groups/".$groupid."/dp/200.jpg");
						$library['imageh']->resizeImage("groups/".$groupid."/dp/orig.jpg", 50, "groups/".$groupid."/dp/50.jpg");
						header("Location: groups.php?id=".$groupid);
					} else {
						header("Location: groups.php?id=$courseid");
					}
				} else {
					$library['group']->EditGroup($groupid, $name, $desc, $type);
					header("Location: groups.php?id=".$groupid);
				}
			} else {
				header("Location: groups.php");
			}
		} else {
			header("Location: groups.php");
		}
	} elseif($action == "deletegroup") {
		if(isset($_POST['idToDelete'])) {
			$idtodelete = $_POST['idToDelete'];
			$library['group']->DeleteGroup($idtodelete);
			header("Location: index.php");
		}
	} elseif($action == "deletejoinrequest") {
		if(isset($_POST['id']) && isset($_POST['userid']) && isset($_POST['fromwhat'])) {
			$id = $_POST['id'];
			$userid = $_POST['userid'];
			$fromwhat = $_POST['fromwhat'];
			if($fromwhat == "Course") {
				if($userid != $loggedUser) {
					$newfromUserID = "$loggedUser-others({".$loggedUser."})";
					mysql_query("INSERT INTO Notification 
						(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
						('$newfromUserID', 'rejectrequest', '$id', '$fromwhat', '$id', 'Request', 0, '$userid', '$dateToday', '$timeToday')");
				}
				$library['course']->RejectRequest($id, $userid);
			} elseif($fromwhat == "Group") {
				if($userid != $loggedUser) {
					$newfromUserID = "$loggedUser-others({".$loggedUser."})";
					mysql_query("INSERT INTO Notification 
						(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
						('$newfromUserID', 'rejectrequest', '$id', '$fromwhat', '$id', 'Request', 0, '$userid', '$dateToday', '$timeToday')");
				}
				$library['group']->RejectRequest($id, $userid);
			}
		}
	} elseif($action == "getpeopleoncourse") {
		if(isset($_POST['id'])) {
			$id = $_POST['id'];
			$library['course']->GetPeopleOnCourse($id, $loggedUser, $library, $skin);
		}
	} elseif($action == "getpeopleongroup") {
		if(isset($_POST['id'])) {
			$id = $_POST['id'];
			$library['group']->GetPeopleOnGroup($id, $loggedUser, $library, $skin);
		}
	} elseif($action == "getpeopletoinvite") {
		if(isset($_POST['id']) && isset($_POST['fromwhat']) && isset($_POST['q'])) {
			$id = $_POST['id'];
			$searchQuery = $_POST['q'];
			$fromwhat = $_POST['fromwhat'];
			if($searchQuery != "") {
				if($fromwhat == "Course") {
					$library['course']->GetPeopleToInvite($searchQuery, $id, $loggedUser, $library, $skin);
				} elseif ($fromwhat == "Group") {
					$library['group']->GetPeopleToInvite($searchQuery, $id, $loggedUser, $library, $skin);
				}
			} else {
				if($fromwhat == "Course") {
					echo '<a><small><i>You can invite other people to enroll this course.</i></small></a>';
				} elseif($fromwhat == "Group") {
					echo '<a><small><i>You can add other people to join this group.</i></small></a>';
				}
			}
		}
	} elseif($action == "getrequests") {
		if(isset($_POST['id']) && isset($_POST['fromwhat'])) {
			$id = $_POST['id'];
			$fromwhat = $_POST['fromwhat'];
			if($fromwhat == "Course") {
				$library['course']->GetRequestToApprove($id, $loggedUser, $library, $skin);
			} elseif ($fromwhat == "Group") {
				$library['group']->GetRequestToApprove($id, $loggedUser, $library, $skin);
			}
		}
	} elseif($action == "joinpeople") {
		if(isset($_POST['id']) && isset($_POST['fromwhat']) && isset($_POST['userid'])) {
			$id = $_POST['id'];
			$userid = $_POST['userid'];
			$fromwhat = $_POST['fromwhat'];
			if($fromwhat == "Course") {
				if(isset($_POST['request']))
					$action = "invited";
				else
					$action = "acceptrequest";
				$newfromUserID = "$loggedUser-others({".$loggedUser."})";
				mysql_query("INSERT INTO Notification 
					(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
					('$newfromUserID', '$action', '$id', '$fromwhat', '$id', 'Request', 0, '$userid', '$dateToday', '$timeToday')");
				$library['course']->AcceptRequest($id, $userid);
			} elseif ($fromwhat == "Group") {
				if(isset($_POST['request']))
					$action = "invited";
				else
					$action = "acceptrequest";
				$newfromUserID = "$loggedUser-others({".$loggedUser."})";
				mysql_query("INSERT INTO Notification 
					(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
					('$newfromUserID', '$action', '$id', '$fromwhat', '$id', 'Request', 0, '$userid', '$dateToday', '$timeToday')");
				$library['group']->AcceptRequest($id, $userid);
			}
		}
	} elseif($action == "refreshdatetimeinterval") {
		if(isset($_POST['date']) && isset($_POST['time']) && isset($_POST['postLink'])) {
			$date = $_POST['date'];
			$time = $_POST['time'];
			$postLink = $_POST['postLink'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $library['newsfeed']->GetDateTimeInterval($date, $time);
			echo '<a title="'.$datetimetitle.'" href="'.$postLink.'">'.$datetimeInterval.'</a>';
		}
	} elseif($action == "refreshcomments") {
		if(isset($_POST['postID']) && isset($_POST['postHash']) && isset($_POST['type'])) {
			$postid = $_POST['postID'];
			$postHashID = $_POST['postHash'];
			$postLink = $_POST['postLink'];
			$type = $_POST['type'];
			echo '<table id="tblCommentsFor_'.$postHashID.'" class="comments" cellspacing="0" cellpadding="0">';
			$library['newsfeed']->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
			echo '</table>';
		}
	} elseif($action == "refreshnewsfeed") {
		if(isset($_POST['fromwhat'])) {
			$fromwhat = $_POST['fromwhat'];
			if($fromwhat == "Newsfeed") {
				if(isset($_POST['viewProfileOf'])) {
					$viewProfileOf = $_POST['viewProfileOf'];
					$library['newsfeed']->GetOtherUserNewsfeed($viewProfileOf, $loggedUser, $skin, $library);
				} else {
					$library['newsfeed']->GetUserNewsfeed($loggedUser, $skin, $library);
				}
			} elseif($fromwhat == "Course") {
				if(isset($_POST['id'])) {
					$id = $_POST['id'];
					$library['newsfeed']->GetCourseNewsfeed($id, $loggedUser, $skin, $library);
				}
			} elseif($fromwhat == "Group") {
				if(isset($_POST['id'])) {
					$id = $_POST['id'];
					$library['newsfeed']->GetGroupNewsfeed($id, $loggedUser, $skin, $library);
				} 
			}
		}
	} elseif($action == "savecomment") {
		if(isset($_POST['postMsg']) && isset($_POST['postID']) && isset($_POST['postHash'])) {
			$postHashID = $_POST['postHash'];
			$postID = $_POST['postID'];
			$query = mysql_query("SELECT * FROM Post WHERE PostID = $postID");
			if(mysql_num_rows($query) == 1) {
				$row = mysql_fetch_array($query);
				$id = $row['ID'];
				$fromwhat = $row['FromWhat'];
				$postMessage = trim($_POST['postMsg']);
				if($postMessage != "") {
					$postMessage = mysql_real_escape_string($postMessage);
					mysql_query("INSERT INTO Post 
						(ID, FromWhat, UserID, Message, ReferTo, DatePosted, TimePosted) VALUES
					 	('$id', '$fromwhat', '$loggedUser', '$postMessage', '$postID', '$dateToday', '$timeToday')");
					
					$newfromUserID = "$loggedUser-others({".$loggedUser."})";
					$query = mysql_query("SELECT * FROM Notification WHERE Action = 'commented' AND DateNotif = '$dateToday' AND WhereID = '$postID'");
					while($row = mysql_fetch_array($query)) {
						$notifID = $row['NotificationID'];
						$fromUserID = $row['FromUserID'];
						$fromUserID = substr($fromUserID, 0, -2);
						$fromUserID = explode('-others({', $fromUserID);
						$andmore = explode(',', $fromUserID[1]);
						$isNewPoster = false;
						if(!in_array($loggedUser, $andmore)) {
							$isNewPoster = true;
						}
						if($isNewPoster) {
							if($fromUserID[1] == "") {
								$fromUserID[1] = $loggedUser;
							} else {
								$fromUserID[1] = $fromUserID[1].','.$loggedUser;
							}
						}
						$fromUserID[0] = $loggedUser;
						$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
						mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
					}
					$query = mysql_query("SELECT DISTINCT UserID FROM Post WHERE ReferTo = $postID AND UserID != $loggedUser UNION SELECT UserID FROM Post WHERE PostID = $postID");
					while($row = mysql_fetch_array($query)) {
						$toID = $row['UserID']; // user id who commented into the post except the current commenter
						if($toID != $loggedUser)
							mysql_query("INSERT INTO Notification 
								(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
								('$newfromUserID', 'commented', '$id', '$fromwhat',  '$postID', 'Post', 0, '$toID', '$dateToday', '$timeToday')");
					}
				}
			}
		}
	} elseif($action == "savepost") {
		if(isset($_POST['postMsg']) && isset($_POST['fromwhat'])) {
			$postMessage = trim($_POST['postMsg']);
			if($postMessage != "") {
				$postMessage = mysql_real_escape_string($postMessage);
				$fromwhat = $_POST['fromwhat'];
				if($fromwhat == "Newsfeed") {
					if(isset($_POST['id'])) {
						$id = $_POST['id'];
						mysql_query("INSERT INTO Post (ID, FromWhat, UserID, Message, DatePosted, TimePosted) VALUES ('$id', 'Newsfeed', '$loggedUser', '$postMessage', '$dateToday', '$timeToday')");
						
						$newfromUserID = "$loggedUser-others({".$loggedUser."})";
						$query = mysql_query("SELECT * FROM Notification WHERE ToUserID = '$id' AND Action = 'posted' AND DateNotif = '$dateToday' AND ActionID = '$id' AND ActionFromWhat = 'Newsfeed'");
						while($row = mysql_fetch_array($query)) {
							$notifID = $row['NotificationID'];
							$fromUserID = $row['FromUserID'];
							$fromUserID = substr($fromUserID, 0, -2);
							$fromUserID = explode('-others({', $fromUserID);
							$andmore = explode(',', $fromUserID[1]);
							$isNewPoster = false;
							if(!in_array($loggedUser, $andmore)) {
								$isNewPoster = true;
							}
							if($isNewPoster) {
								if($fromUserID[1] == "") {
									$fromUserID[1] = $loggedUser;
								} else {
									$fromUserID[1] = $fromUserID[1].','.$loggedUser;
								}
							}
							$fromUserID[0] = $loggedUser;
							$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
							mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
							break;
						}
						$query = mysql_query("SELECT PostID FROM Post ORDER BY PostID DESC LIMIT 1");
						while($row = mysql_fetch_array($query)) {
							$postID = $row['PostID'];
							mysql_query("INSERT INTO Notification 
								(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
								('$newfromUserID', 'posted', '$id', 'Newsfeed', '$postID', 'Post', 0, '$id', '$dateToday', '$timeToday')");
						}
					} else {
						mysql_query("INSERT INTO Post (ID, FromWhat, UserID, Message, DatePosted, TimePosted) VALUES ('$loggedUser', 'Newsfeed', '$loggedUser', '$postMessage', '$dateToday', '$timeToday')");
					}
				} elseif($fromwhat == "Course") {
					if(isset($_POST['id'])) {
						$id = $_POST['id'];
						mysql_query("INSERT INTO Post (ID, FromWhat, UserID, Message, DatePosted, TimePosted) VALUES ('$id', 'Course', '$loggedUser', '$postMessage', '$dateToday', '$timeToday')");
						
						$newfromUserID = "$loggedUser-others({".$loggedUser."})";
						$query = mysql_query("SELECT * FROM Notification WHERE Action = 'posted' AND DateNotif = '$dateToday' AND ActionID = '$id' AND ActionFromWhat = 'Course'");
						while($row = mysql_fetch_array($query)) {
							$notifID = $row['NotificationID'];
							$fromUserID = $row['FromUserID'];
							$fromUserID = substr($fromUserID, 0, -2);
							$fromUserID = explode('-others({', $fromUserID);
							$andmore = explode(',', $fromUserID[1]);
							$isNewPoster = false;
							if(!in_array($loggedUser, $andmore)) {
								$isNewPoster = true;
							}
							if($isNewPoster) {
								if($fromUserID[1] == "") {
									$fromUserID[1] = $loggedUser;
								} else {
									$fromUserID[1] = $fromUserID[1].','.$loggedUser;
								}
							}
							$fromUserID[0] = $loggedUser;
							$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
							mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
						}
						$query = mysql_query("SELECT PostID FROM Post ORDER BY PostID DESC LIMIT 1");
						while($row = mysql_fetch_array($query)) {
							$postID = $row['PostID'];
							$query2 = mysql_query("SELECT UserID FROM CourseUser WHERE CourseID = $id AND UserID != $loggedUser");
							while($row2 = mysql_fetch_array($query2)) {
								$toID = $row2['UserID'];
								mysql_query("INSERT INTO Notification 
									(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
									('$newfromUserID', 'posted', '$id', 'Course',  '$postID', 'Post', 0, '$toID', '$dateToday', '$timeToday')");
							}
						}
					}
				} elseif($fromwhat == "Group") {
					if(isset($_POST['id'])) {
						$id = $_POST['id'];
						mysql_query("INSERT INTO Post (ID, FromWhat, UserID, Message, DatePosted, TimePosted) VALUES ('$id', 'Group', '$loggedUser', '$postMessage', '$dateToday', '$timeToday')");
						
						$newfromUserID = "$loggedUser-others({".$loggedUser."})";
						$query = mysql_query("SELECT * FROM Notification WHERE Action = 'posted' AND DateNotif = '$dateToday' AND ActionID = '$id' AND ActionFromWhat = 'Group'");
						while($row = mysql_fetch_array($query)) {
							$notifID = $row['NotificationID'];
							$fromUserID = $row['FromUserID'];
							$fromUserID = substr($fromUserID, 0, -2);
							$fromUserID = explode('-others({', $fromUserID);
							$andmore = explode(',', $fromUserID[1]);
							$isNewPoster = false;
							if(!in_array($loggedUser, $andmore)) {
								$isNewPoster = true;
							}
							if($isNewPoster) {
								if($fromUserID[1] == "") {
									$fromUserID[1] = $loggedUser;
								} else {
									$fromUserID[1] = $fromUserID[1].','.$loggedUser;
								}
							}
							$fromUserID[0] = $loggedUser;
							$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
							mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
						}
						$query = mysql_query("SELECT PostID FROM Post ORDER BY PostID DESC LIMIT 1");
						while($row = mysql_fetch_array($query)) {
							$postID = $row['PostID'];
							$query2 = mysql_query("SELECT UserID FROM GroupUser WHERE GroupID = $id AND UserID != $loggedUser");
							while($row2 = mysql_fetch_array($query2)) {
								$toID = $row2['UserID'];
								mysql_query("INSERT INTO Notification 
									(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
									('$newfromUserID', 'posted', '$id', 'Group',  '$postID', 'Post', 0, '$toID', '$dateToday', '$timeToday')");
							}
						}
					}
				}
			} else {
?>
			<script>
			$(document).ready(function () {
				$('#msgPost_Error').css({
					"height": "auto",
					"padding": "10px 10px"
				})
				setTimeout(function() {
					$('#msgPost_Error').css({
					"height": "0px",
					"padding": "0px 10px"
					});
				}, 3000);
			});
			</script>
<?php
			}
		}
	} elseif($action == "editpost") {
		if(isset($_POST['postMsg']) && isset($_POST['postid'])) {
			$postMessage = trim($_POST['postMsg']);
			$postID = $_POST['postid'];
			if($postMessage != "") {
				$postMessage1 = mysql_real_escape_string($postMessage);
				mysql_query("UPDATE Post SET Message = '$postMessage1' WHERE PostID = $postID");
			}
			echo nl2br(htmlentities($postMessage));
		}
	} elseif($action == "deletepost") {
		if(isset($_POST['postid'])) {
			$postID = $_POST['postid'];
			mysql_query("DELETE FROM PostLikes WHERE PostID = $postID");
			mysql_query("DELETE FROM PostLikes WHERE PostID IN (SELECT PostID FROM Post WHERE ReferTo = $postID)");
			mysql_query("DELETE FROM Post WHERE PostID = $postID");
			mysql_query("DELETE FROM Post WHERE ReferTo = $postID");
			mysql_query("DELETE FROM Notification WHERE WhereFromWhat = 'Post' AND WhereID = $postID");
		}
	} elseif($action == "deletecomment") {
		if(isset($_POST['commentid'])) {
			$commentID = $_POST['commentid'];
			mysql_query("DELETE FROM PostLikes WHERE PostID = $commentID");
			mysql_query("DELETE FROM Post WHERE PostID = $commentID");
		}
	} elseif($action == "sendjoinrequest") {
		if(isset($_POST['id']) && isset($_POST['fromwhat'])) {
			$id = $_POST['id'];
			$fromwhat = $_POST['fromwhat'];
			if($fromwhat == "Course") {
				$newfromUserID = "$loggedUser-others({".$loggedUser."})";
				$query = mysql_query("SELECT * FROM Notification WHERE Action = 'joinrequest' AND ActionFromWhat = 'Course' AND DateNotif = '$dateToday' AND WhereID = '$id'");
				while($row = mysql_fetch_array($query)) {
					$notifID = $row['NotificationID'];
					$fromUserID = $row['FromUserID'];
					$fromUserID = substr($fromUserID, 0, -2);
					$fromUserID = explode('-others({', $fromUserID);
					$andmore = explode(',', $fromUserID[1]);
					$isNewPoster = false;
					if(!in_array($loggedUser, $andmore)) {
						$isNewPoster = true;
					}
					if($isNewPoster) {
						if($fromUserID[1] == "") {
							$fromUserID[1] = $loggedUser;
						} else {
							$fromUserID[1] = $fromUserID[1].','.$loggedUser;
						}
					}
					$fromUserID[0] = $loggedUser;
					$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
					mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
				}

				$query = mysql_query("SELECT * FROM CourseUser WHERE CourseID = $id AND UserID IN (SELECT ID FROM Account WHERE Type = 'Faculty')");
				while($row = mysql_fetch_array($query)) {
					$facultyID = $row['UserID'];
					mysql_query("INSERT INTO Notification 
						(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
						('$newfromUserID', 'joinrequest', '$id', '$fromwhat', '$id', 'Request', 0, '$facultyID', '$dateToday', '$timeToday')");
				}
				mysql_query("INSERT INTO CourseRequest (CourseID, UserID) VALUES ($id, $loggedUser)");
			} elseif ($fromwhat == "Group") {
				$newfromUserID = "$loggedUser-others({".$loggedUser."})";
				$query = mysql_query("SELECT * FROM Notification WHERE Action = 'joinrequest' AND ActionFromWhat = 'Group' AND DateNotif = '$dateToday' AND WhereID = '$id'");
				while($row = mysql_fetch_array($query)) {
					$notifID = $row['NotificationID'];
					$fromUserID = $row['FromUserID'];
					$fromUserID = substr($fromUserID, 0, -2);
					$fromUserID = explode('-others({', $fromUserID);
					$andmore = explode(',', $fromUserID[1]);
					$isNewPoster = false;
					if(!in_array($loggedUser, $andmore)) {
						$isNewPoster = true;
					}
					if($isNewPoster) {
						if($fromUserID[1] == "") {
							$fromUserID[1] = $loggedUser;
						} else {
							$fromUserID[1] = $fromUserID[1].','.$loggedUser;
						}
					}
					$fromUserID[0] = $loggedUser;
					$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
					mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
				}

				$query = mysql_query("SELECT * FROM GroupUser WHERE GroupID = $id AND Type = 'Admin'");
				while($row = mysql_fetch_array($query)) {
					$adminID = $row['UserID'];
					mysql_query("INSERT INTO Notification 
						(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
						('$newfromUserID', 'joinrequest', '$id', '$fromwhat', '$id', 'Request', 0, '$adminID', '$dateToday', '$timeToday')");
				}
				mysql_query("INSERT INTO GroupBlockRequest (GroupID, UserID) VALUES ($id, $loggedUser)");
			}
		}
	} elseif($action == "refreshdatetime") {
		echo '<h1>'.date('g:i:s A').'</h1><hr><p class="text" style="text-align: center;">'.date('F d, Y - l').'</p><hr>';
	} elseif($action == "refreshpostlikes") {
		if(isset($_POST['postid'])) {
			$postid = $_POST['postid'];
			$library['newsfeed']->GetPostLikes($postid, $loggedUser, $skin);
		}
	} elseif($action == "likeapost") {
		if(isset($_POST['postid'])) {
			$postid = $_POST['postid'];
			mysql_query("INSERT INTO PostLikes (PostID, UserID, DateLiked, TimeLiked) VALUES ($postid, $loggedUser, '$dateToday', '$timeToday') ");
			$newfromUserID = "$loggedUser-others({".$loggedUser."})";
			$query = mysql_query("SELECT * FROM Post WHERE PostID = $postid");
			$row = mysql_fetch_array($query);
			$posterID = $row['UserID'];
			$query = mysql_query("SELECT * FROM Notification WHERE Action = 'likeapost' AND DateNotif = '$dateToday' AND WhereID = '$postid'");
			while($row = mysql_fetch_array($query)) {
				$notifID = $row['NotificationID'];
				$fromUserID = $row['FromUserID'];
				$fromUserID = substr($fromUserID, 0, -2);
				$fromUserID = explode('-others({', $fromUserID);
				$andmore = explode(',', $fromUserID[1]);
				$isNewPoster = false;
				if(!in_array($loggedUser, $andmore)) {
					$isNewPoster = true;
				}
				if($isNewPoster) {
					if($fromUserID[1] == "") {
						$fromUserID[1] = $loggedUser;
					} else {
						$fromUserID[1] = $fromUserID[1].','.$loggedUser;
					}
				}
				$fromUserID[0] = $loggedUser;
				$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
				mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
			}

			if($posterID != $loggedUser) {
				mysql_query("INSERT INTO Notification 
					(FromUserID, Action, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
					('$newfromUserID', 'likeapost', '$postid', 'Post', 0, '$posterID', '$dateToday', '$timeToday')");
			}
			$library['newsfeed']->GetPostLikes($postid, $loggedUser, $skin);
		}
	} elseif($action == "unlikeapost") {
		if(isset($_POST['postid'])) {
			$postid = $_POST['postid'];
			mysql_query("DELETE FROM PostLikes WHERE PostID = $postid AND UserID = $loggedUser");
			$library['newsfeed']->GetPostLikes($postid, $loggedUser, $skin);
		}
	} elseif($action == "likeacomment") {
		if(isset($_POST['postid']) && isset($_POST['posthashid']) && isset($_POST['type'])) {
			$postid = $_POST['postid'];
			$postHashID = $_POST['posthashid'];
			$postLink = $_POST['postLink'];
			$commentid = $_POST['commentid'];
			$commentHashID = $_POST['commenthashid'];
			$type = $_POST['type'];
			mysql_query("INSERT INTO PostLikes (PostID, UserID, DateLiked, TimeLiked) VALUES ($commentid, $loggedUser, '$dateToday', '$timeToday') ");
			
			$newfromUserID = "$loggedUser-others({".$loggedUser."})";
			$query = mysql_query("SELECT * FROM Post WHERE PostID = $commentid");
			$row = mysql_fetch_array($query);
			$posterID = $row['UserID'];
			$query = mysql_query("SELECT * FROM Post WHERE PostID = $postid");
			$row = mysql_fetch_array($query);
			$id = $row['ID'];
			$fromwhat = $row['FromWhat'];
			$query = mysql_query("SELECT * FROM Notification WHERE Action = 'likeacomment' AND DateNotif = '$dateToday' AND WhereID = '$commentid'");
			while($row = mysql_fetch_array($query)) {
				$notifID = $row['NotificationID'];
				$fromUserID = $row['FromUserID'];
				$fromUserID = substr($fromUserID, 0, -2);
				$fromUserID = explode('-others({', $fromUserID);
				$andmore = explode(',', $fromUserID[1]);
				$isNewPoster = false;
				if(!in_array($loggedUser, $andmore)) {
					$isNewPoster = true;
				}
				if($isNewPoster) {
					if($fromUserID[1] == "") {
						$fromUserID[1] = $loggedUser;
					} else {
						$fromUserID[1] = $fromUserID[1].','.$loggedUser;
					}
				}
				$fromUserID[0] = $loggedUser;
				$newfromUserID = $fromUserID[0].'-others({'.$fromUserID[1].'})';
				mysql_query("DELETE FROM Notification WHERE NotificationID = $notifID");
			}

			if($posterID != $loggedUser) {
				mysql_query("INSERT INTO Notification 
					(FromUserID, Action, ActionID, ActionFromWhat, WhereID, WhereFromWhat, isRead, ToUserID, DateNotif, TimeNotif) VALUES 
					('$newfromUserID', 'likeacomment', '$id', '$fromwhat', '$commentid', 'Post', 0, '$posterID', '$dateToday', '$timeToday')");
			}

			$library['newsfeed']->GetCommentLikes($postid, $postHashID, $postLink, $commentid, $commentHashID, $type, $loggedUser, $skin);
		}
	}  elseif($action == "unlikeacomment") {
		if(isset($_POST['postid']) && isset($_POST['posthashid']) && isset($_POST['type'])) {
			$postid = $_POST['postid'];
			$postHashID = $_POST['posthashid'];
			$postLink = $_POST['postLink'];
			$commentid = $_POST['commentid'];
			$commentHashID = $_POST['commenthashid'];
			$type = $_POST['type'];
			mysql_query("DELETE FROM PostLikes WHERE PostID = $commentid AND UserID = $loggedUser");
			$library['newsfeed']->GetCommentLikes($postid, $postHashID, $postLink, $commentid, $commentHashID, $type, $loggedUser, $skin);
		}
	} elseif($action == "createassessment") {
		if(isset($_POST['courseID'])) {
			$courseID = $_POST['courseID'];
			$status = 0;
			$name = $_POST['assessname'];
			$name = mysql_real_escape_string(trim($name));
			$attempts = $_POST['attempts'];
			$datecreated = date('Y-m-d');
			$h = ltrim($_POST['timer_Hours'], '0');
			$m = ltrim($_POST['timer_Minutes'], '0');
			if($h == "" || $h == 0) {
				$h = 0;
				if($m == "" || $m == 0)
					$m = 1;
			} else {
				if($m == "") {
					$m = 0;
				}
			}
			if($h >= 0 && $h <= 9) {
				$h = "0$h";
			}
			if($m >= 0 && $m <= 9) {
				$m = "0$m";
			}
			$timer = "$h:$m:00";
			mysql_query("INSERT INTO Assessment (Name, CourseID, Status, Attempts, Timer, DateCreated) VALUES ('$name', '$courseID', '$status', '$attempts', '$timer', '$datecreated')");
			header("Location: courses.php?id=$courseID&show=Assessment");
		}
	} elseif($action == "editassessmentinfo") {
		if(isset($_POST['submit'])) {
			$courseID = $_POST['courseID'];
			$id = $_POST['assessID'];
			$name = $_POST['name'];
			$name = mysql_real_escape_string(trim($name));
			$attempts = $_POST['attempts'];
			$h = $_POST['timer_Hours'];
			$m = $_POST['timer_Minutes'];
			$instructions = trim($_POST['instructions']);
			$instructions = mysql_real_escape_string($instructions);
			if($h == "" || $h == 0) {
				$h = 0;
				if($m == "" || $m == 0)
					$m = 1;
			} else {
				if($m == "" || $m == 0) {
					$m = 0;
				}
			}
			if($h >= 0 && $h <= 9) {
				$h = "0$h";
			}
			if($m >= 0 && $m <= 9) {
				$m = "0$m";
			}
			$timer = "$h:$m:00";
			mysql_query("UPDATE Assessment SET Name = '$name', Attempts = '$attempts', Instructions = '$instructions', Timer = '$timer' WHERE AssessmentID = $id");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action =="giveassessment") {
		if(isset($_POST['submit'])) {
			$assessID = $_POST['assessID'];
			$courseID = $library['assessment']->GetCourseID($assessID);
			$set = $_POST['set'];
			$query = mysql_query("SELECT * FROM Question WHERE AssessmentID = $assessID");
			if(mysql_num_rows($query) != 0) {
				if($set == "dateandtime") {
					$duedate = $_POST['duedate'];
					$duetime = $_POST['duetime'];
					if($duedate == $dateToday) {
						if(strtotime($duetime) > strtotime($timeToday)) {
							mysql_query("UPDATE Assessment SET Status = 1, DateFrom = '$dateToday', TimeFrom = '$timeToday', DateTo = '$duedate', TimeTo = '$duetime' WHERE AssessmentID = $assessID");
						}
					} else {
						mysql_query("UPDATE Assessment SET Status = 1, DateFrom = '$dateToday', TimeFrom = '$timeToday', DateTo = '$duedate', TimeTo = '$duetime' WHERE AssessmentID = $assessID");
					}
				} elseif($set == "dayhourminute") {
					$newdatetime = $dateToday.' '.$timeToday;
					$days = $_POST['timer_Days'];
					$hours = $_POST['timer_Hours'];
					$minutes = $_POST['timer_Minutes'];
					$due = date('Y-m-d H:i:00', strtotime("$days day $hours hour $minutes minute"));
					$due = explode(" ", $due);
					$duedate = $due[0];
					$duetime = $due[1];
					mysql_query("UPDATE Assessment SET Status = 1, DateFrom = '$dateToday', TimeFrom = '$timeToday', DateTo = '$duedate', TimeTo = '$duetime' WHERE AssessmentID = $assessID");
				}
			}
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$assessID");
		}
	} elseif($action == "deleteassessment") {
		if(isset($_POST['aid']) && isset($_POST['courseID'])) {
			$courseID = $_POST['courseID'];
			$aid = $_POST['aid'];
			mysql_query("DELETE FROM Assessment WHERE AssessmentID = $aid");
			mysql_query("DELETE FROM Question WHERE AssessmentID = $aid");
			header("Location: courses.php?id=$courseID&show=Assessment");
		}
	} elseif($action == "createquestion_trueorfalse") {
		if(isset($_POST['submit'])) {
			$id = $_POST['assessID'];
			$courseID = $_POST['courseID'];
			$question = mysql_real_escape_string(trim($_POST['question']));
			$answer = $_POST['answer'];
			$points = $_POST['points'];
			if($points == "") 
				$points = 1;
			mysql_query("INSERT INTO Question (AssessmentID, QuestionType, Question, Choices, Points) VALUES ('$id', 'True or False', '$question', '$answer', '$points')");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "editquestion_trueorfalse") {
		if(isset($_POST['submit'])) {
			$id = $_POST['assessID'];
			$courseID = $_POST['courseID'];
			$qid = $_POST['questionID'];
			$question = mysql_real_escape_string(trim($_POST['question']));
			$answer = $_POST['answer'];
			$points = $_POST['points'];
			if($points == "") 
				$points = 1;
			mysql_query("UPDATE Question SET Question = '$question', Choices = '$answer', Points = '$points' WHERE QuestionID = $qid");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "editquestion_multiplechoice") {
		if(isset($_POST['submit'])) {
			$qid = $_POST['questionID'];
			$id = $_POST['assessID'];
			$courseID = $_POST['courseID'];
			$question = mysql_real_escape_string(trim($_POST['question']));
			$answer = $_POST['answer'];
			$points = $_POST['points'];
			$choices = array($_SESSION["choices_$qid"][$answer]);
			for($i = 0; $i < sizeof($_SESSION["choices_$qid"]); $i++) {
				if($i != $answer) {
					array_push($choices, $_SESSION["choices_$qid"][$i]);
				}
			}
			$divider = "}); t.({";
			$choices1 = "t.({";
			for($i = 0; $i < sizeof($choices); $i++) {
				$choices1 = $choices1 . $choices[$i];
				if($i != sizeof($choices)-1) {
					$choices1 = $choices1 . $divider;
				}
			}
			$choices1 = $choices1."});";
			if($points == "") 
				$points = 1;
			mysql_query("UPDATE Question SET Question = '$question', Choices = '$choices1', Points = '$points' WHERE QuestionID = $qid");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "editquestion_essay") {
		if(isset($_POST['submit'])) {
			$id = $_POST['assessID'];
			$courseID = $_POST['courseID'];
			$qid = $_POST['questionID'];
			$question = mysql_real_escape_string(trim($_POST['question']));
			$chars = $_POST['chars'];
			$points = $_POST['points'];
			if($points == "") 
				$points = 1;
			mysql_query("UPDATE Question SET Question = '$question', Choices = '$chars', Points = '$points' WHERE QuestionID = $qid");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "loadchoices") {
		if(isset($_POST['questionid'])) {
			$qid = $_POST['questionid'];
			$query = mysql_query("SELECT Choices FROM Question WHERE QuestionID = $qid");
			while($row = mysql_fetch_array($query)) {
				$choices = $row['Choices'];
				$choices = substr($choices, 4);
				$choices = substr($choices, 0, -3);
				$choices = explode("}); t.({", $choices);
				$_SESSION["choices_$qid"] = array();
				for($i = 0; $i < sizeof($choices); $i++) {
					$_SESSION["choices_$qid"][] = $choices[$i];
				}
			}
		}
	} elseif($action == "refreshquestions") {
		if(isset($_POST['aid']) && isset($_POST['courseID'])) {
			$aid = $_POST['aid'];
			$courseID = $_POST['courseID'];
			$library['assessment']->GetQuestionsToAssessment($courseID, $aid, $skin);
		}
	} elseif($action == "addchoice") {
		if(isset($_POST['qid'])) {
			$choice = $_POST['choice'];
			$qid = $_POST['qid'];
			array_push($_SESSION["choices_$qid"], $choice);
		} elseif(isset($_SESSION['choices'])) {
			$choice = $_POST['choice'];
			array_push($_SESSION['choices'], $choice);
		} else {
			$choice = $_POST['choice'];
			$_SESSION['choices'] = array($choice);
		}
	} elseif($action == "refreshchoices") {
		if(isset($_POST['qid'])) {
			$qid = $_POST['qid'];
			$disabled = "";
			if(sizeof($_SESSION["choices_$qid"]) == 0)
				$disabled = " disabled";
?>
		<select name="answer" id="choicestoanswer_<?php echo $qid; ?>"<?php echo $disabled; ?>>
		<?php
		if(sizeof($_SESSION["choices_$qid"]) == 0)
			echo '<option value="no">-- You have not added any choices --</option>';
		
		for($i = 0; $i < sizeof($_SESSION["choices_$qid"]); $i++) {
			echo '<option value="'.$i.'">'.$_SESSION["choices_$qid"][$i].'</option>';
		}
		?>
		</select>
		<script>
		$(document).ready(function() {
			$('#btnRemoveChoice_<?php echo $qid; ?>').click(function() {
				$('#loadingDeleteChoice_<?php echo $qid; ?>').css({"display": "block"});
				$(this).css({"display": "none"});
				$selected_index = $("#choicestoanswer_<?php echo $qid; ?> option:selected").attr('value');
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=deletechoice",
					data: {index: $selected_index, qid: <?php echo $qid; ?>},
					success: function(html) {
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=refreshchoices",
							data: {qid: <?php echo $qid; ?>},
							success: function(html) {
								$('#popupChoices_<?php echo $qid; ?>').html(html);
								$('#loadingDeleteChoice_<?php echo $qid; ?>').css({"display": "none"});
								$('#btnRemoveChoice_<?php echo $qid; ?>').css({"display": "block"});
							}
						});
					}
				});
			});
		});
		</script>
		<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" id="loadingDeleteChoice_<?php echo $qid; ?>" style="display: none; margin: 16px 0px;" class="loadingGif"></center>
		<a class="inputbutton" id="btnRemoveChoice_<?php echo $qid; ?>" style="margin: 0px; margin-top: 20px;" disabled>Remove from Choices</a>
<?php
		} elseif(isset($_SESSION['choices'])) {
			$disabled = "";
			if(sizeof($_SESSION['choices']) == 0)
				$disabled = " disabled";
?>
		<select name="answer" id="choicestoanswer"<?php echo $disabled; ?>>
		<?php
		if(sizeof($_SESSION['choices']) == 0)
			echo '<option value="no">-- You have not added any choices --</option>';
		for($i = 0; $i < sizeof($_SESSION['choices']); $i++) {
			echo '<option value="'.$i.'">'.$_SESSION['choices'][$i].'</option>';
		}
		?>
		</select>
		<script>
		$(document).ready(function() {
			$('#btnRemoveChoice').click(function() {
				$('#loadingDeleteChoice').css({"display": "block"});
				$(this).css({"display": "none"});
				$selected_index = $("#choicestoanswer option:selected").attr('value');
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=deletechoice",
					data: {index: $selected_index},
					success: function() {
						$.ajax({
							cache: false,
							url: "process.php?action=refreshchoices",
							success: function(html) {
								$('#popupChoices').html(html);
								$('#loadingDeleteChoice').css({"display": "none"});
								$('#btnRemoveChoice').css({"display": "block"});
							}
						});
					}
				});
			});
		});
		</script>
		<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" id="loadingDeleteChoice" style="display: none; margin: 16px 0px;" class="loadingGif"></center>
		<a class="inputbutton" id="btnRemoveChoice" style="margin: 0px; margin-top: 20px;" disabled>Remove from Choices</a>
<?php
		}
	} elseif($action == "deletechoice") {
		if(isset($_POST['index']) && isset($_POST['qid'])) {
			$qid = $_POST['qid'];
			unset($_SESSION["choices_$qid"][$_POST['index']]);
			$_SESSION["choices_$qid"] = array_values($_SESSION["choices_$qid"]);
		} elseif(isset($_POST['index']) && isset($_SESSION['choices'])) {
			unset($_SESSION['choices'][$_POST['index']]);
			$_SESSION['choices'] = array_values($_SESSION['choices']);
		}
	} elseif($action == "createquestion_multiplechoice") {
		if(isset($_POST['submit'])) {
			$id = $_POST['assessID'];
			$courseID = $_POST['courseID'];
			$question = mysql_real_escape_string(trim($_POST['question']));
			$answer = $_POST['answer'];
			$points = $_POST['points'];
			$choices = array($_SESSION['choices'][$answer]);
			for($i = 0; $i < sizeof($_SESSION['choices']); $i++) {
				if($i != $answer) {
					array_push($choices, $_SESSION['choices'][$i]);
				}
			}
			$divider = "}); t.({";
			$choices1 = "t.({";
			for($i = 0; $i < sizeof($choices); $i++) {
				$choices1 = $choices1 . $choices[$i];
				if($i != sizeof($choices)-1) {
					$choices1 = $choices1 . $divider;
				}
			}
			$choices1 = $choices1."});";
			if($points == "") 
				$points = 1;
			mysql_query("INSERT INTO Question (AssessmentID, QuestionType, Question, Choices, Points) VALUES ('$id', 'Multiple Choice', '$question', '$choices1', '$points')");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "createquestion_essay") {
		if(isset($_POST['submit'])) {
			$id = $_POST['assessID'];
			$courseID = $_POST['courseID'];
			$question = mysql_real_escape_string(trim($_POST['question']));
			$min_chars = $_POST['chars'];
			if($min_chars == "") 
				$min_chars = 100;
			$points = $_POST['points'];
			if($points == "") 
				$points = 1;
			mysql_query("INSERT INTO Question (AssessmentID, QuestionType, Question, Choices, Points) VALUES ('$id', 'Essay', '$question', '$min_chars', '$points')");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "deletequestion") {
		if(isset($_POST['qid']) && isset($_POST['aid']) && isset($_POST['courseID'])) {
			$qid = $_POST['qid'];
			$aid = $_POST['aid'];
			$courseID = $_POST['courseID'];
			mysql_query("DELETE FROM Question WHERE QuestionID = $qid");
			header("Location: courses.php?id=$courseID&show=Assessment&aid=$id");
		}
	} elseif($action == "showpopup") {
		if(isset($_POST['popup'])) {
			$popup = $_POST['popup'];

			if(isset($_POST['date']))
				$date = $_POST['date'];

			if(isset($_POST['dateToday']))
				$dateToday = $_POST['dateToday'];

			if(isset($_POST['fixedCourseKey']))
				$fixedCourseKey = $_POST['fixedCourseKey'];

			if(isset($_POST['fixedGroupKey']))
				$fixedGroupKey = $_POST['fixedGroupKey'];

			if(isset($_POST['userID']))
				$userID = $_POST['userID'];

			if(isset($_POST['courseID']))
				$courseID = $_POST['courseID'];

			if(isset($_POST['groupID']))
				$groupID = $_POST['groupID'];

			if(isset($_POST['assessID']))
				$assessID = $_POST['assessID'];

			if(isset($_POST['postID']))
				$postID = $_POST['postID'];

			if(isset($_POST['parentfolder']))
				$parentfolder = $_POST['parentfolder'];

			require_once("popup/$popup.php");
		}
	} elseif($action == "copyassessment") {
		if(isset($_POST['submit'])) {
			echo 'copying...';
			$datecreated = date('Y-m-d');
			$courseID = $_POST['courseID'];
			$assessID = $_POST['assessID'];
			$name = $library['assessment']->GetAssessmentName($assessID);
			$attempts = $library['assessment']->GetAssessmentAttempts($assessID);
			$instructions = $library['assessment']->GetAssessmentInstruction($assessID);
			if($instructions == "")
				$instructions = "NULL";
			else
				$instructions = "'$instructions'";
			$timer = $library['assessment']->GetAssessmentTimer($assessID);
			mysql_query("INSERT INTO Assessment (Name, CourseID, Status, Attempts, Instructions, DateFrom, DateTo, TimeFrom, TimeTo, Timer, DateCreated) VALUES ('$name', '$courseID', '0', '$attempts', $instructions, NULL, NULL, NULL, NULL, '$timer', '$datecreated')");
			$query = mysql_query("SELECT AssessmentID FROM Assessment ORDER BY AssessmentID DESC LIMIT 1");
			while($row = mysql_fetch_array($query)) {
				$new_assessID = $row['AssessmentID'];
				$query2 = mysql_query("SELECT * FROM Question WHERE AssessmentID = $assessID");
				while($row2 = mysql_fetch_array($query2)) {
					$qtype = $row2['QuestionType'];
					$question = $row2['Question'];
					$choices = $row2['Choices'];
					$points = $row2['Points'];
					mysql_query("INSERT INTO Question (AssessmentID, QuestionType, Question, Choices, Points) VALUES ('$new_assessID', '$qtype', '$question', '$choices', '$points')");
				}
			}
			header("Location: courses.php?id=$courseID&show=Assessment");
		}
	} elseif($action == "getnumnotification") {
		if(isset($_POST['hash']) && $status == "LoggedIn") {
			$query = mysql_query("SELECT * FROM Notification WHERE ToUserID = $loggedUser AND isRead = 0"); 
			if(mysql_num_rows($query) >= 1) {
				echo '<span class="alert">'.mysql_num_rows($query).'</span>';
			}
		}
	} elseif($action == "gettitlenotification") {
		if(isset($_POST['hash']) && isset($_POST['bTitle']) && $status == "LoggedIn") {
			$bTitle = $_POST['bTitle'];
			$query = mysql_query("SELECT * FROM Notification WHERE ToUserID = $loggedUser AND isRead = 0"); 
			if(mysql_num_rows($query) >= 1) {
				echo '('.mysql_num_rows($query).') ';
			}
			echo $bTitle;
		}
	} elseif($action == "getnotification") {
		if(isset($_POST['hash'])) {
			$query = mysql_query("SELECT * FROM Notification WHERE ToUserID = $loggedUser ORDER BY NotificationID DESC"); 
			while($row = mysql_fetch_array($query)) {
				$fromID = $row['FromUserID'];
				$fromID = substr($fromID, 0, -2);
				$fromID = explode('-others({', $fromID);
				$andmore = explode(',', $fromID[1]);
				if(in_array($loggedUser, $andmore)) {
					$numUsers = sizeof($andmore) - 2;
					if($numUsers > 1) {
						$andmore = ' and '.$numUsers;
						if($numUsers == 1) {
							$andmore .= ' other ';
						} else {
							$andmore .= ' others ';
						}
					} else {
						$andmore = "";
					}
				} else {
					$numUsers = sizeof($andmore);
					if($numUsers > 1) {
						$andmore = ' and '.($numUsers-1);
						if($numUsers-1 == 1) {
							$andmore .= ' other ';
						} else {
							$andmore .= ' others ';
						}
					} else {
						$andmore = "";
					}
				}
				$dp = $library['user']->GetProfilePicture($fromID[0], 50);
				$action = $row['Action'];
				$whereID = $row['WhereID'];
				$whereFromWhat = $row['WhereFromWhat'];
				$toUserID = $row['ToUserID'];
				$isRead = $row['isRead'];
				if($isRead == 0)
					$isRead = ' class="unread"';
				else
					$isRead = '';
				$datenotif = $row['DateNotif'];
				$timenotif = $row['TimeNotif'];
				$timeInterval = $library['newsfeed']->GetDateTimeInterval($datenotif, $timenotif);
				if($whereFromWhat == "Post") {
					$query1 = mysql_query("SELECT * FROM Post WHERE PostID = $whereID");
					$row1 = mysql_fetch_array($query1);
					$posterFromID = $row1['UserID'];
					$posterToID = $row1['ID'];
					$postFromWhat = $row1['FromWhat'];
					if($postFromWhat == "Newsfeed") {
						$link = "index.php?-_-=".$library['user']->GetUsername($posterToID)."&post=$whereID";
						if($action == "likeacomment") {
							$query = mysql_query("SELECT * FROM Post WHERE PostID = $whereID");
							$row = mysql_fetch_array($query);
							$referTo = $row['ReferTo'];
							$link = "index.php?-_-=".$library['user']->GetUsername($posterToID)."&post=$referTo";
						}
						if($action == "posted") {
							if($posterToID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on your profile.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} else {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $library['user']->GetName($posterToID); ?>'s profile.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "commented") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on your post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} elseif($fromID[0] == $posterFromID) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $library['user']->GetThirdPersonWord($fromID[0]); ?> post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} else {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $library['user']->GetName($fromID[0]); ?> post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "likeapost") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' liked'; ?> your post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "likeacomment") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' liked'; ?> your comment.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						}
					} elseif($postFromWhat == "Course") {
						$link = "courses.php?id=$posterToID&post=$whereID";
						if($action == "likeacomment") {
							$query = mysql_query("SELECT * FROM Post WHERE PostID = $whereID");
							$row = mysql_fetch_array($query);
							$referTo = $row['ReferTo'];
							$link = "courses.php?id=$posterToID&post=$referTo";
						}
						if($action == "posted") {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on course <b><?php echo $library['course']->GetCourseName($posterToID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($action == "commented") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on your post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} elseif($fromID[0] == $posterFromID) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $fromID[0];//$library['user']->GetThirdPersonWord($fromID[0]); ?> post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} else {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $library['user']->GetName($fromID[0]); ?> post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "likeapost") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' likes'; ?> your post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "likeacomment") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' likes'; ?> your comment.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} 
					} elseif($postFromWhat == "Group") {
						$link = "groups.php?id=$posterToID&post=$whereID";
						if($action == "likeacomment") {
							$query = mysql_query("SELECT * FROM Post WHERE PostID = $whereID");
							$row = mysql_fetch_array($query);
							$referTo = $row['ReferTo'];
							$link = "groups.php?id=$posterToID&post=$referTo";
						}
						if($action == "posted") {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on course <b><?php echo $library['group']->GetGroupName($posterToID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($action == "commented") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on your post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} elseif($fromID[0] == $posterFromID) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $library['user']->GetThirdPersonWord($fromID[0]); ?> post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							} else {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' '.$action; ?> on <?php echo $library['user']->GetName($fromID[0]); ?> post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "likeapost") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' likes'; ?> on your post.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						} elseif($action == "likeacomment") {
							if($posterFromID == $loggedUser) {
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' likes'; ?> on your comment.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
							}
						}
					}
				} elseif($whereFromWhat == "Request") {
					if($action == "joinrequest") {
						$fromWhat = $row['ActionFromWhat'];
						if($fromWhat == "Course") {
							$courseID = $row['ActionID'];
							$link = "courses.php?id=$courseID&show=Request";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' requested to join'; ?> <b><?php echo $library['course']->GetCourseName($courseID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($fromWhat == "Group") {
							$groupID = $row['ActionID'];
							$link = "groups.php?id=$groupID&show=Request";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' requested to join'; ?> <b><?php echo $library['group']->GetGroupName($groupID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						}
					} elseif($action == "acceptrequest") {
						$fromWhat = $row['ActionFromWhat'];
						if($fromWhat == "Course") {
							$courseID = $row['ActionID'];
							$link = "courses.php?id=$courseID";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' accepted you to enroll'; ?> <b><?php echo $library['course']->GetCourseName($courseID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($fromWhat == "Group") {
							$groupID = $row['ActionID'];
							$link = "groups.php?id=$groupID";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' accepted you to join'; ?> <b><?php echo $library['group']->GetGroupName($groupID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						}
					} elseif($action == "rejectrequest") {
						$fromWhat = $row['ActionFromWhat'];
						if($fromWhat == "Course") {
							$courseID = $row['ActionID'];
							$link = "courses.php?id=$courseID";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' rejected you to enroll'; ?> <b><?php echo $library['course']->GetCourseName($courseID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($fromWhat == "Group") {
							$groupID = $row['ActionID'];
							$link = "groups.php?id=$groupID";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' rejected you to join'; ?> <b><?php echo $library['group']->GetGroupName($groupID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						}
					} elseif($action == "joined") {
						$fromWhat = $row['ActionFromWhat'];
						if($fromWhat == "Course") {
							$courseID = $row['ActionID'];
							$link = "courses.php?id=$courseID&show=People";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' enrolled to'; ?> <b><?php echo $library['course']->GetCourseName($courseID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($fromWhat == "Group") {
							$groupID = $row['ActionID'];
							$link = "groups.php?id=$groupID&show=People";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' joined to'; ?> <b><?php echo $library['group']->GetGroupName($groupID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						}
					} elseif($action == "invited") {
						$fromWhat = $row['ActionFromWhat'];
						if($fromWhat == "Course") {
							$courseID = $row['ActionID'];
							$link = "courses.php?id=$courseID&show=People";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' enrolled you to'; ?> <b><?php echo $library['course']->GetCourseName($courseID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						} elseif($fromWhat == "Group") {
							$groupID = $row['ActionID'];
							$link = "groups.php?id=$groupID&show=People";
?>
					<a href="<?php echo $link; ?>"<?php echo $isRead; ?>><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div>
					<?php echo $library['user']->GetName($fromID[0]).$andmore.' joined you to'; ?> <b><?php echo $library['group']->GetGroupName($groupID); ?></b>.
					<br><small><?php echo $timeInterval; ?></small></a>
<?php
						}
					}
				}
			}
			if(mysql_num_rows($query) == 0) {
				echo '<span style="display: block; width: 100%; text-align: center;color: white; font-size: 12px; font-style: italic; padding: 10px 0px; box-sizing: border-box;">You have no notification.</span>';
			}
		}
	} elseif($action == "readnotification") {
		if(isset($_POST['hash'])) {
			mysql_query("UPDATE Notification SET isRead = '1' WHERE ToUserID = '$loggedUser'");
		}
	} elseif($action == "topsearch") {
		if(isset($_POST['searchQuery'])) {
			$searchQuery = $_POST['searchQuery'];
			$resultsDisplayed = 0;
			$maxResultsPerCat = 5;
			$searchPeople = mysql_query("SELECT * FROM Account WHERE FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR CONCAT(FirstName, ' ', LastName) LIKE '%$searchQuery%'");
			$searchCourse = mysql_query("SELECT * FROM Course WHERE Name LIKE '%$searchQuery%'");
			$searchGroup = mysql_query("SELECT * FROM GroupBlock WHERE Name LIKE '%$searchQuery%'");
			$found = false;
			$numRows = array("people" => mysql_num_rows($searchPeople),
							 "course" => mysql_num_rows($searchCourse),
							 "group" => mysql_num_rows($searchGroup));
			if($numRows['people'] > 0) {
				$found = true;
				echo '<h5>People</h5>';
				while($row = mysql_fetch_array($searchPeople)) {
					if($resultsDisplayed < $maxResultsPerCat) {
						$userid = $row['ID'];
						$name = $row['FirstName'].' '.$row['LastName'];
						$username = $row['Username'];
						$dp = $library['user']->GetProfilePicture($userid, 50);
						echo '<a href="index.php?-_-='.$username.'"><div class="profpic" style="background-image: url('.$dp.');"></div><b>'.$name.'</b><br><small>'.$username.'</small></a>';
						$resultsDisplayed++;
					} else {
						echo '<a href="search.php?q='.$searchQuery.'&cat=People" style="text-align: right; font-size: 10px; font-weight: bold; text-transform: uppercase;">More results...</a>';
						break;
					}
				}
			}
			$resultsDisplayed = 0;
			if($numRows['course'] > 0) {
				$found = true;
				echo '<h5>Courses</h5>';
				while($row = mysql_fetch_array($searchCourse)) {
					if($resultsDisplayed < $maxResultsPerCat) {
						$courseID = $row['CourseID'];
						$name = $row['Name'];
						$type = $library['course']->GetCourseType($courseID).' Course';
						$dp = $library['course']->GetProfilePicture($courseID, 50);
						echo '<a href="courses.php?id='.$courseID.'"><div class="profpic" style="background-image: url('.$dp.');"></div><b>'.$name.'</b><br><small>'.$type.'</small></a>';
						$resultsDisplayed++;
					} else {
						echo '<a href="search.php?q='.$searchQuery.'&cat=Courses" style="text-align: right; font-size: 10px; font-weight: bold; text-transform: uppercase;">More results...</a>';
						break;
					}
				}
			}
			$resultsDisplayed = 0;
			if($numRows['group'] > 0) {
				$found = true;
				echo '<h5>Groups</h5>';
				while($row = mysql_fetch_array($searchGroup)) {
					if($resultsDisplayed < $maxResultsPerCat) {
						$groupID = $row['GroupID'];
						$name = $row['Name'];
						$type = $library['group']->GetGroupType($groupID).' Group';
						$dp = $library['group']->GetProfilePicture($groupID, 50);
						echo '<a href="groups.php?id='.$groupID.'"><div class="profpic" style="background-image: url('.$dp.');"></div><b>'.$name.'</b><br><small>'.$type.'</small></a>';
						$resultsDisplayed++;
					} else {
						echo '<a href="search.php?q='.$searchQuery.'&cat=Groups" style="text-align: right; font-size: 10px; font-weight: bold; text-transform: uppercase;">More results...</a>';
						break;
					}
				}
			}
			if(!$found) {
				echo '<h5>Search not found.</h5>';
			}
		}
	} else {
		header("Location: index.php");
	}
} else {
	header("Location: index.php");
}
?>