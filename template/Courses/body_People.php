<?php
if($isGuest || $userType == "Student") {
?>
		<div id="bigbody">
			<script>
			$(document).ready(function () {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=getpeopleoncourse",
					data: {id: <?php echo $id; ?>},
					success: function(html) {
						$('#listPeopleCourses').html(html);
					}
				});
			});
			$refreshlistPeopleCourses = 0;
			$(document).everyTime(1000, function() {
				if($refreshlistPeopleCourses == 1) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getpeopleoncourse",
						data: {id: <?php echo $id; ?>},
						success: function(html) {
							$('#listPeopleCourses').html(html);
						}
					});
					$refreshlistPeopleCourses = 0;
				}
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>People</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<div id="listPeopleCourses">
					<br><br><center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif" /></center><br>
				</div>
			</div>
		</div>
<?php
} else {
?>
		<div id="body">
			<script>
			$(document).ready(function () {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=getpeopleoncourse",
					data: {id: <?php echo $id; ?>},
					success: function(html) {
						$('#listPeopleCourses').html(html);
					}
				});
			});
			$refreshlistPeopleCourses = 0;
			$(document).everyTime(1000, function() {
				if($refreshlistPeopleCourses == 1) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getpeopleoncourse",
						data: {id: <?php echo $id; ?>},
						success: function(html) {
							$('#listPeopleCourses').html(html);
						}
					});
					$refreshlistPeopleCourses = 0;
				}
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>People</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<div id="listPeopleCourses">
					<br><br><center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif" /></center><br>
				</div>
			</div>
		</div>
		<div id="sb2">
		<?php if($userType == "Faculty") require_once('widgets/course_AddPeople.php'); ?>
		<?php if($userType == "Faculty") require_once('widgets/course_list_Request.php'); ?>
		</div>
<?php
}
?>