<?php
if($show != "Gradesheet") {
?>
		<div id="sb1left">
			<div class="content">
				<table class="title"><tr>
					<td><a href="courses.php?id=<?php echo $id; ?>"><?php $coursename = $library['course']->GetCourseName($id); if(strlen($coursename) >= 15) $coursename = substr($coursename, 0, 15).'...'; echo $coursename; ?></a></td>
					
					<script>
					$(document).ready(function() {
						$('#btnPopup_EditCourse').click(function() {
							showPopup();
							$showPopup = "EditCourse";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'course_'+$showPopup, courseID: <?php echo $id; ?>},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<td><?php if($userType == "Faculty") echo '<a id="btnPopup_EditCourse" title="Course Settings" class="settingsicon"></a>'; ?></td>
				</tr></table>
				<hr>
				<?php 
				$dp = $library['course']->GetProfilePicture($id, 200);
				$courseType = $library['course']->GetCourseType($id);
				?>
				<div class="groupprofilepicture" style="background-image: url(<?php echo $dp; ?>)">
				</div>
				<?php
				if(!$isGuest) {
					if($library['course']->GetCourseKey($id) != "") {
						echo '
					<p class="text"><b>Course Key:</b> '.$library['course']->GetCourseKey($id).'</p><hr>';
					}
					if ($library['course']->GetCourseDescription($id) != "")
						echo '
					<p class="text wrap">'.nl2br(htmlentities($library['course']->GetCourseDescription($id))).'</p><hr>';
				} elseif($isGuest) {
					if($courseType == "Closed") {
						$msg = "Only faculty and students with the Course Key can enroll to this course.";
						$fixedCourseKey = $id;
						$callEnrollCourse = 'id="btnPopup_EnrollCourse" ';
						$btnEnrollCourse_Text = "Enroll Course";
				?>
				<script>
				$(document).ready(function() {
					$('#btnPopup_EnrollCourse').click(function() {
						showPopup();
						$showPopup = "EnrollCourse";
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=showpopup",
							data: {popup: 'course_'+$showPopup, fixedCourseKey: <?php echo $id; ?>,},
							success: function(html) {
								$('#Popup').html(html);
								$heightPopup = $('div#popup_'+$showPopup).height()+5;
								$('#Popup').css({
									"width": "450px",
									"height": ($heightPopup)+"px",
									"margin-left": "-225px",
									"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
								});
								popup = 1;
							}
						});
					});
				});
				</script>
				<?php
					} else {
						$msg = "Anyone can enroll to this course with the permission of the faculty of the course.";
						$callEnrollCourse = 'id="btnEnrollCourse" ';
						$isRequested = $library['course']->IsRequested($id, $loggedUser);
						$btnEnrollCourse_Text = "Enroll Course";
						if($isRequested) 
							$btnEnrollCourse_Text = "Request Sent";
				?>
				<script>
				$(document).ready(function() {
					$('#btnEnrollCourse').click(function() {
						$text = $('#btnEnrollCourse').text();
						if($text == "Enroll Course") {
							$('#btnEnrollCourse').text('Sending Request...');
							$.ajax({
								type: "POST",
								cache: false,
								url: "sendCourseRequest.php",
								data: {courseID: <?php echo $id; ?>},
								success: function() {
									$('#btnEnrollCourse').text('Request Sent');
								}
							});
						} else if ($text == "Cancel Request") {
							$.ajax({
								type: "POST",
								cache: false,
								url: "deleteCourseRequest.php",
								data: {courseID: <?php echo $id; ?>},
								success: function() {
									$('#btnEnrollCourse').text('Enroll Course');
								}
							});
						}
					})
					.mouseover(function() {
						$text = $('#btnEnrollCourse').text();
						if($text == "Request Sent")
							$('#btnEnrollCourse').text('Cancel Request');
					})
					.mouseleave(function() {
						$text = $('#btnEnrollCourse').text();
						if($text == "Cancel Request")
							$('#btnEnrollCourse').text('Request Sent');
					});
				});
				</script>
				<?php
					}
					echo '<p class="text"><b>Course Type:</b> '.$library['course']->GetCourseType($id).' Course</p><hr><p class="text">'.$msg.'
					<a '.$callEnrollCourse.'style="margin-bottom: 0px; margin-top: 10px;" class="inputbutton">'.$btnEnrollCourse_Text.'</a></p>';
				}
				?>
			</div>
			<?php if(!$isGuest) require_once($toRoot.'template/Courses/sidebarCourseMenu.php'); ?>
		</div>
		<?php 
		$pages = array('Assessment','Resources','People','Calendar', 'Request');
		if($isGuest) {
			require_once($toRoot.'template/Courses/body_'.$show.'.php');
		} else {
			if($page_authorization[$show] == 0) {
				require_once($toRoot.'template/Courses/body_'.$show.'.php');
			} elseif ($page_authorization[$show] == 1) {
				require_once($toRoot.'template/Courses/body_'.$show.'.php');
			} elseif ($page_authorization[$show] == 2) {
				$bothStudentandFaculty = array('People', 'Calendar');
				if(in_array($show, $bothStudentandFaculty))
					require_once($toRoot.'template/Courses/body_'.$show.'.php');
				else
					require_once($toRoot.'template/Courses/body_'.$userType.$show.'.php');
			}
		}
		?>
<?php
} elseif($show == "Gradesheet") {
	require_once($toRoot.'template/Courses/body_'.$show.'.php');
}
?>