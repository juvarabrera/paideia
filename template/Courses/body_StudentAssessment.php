
		<div id="bigbody">
			<div class="content">
				<table class="title">
					<tr>
						<td>Assessment</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<table class="list">
					<tr class="title">
						<td>Name</td>
						<td>Status</td>
						<td>Start</td>
						<td>Due</td>
						<td>Overall Points</td>
						<td>Action</td>
					</tr>
					<?php
					$row = $library['assessment']->GetCourseActiveAssessment($id);
					for($i = 0; $i < sizeof($row); $i++) {
						$a_name = $library['assessment']->GetAssessmentName($row[$i]);
						$a_status = $library['assessment']->GetAssessmentStatus($row[$i]);
						$a_datefrom = $library['assessment']->GetAssessmentStartDate($row[$i]);
						if($a_datefrom != "") {
							$a_datefrom = date('M. j', strtotime($a_datefrom));
						} else {
							$a_datefrom = "-";
						}
						$a_dateto = $library['assessment']->GetAssessmentDueDate($row[$i]);
						if($a_dateto != "") {
							$a_dateto = date('M. j', strtotime($a_dateto));
						}
						$a_timefrom = $library['assessment']->GetAssessmentStartTime($row[$i]);
						if($a_timefrom != "") {
							$a_timefrom = date('h:i a', strtotime($a_timefrom));
						}
						$a_timeto = $library['assessment']->GetAssessmentDueTime($row[$i]);
						if($a_timeto != "") {
							$a_timeto = date('h:i a', strtotime($a_timeto));
						}
						$a_overallpoints = $library['assessment']->GetOverallPoints($row[$i]);
					?>
					<tr>
						<td><?php echo $a_name; ?></td>
						<td><?php echo $a_status; ?></td>
						<td><b><?php echo $a_datefrom; ?></b><br><?php echo $a_timefrom; ?></td>
						<td><b><?php echo $a_dateto; ?></b><br><?php echo $a_timeto; ?></td>
						<td><?php echo $a_overallpoints; ?></td>
						<td><a href="courses.php?id=<?php echo $id; ?>&show=Assessment&aid=<?php echo $row[$i]; ?>" class="mini_icons view" title="View"></a></td>
					</tr>
					<?php
					}
					if(sizeof($row) == 0) {
						echo '<tr><td colspan="6"><center>There are no assessments in this course.</center></td></tr>';
					}
					?>
				</table>
			</div>
		</div>