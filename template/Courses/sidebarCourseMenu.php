<div class="content">
				<table class="title">
					<tr>
						<td>Courses Menu</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<ul class="contentlist">
					<a href="courses.php?id=<?php echo $id ?>">News</a>
					<a href="courses.php?id=<?php echo $id ?>&show=Assessment">Assessment</a>
					<?php
					if($userType == "Faculty") {
						echo '<a href="courses.php?id='.$id.'&show=Gradesheet">Grade Sheet</a>';
					}
					?>
					<a href="courses.php?id=<?php echo $id ?>&show=Resources">Resources</a>
					<a href="courses.php?id=<?php echo $id ?>&show=People">People</a>
					<a href="courses.php?id=<?php echo $id ?>&show=Calendar">Calendar</a>
				</ul>
			</div>