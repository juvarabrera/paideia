<?php
if(isset($aid)) {
$a_name = $library['assessment']->GetAssessmentName($aid);


$a_status = $library['assessment']->GetAssessmentStatus($aid);
if($a_status == "Active") {
	$a_status = '<span style="color: #007a5c;font-weight: bold;">'.$a_status.'</span>';
} else {
	$a_status = '<span style="color: red;font-weight: bold;">'.$a_status.'</span>';
}

$a_datefrom = $library['assessment']->GetAssessmentStartDate($aid);
if($a_datefrom != "") {
	$a_datefrom_Show = date('F j, Y', strtotime($a_datefrom));
} else {
	$a_datefrom_Show = "-";
}


$a_dateto = $library['assessment']->GetAssessmentDueDate($aid);
if($a_dateto != "") {
	$a_dateto_Show = date('F j, Y', strtotime($a_dateto));
} else {
	$a_dateto_Show = "-";
}


$a_timefrom = $library['assessment']->GetAssessmentStartTime($aid);
$a_timefrom_Show = "";
if($a_timefrom != "" && $a_datefrom_Show != "-") {
	$a_timefrom_Show = ' · '.date('h:i a', strtotime($a_timefrom));
}


$a_timeto = $library['assessment']->GetAssessmentDueTime($aid);
$a_timeto_Show = "";
if($a_timeto != "" && $a_dateto_Show != "-") {
	$a_timeto_Show = ' · '.date('h:i a', strtotime($a_timeto));
}


$a_overallpoints = $library['assessment']->GetOverallPoints($aid);

$a_attempts = $library['assessment']->GetAssessmentAttempts($aid);
if($a_attempts > 1)
	$a_attempts = "$a_attempts attempts";
else
	$a_attempts = "$a_attempts attempt";


$a_timer = $library['assessment']->GetAssessmentTimer($aid);
if($a_timer != "") {
	if(date('G', strtotime($a_timer)) == 0) {
		if(date('i', strtotime($a_timer)) == 1) {
			$a_timer_Show = ltrim(date('i', strtotime($a_timer)), '0').' minute';
		} else {
			$a_timer_Show = ltrim(date('i', strtotime($a_timer)), '0').' minutes';
		}
	} else {
		if(date('G', strtotime($a_timer)) == 1) {
			$a_timer_Show = ltrim(date('G', strtotime($a_timer)), '0').' hour';
		} else {
			$a_timer_Show = ltrim(date('G', strtotime($a_timer)), '0').' hours';
		}
		if(date('i', strtotime($a_timer)) == 1) {
			$a_timer_Show = $a_timer_Show.' and '.ltrim(date('i', strtotime($a_timer)), '0').' minute';
		} elseif(date('i', strtotime($a_timer)) > 1) {
			$a_timer_Show = $a_timer_Show.' and '.ltrim(date('i', strtotime($a_timer)), '0').' minutes';
		} 
	}
	$a_timer_Show.= ' to answer';
}


$a_instructions = $library['assessment']->GetAssessmentInstruction($aid);
$a_instructions_Show = nl2br(htmlentities($a_instructions));;
if($a_instructions == "") {
	$a_instructions_Show = "<i>No instructions defined</i>";
}

$a_datecreated = date('M. d, Y', strtotime($library['assessment']->GetAssessmentDateCreated($aid)));
?>
		<div id="body">
			<div id="EditAssessment" style="display: none;">
			<form action="process.php?action=editassessmentinfo" method="POST">
			<div class="content">
				<table class="title">
					<tr>
						<td>Edit Assessment Information</td>
						<td><a id="btnCancelEdit" title="Cancel edit" class="mini_icons close"></a></td>
					</tr>
				</table>
				<hr>
				<table class="form">
					<tr class="borderbottom">
						<td><b>Name: </b></td><td><input type="text" name="name" value="<?php echo $a_name; ?>" required></td>
					</tr>
					<tr>
						<td><b>Maximum Attempts: </b></td><td>
						<select style="width: 100%;" name="attempts">
							<?php
							for($i = 1; $i <= 10; $i++) {
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
							?>
						</select>
						</td>
					</tr>
					<tr>
						<td><b>Timer: </b></td>
						<td>
						<?php
						$a_timer_input = explode(':', $a_timer);
						?>
						<table class="nopad">
							<tr>
								<td><input type="number" value="<?php echo ltrim($a_timer_input[0], '0'); ?>" max="5" min="0" name="timer_Hours" id="timer_Hours" placeholder="Hours"></td>
								<td>h</td>
								<td><input type="number" value="<?php echo ltrim($a_timer_input[1], '0'); ?>" max="59" min="0" name="timer_Minutes" id="timer_Minutes" placeholder="Minutes"></td>
								<td>m</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr valign="top">
						<td><b>Instructions: </b></td><td><textarea class="resizabletext" placeholder="No instructions defined" style="resize: none; min-height: 100px; max-height: 200px;" name="instructions"><?php echo $a_instructions; ?></textarea></td>
					</tr>
				</table>
			</div>
			<input type="hidden" name="courseID" value="<?php echo $id; ?>">
			<input type="hidden" name="assessID" value="<?php echo $aid; ?>">
			<input type="submit" name="submit" value="Save" style="margin-bottom: 10px;">
			<a id="btnCancelEdit1" class="inputbutton delete">Cancel</a>
			</form>
			</div>
			<script>
			$(document).ready(function() {
				$('[type="number"]').keydown(function (e) {
					if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
						(e.keyCode == 65 && e.ctrlKey === true) || 
						(e.keyCode >= 35 && e.keyCode <= 39)) {
						return;
					}
					if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
						e.preventDefault();
					}
				});
				$('#btnEditAssessment').click(function () {
					$('#EditAssessment').css({
						"display": "block"
					});
					$('#AssessmentInfo').css({
						"display": "none"
					});
					$('#AssessmentQuestions').css({
						"display": "none"
					});
				});
				$('#btnCancelEdit').click(function () {
					$('#EditAssessment').css({
						"display": "none"
					});
					$('#AssessmentInfo').css({
						"display": "block"
					});
					$('#AssessmentQuestions').css({
						"display": "block"
					});
				});
				$('#btnCancelEdit1').click(function () {
					$('#EditAssessment').css({
						"display": "none"
					});
					$('#AssessmentInfo').css({
						"display": "block"
					});
					$('#AssessmentQuestions').css({
						"display": "block"
					});
				});
			});
			</script>
			<div id="AssessmentInfo">
			<div class="content">
				<table class="title">
					<tr>
						<td>Assessment Information</td>
						<td><a id="btnEditAssessment" title="Edit Assessment Settings" class="settingsicon"></a></td>
					</tr>
				</table>
				<hr>
				<table class="list">
					<tr class="borderbottom">
						<td><b>Name: </b></td><td><?php echo $a_name; ?></td>
					</tr>
					<tr>
						<td><b>Status: </b></td><td><?php echo $a_status; ?></td>
					</tr>
					<tr>
						<td><b>Maximum Attempts: </b></td><td><?php echo $a_attempts; ?></td>
					</tr>
					<tr>
						<td><b>Start: </b></td><td><b><?php echo $a_datefrom_Show; ?></b><?php echo $a_timefrom_Show; ?></td>
					</tr>
					<tr>
						<td><b>Due: </b></td><td><b><?php echo $a_dateto_Show; ?></b><?php echo $a_timeto_Show; ?></td>
					</tr>
					<tr>
						<td><b>Timer: </b></td><td><?php echo $a_timer_Show; ?></td>
					</tr>
					<tr valign="top">
						<td><b>Instructions: </b></td><td><?php echo $a_instructions_Show; ?></td>
					</tr>
				</table>
			</div>
			</div>
			<div id="AssessmentQuestions">
			<div class="content" style="margin-bottom:500px;">
				<table class="title">
					<tr>
						<td>List of Questions</td>
						<td>
						
						<span><a class="mini_icons add"></a>
						<ul>
							<h4>Add Question</h4>
							<script>
							$(document).ready(function() {
								$('#btnPopup_CreateQuestion_TrueOrFalse').click(function() {
									showPopup();
									$showPopup = "CreateQuestion_TrueOrFalse";
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=showpopup",
										data: {popup: 'course_'+$showPopup, assessID: <?php echo $aid; ?>, courseID: <?php echo $id; ?>},
										success: function(html) {
											$('#Popup').html(html);
											$heightPopup = $('div#popup_'+$showPopup).height()+5;
											$('#Popup').css({
												"width": "450px",
												"height": ($heightPopup)+"px",
												"margin-left": "-225px",
												"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
											});
											popup = 1;
										}
									});
								});
							});
							</script>
							<a id="btnPopup_CreateQuestion_TrueOrFalse">True or False</a>


							<script>
							$(document).ready(function() {
								$('#btnPopup_CreateQuestion_MultipleChoice').click(function() {
									showPopup();
									$showPopup = "CreateQuestion_MultipleChoice";
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=showpopup",
										data: {popup: 'course_'+$showPopup, assessID: <?php echo $aid; ?>, courseID: <?php echo $id; ?>},
										success: function(html) {
											$('#Popup').html(html);
											$heightPopup = $('div#popup_'+$showPopup).height()+5;
											$('#Popup').css({
												"width": "450px",
												"height": ($heightPopup)+"px",
												"margin-left": "-225px",
												"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
											});
											popup = 1;
										}
									});
								});
							});
							</script>
							<a id="btnPopup_CreateQuestion_MultipleChoice">Multiple Choice</a>


							<script>
							$(document).ready(function() {
								$('#btnPopup_CreateQuestion_Essay').click(function() {
									showPopup();
									$showPopup = "CreateQuestion_Essay";
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=showpopup",
										data: {popup: 'course_'+$showPopup, assessID: <?php echo $aid; ?>, courseID: <?php echo $id; ?>},
										success: function(html) {
											$('#Popup').html(html);
											$heightPopup = $('div#popup_'+$showPopup).height()+5;
											$('#Popup').css({
												"width": "450px",
												"height": ($heightPopup)+"px",
												"margin-left": "-225px",
												"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
											});
											popup = 1;
										}
									});
								});
							});
							</script>
							<a id="btnPopup_CreateQuestion_Essay">Essay</a>
						</ul>
						</span></td>
					</tr>
				</table>
				<hr>
				<script>
				$(document).ready(function() {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=refreshquestions",
						data: {aid: <?php echo $aid; ?>, courseID: <?php echo $id; ?>},
						success: function(html) {
							$('#QuestionsTo_<?php echo $aid; ?>').html(html);
						}
					});
				});
				</script>
				<div id="QuestionsTo_<?php echo $aid; ?>">
					<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif margintop10"></center>
				</div>
			</div>
			</div>
		</div>
		<div id="sb2">
			<a href="courses.php?id=<?php echo $id; ?>&show=Assessment" class="inputbutton">Back to Assessments</a>
			<div class="content">
				<table class="title">
					<tr>
						<td>Actions</td>
						<td>
						</td>
					</tr>
				</table>
				<script>
				$(document).ready(function() {
					$('#btnPopup_CopyAssessment').click(function() {
						showPopup();
						$showPopup = "CopyAssessment";
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=showpopup",
							data: {popup: 'course_'+$showPopup, userID: <?php echo $loggedUser; ?>, assessID: <?php echo $aid; ?>},
							success: function(html) {
								$('#Popup').html(html);
								$heightPopup = $('div#popup_'+$showPopup).height()+5;
								$('#Popup').css({
									"width": "450px",
									"height": ($heightPopup)+"px",
									"margin-left": "-225px",
									"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
								});
								popup = 1;
							}
						});
					});
				});
				</script>
				<a id="btnPopup_CopyAssessment" class="assessment_mini_icons copyicon"></a>


				<script>
				$(document).ready(function() {
					$('#btnPopup_GiveAssessment').click(function() {
						showPopup();
						$showPopup = "GiveAssessment";
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=showpopup",
							data: {popup: 'course_'+$showPopup, assessID: <?php echo $aid; ?>, dateToday: "<?php echo date('Y-m-d'); ?>"},
							success: function(html) {
								$('#Popup').html(html);
								$heightPopup = $('div#popup_'+$showPopup).height()+5;
								$('#Popup').css({
									"width": "450px",
									"height": ($heightPopup)+"px",
									"margin-left": "-225px",
									"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
								});
								popup = 1;
							}
						});
					});
				});
				</script>
				<a id="btnPopup_GiveAssessment" class="assessment_mini_icons giveicon"></a>
				<a class="assessment_mini_icons copyicon"></a>
				<script>
				$(document).ready(function() {
					$('#btnDeleteAssessment').click(function() {
						var e = confirm("Are you sure you want to delete this assessment?\n\nNote: You cannot undo this action.");
						if(e) {
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=deleteassessment",
								data: {courseID: <?php echo $id; ?>, aid: <?php echo $aid; ?>},
								success: function() {
									location.reload();
								}
							});
						}
					});
				});
				</script>
				<a id="btnDeleteAssessment" class="assessment_mini_icons deleteicon"></a>
				<hr>
				<p class="text">
					<b>Date Created:</b> <?php echo $a_datecreated; ?>
				</p>
			</div>
			<div class="content">
				<table class="title">
					<tr>
						<td>Unfinished</td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
<?php
} else {
?>
		<div id="bigbody">
			<div class="content">
				<table class="title">
					<tr>
						<td>Assessment</td>
						<script>
						$(document).ready(function() {
							$('#btnPopup_CreateAssessment').click(function() {
								showPopup();
								$showPopup = "CreateAssessment";
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=showpopup",
									data: {popup: 'course_'+$showPopup, courseID: <?php echo $id; ?>},
									success: function(html) {
										$('#Popup').html(html);
										$heightPopup = $('div#popup_'+$showPopup).height()+5;
										$('#Popup').css({
											"width": "450px",
											"height": ($heightPopup)+"px",
											"margin-left": "-225px",
											"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
										});
										popup = 1;
									}
								});
							});
						});
						</script>
						<td><a id="btnPopup_CreateAssessment" class="mini_icons add"></a></td>
					</tr>
				</table>
				<hr>
				<table class="list">
					<tr class="title">
						<td>Name</td>
						<td>Status</td>
						<td>Start</td>
						<td>Due</td>
						<td>No. of Questions</td>
						<td>Overall Points</td>
						<td>Action</td>
					</tr>
					<?php
					$row = $library['assessment']->GetCourseAssessment($id);
					for($i = 0; $i < sizeof($row); $i++) {
						$a_name = $library['assessment']->GetAssessmentName($row[$i]);
						$a_status = $library['assessment']->GetAssessmentStatus($row[$i]);
						if($a_status == "Active") {
							$a_status = '<span style="color: #007a5c;font-weight: bold;">'.$a_status.'</span>';
						} else {
							$a_status = '<span style="color: red;font-weight: bold;">'.$a_status.'</span>';
						}
						$a_datefrom = $library['assessment']->GetAssessmentStartDate($row[$i]);
						if($a_datefrom != "") {
							$a_datefrom = date('M. j', strtotime($a_datefrom));
						} else {
							$a_datefrom = "-";
						}
						$a_dateto = $library['assessment']->GetAssessmentDueDate($row[$i]);
						if($a_dateto != "") {
							$a_dateto = date('M. j', strtotime($a_dateto));
						} else {
							$a_dateto = "-";
						}
						$a_timefrom = $library['assessment']->GetAssessmentStartTime($row[$i]);
						if($a_timefrom != "" && $a_datefrom != "-") {
							$a_timefrom = date('h:i a', strtotime($a_timefrom));
						} else {
							$a_timefrom = "";
						}
						$a_timeto = $library['assessment']->GetAssessmentDueTime($row[$i]);
						if($a_timeto != "" && $a_dateto != "-") {
							$a_timeto = date('h:i a', strtotime($a_timeto));
						} else {
							$a_timeto = "";
						}
						$a_overallpoints = $library['assessment']->GetOverallPoints($row[$i]);
						if($a_overallpoints == "" || $a_overallpoints == "0") 
							$a_overallpoints = "No points";
						elseif($a_overallpoints == 1)
							$a_overallpoints = "1 point";
						else 
							$a_overallpoints = "$a_overallpoints points";


						$a_numQuestions = $library['assessment']->GetNumberOfQuestions($row[$i]);

					?>
					<script>
					$(document).ready(function() {
						$('#btnDeleteAssessment_<?php echo $row[$i]; ?>').click(function() {
							var e = confirm("Are you sure you want to delete this assessment?\n\nNote: You cannot undo this action.");
							if(e) {
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=deleteassessment",
									data: {courseID: <?php echo $id; ?>, aid: <?php echo $row[$i]; ?>},
									success: function() {
										location.reload();
									}
								});
							}
						});
					});
					</script>
					<tr>
						<td><?php echo $a_name; ?></td>
						<td><?php echo $a_status; ?></td>
						<td><b><?php echo $a_datefrom; ?></b><br><?php echo $a_timefrom; ?></td>
						<td><b><?php echo $a_dateto; ?></b><br><?php echo $a_timeto; ?></td>
						<td><?php echo $a_numQuestions; ?></td>
						<td><?php echo $a_overallpoints; ?></td>
						<td><a href="courses.php?id=<?php echo $id; ?>&show=Assessment&aid=<?php echo $row[$i]; ?>" class="mini_icons view" title="View"></a>
							<a id="btnDeleteAssessment_<?php echo $row[$i]; ?>" class="mini_icons deletebtn" title="Delete"></a></td>
					</tr>
					<?php
					}
					if(sizeof($row) == 0) {
						echo '<tr><td colspan="7"><center>There are no assessments in this course.</center></td></tr>';
					}
					?>
				</table>
			</div>
		</div>
<?php
}
?>