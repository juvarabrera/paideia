<div id="sb1left">
		<?php require_once($toRoot.'template/sidebarMainMenu.php'); ?>
		</div>
		<div id="bigbody">
			<div class="content">
				<table class="title">
					<tr>
						<td>Courses</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<p class="text">
					<?php
					$row = $library['course']->GetAllCoursesHandledBy($loggedUser);
					for($i = 0; $i < sizeof($row); $i++) {
						$numStudents = $library['course']->GetNumberOfStudentsIn($row[$i]);
						$numProfessors = $library['course']->GetNumberOfProfessorsIn($row[$i]);
						if($numStudents == 0 || $numStudents == 1)
							$numStudents .= " student";
						else
							$numStudents .= " students";
						if($numProfessors == 0 || $numProfessors == 1)
							$numProfessors .= " professor";
						else
							$numProfessors .= " professors";
						$dp = $library['course']->GetProfilePicture($row[$i], 200);
						echo '
					<div class="box-list">
						<a href="courses.php?id='.$row[$i].'" title="'.$library['course']->GetCourseName($row[$i]).'"><div style="background-image:url('.$dp.');"></div>
						<div>
							<span>'.$library['course']->GetCourseName($row[$i]).'</span>
							<span>'.$numProfessors.', '.$numStudents.'</span>
						</div>
						<div class="shadow"></div></a>
					</div>';
					}
					?>
					<script>
					$(document).ready(function() {
						$('#btnPopup_EnrollCourse').click(function() {
							showPopup();
							$showPopup = "EnrollCourse";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'course_'+$showPopup},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<div class="box-list">
						<a id="btnPopup_EnrollCourse" title="Enroll A Course"><div class="createnew"></div>
						<div>
							<span>Enroll A Course</span>
							<span></span>
						</div>
						<div class="shadow"></div></a>
					</div>
				</p>
			</div>
		</div>
	</div>