		<div id="superbigbody">
			<div class="content" style="padding-bottom: 0px; overflow: hidden">
				<table class="title" style="margin-bottom: 0px">
					<tr>
						<td><a style="color: white;" href="courses.php?id=<?php echo $id; ?>"><?php echo $library['course']->GetCourseName($id); ?></a> | Grade Sheet</td>
						<td><div id="appGradeSheet_Status">Load Complete</div></td>
					</tr>
				</table>
<style>
.content:hover {
	background: rgba(255,255,255,.75) !important;
}
#appGradeSheet {
	width: 100%;
	position: relative;
	overflow: hidden;
	cursor: pointer;
}
#appGradeSheet #studentList {
	background: #222;
	width: 300px;
	height: 100%;
	position: absolute;
	top: 0px;
	left: 0px;
	color: white;
	font-size: 13px;
	overflow: hidden;
}
#appGradeSheet #studentList .title {
	height: 100px;
	float: left;
	text-align: center;
	width: 100%;
	font-weight: bold;
	text-transform: uppercase;
	font-size: 15px;
	text-shadow: 0px 2px 1px black;
	border-bottom: 1px solid rgba(255,255,255,.1);
	display: table;
}
#appGradeSheet #studentList .title span {
	vertical-align: middle;
	display: table-cell;
}
#appGradeSheet #studentList .stud {
	width: 100%;
	float: left;
	color: white;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 15px;
	text-shadow: 0px 2px 1px black;
	border-bottom: 1px solid transparent;
	background: #222222;
	background: -moz-linear-gradient(top,  #222222 0%, #1c1c1c 25%, #191919 50%, #1c1c1c 75%, #222222 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#222222), color-stop(25%,#1c1c1c), color-stop(50%,#191919), color-stop(75%,#1c1c1c), color-stop(100%,#222222));
	background: -webkit-linear-gradient(top,  #222222 0%,#1c1c1c 25%,#191919 50%,#1c1c1c 75%,#222222 100%);
	background: -o-linear-gradient(top,  #222222 0%,#1c1c1c 25%,#191919 50%,#1c1c1c 75%,#222222 100%);
	background: -ms-linear-gradient(top,  #222222 0%,#1c1c1c 25%,#191919 50%,#1c1c1c 75%,#222222 100%);
	background: linear-gradient(to bottom,  #222222 0%,#1c1c1c 25%,#191919 50%,#1c1c1c 75%,#222222 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#222222', endColorstr='#222222',GradientType=0 );
}
#appGradeSheet #studentList .stud:hover {
	background: #007c5a;
	background: -moz-linear-gradient(top,  #007c5a 0%, #007056 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#007c5a), color-stop(100%,#007056));
	background: -webkit-linear-gradient(top,  #007c5a 0%,#007056 100%);
	background: -o-linear-gradient(top,  #007c5a 0%,#007056 100%);
	background: -ms-linear-gradient(top,  #007c5a 0%,#007056 100%);
	background: linear-gradient(to bottom,  #007c5a 0%,#007056 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#007c5a', endColorstr='#007056',GradientType=0 );
}
#appGradeSheet #studentList .stud div.profpic {
	width: 20px;
	height: 20px;
	margin-right: 8px;
	float: left;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	border-radius: 18px;
}
#appGradeSheet #assessmentList {
	width: 530px;
	height: 100%;
	margin-left: 300px;
	float: left;
	color: #121212;
	font-size: 13px;
	overflow: hidden;
}
#appGradeSheet #assessmentList .title {
	padding: 15px 0px;
	float: left;
	text-align: center;
	width: 100%;
	font-weight: bold;
	text-transform: uppercase;
	font-size: 15px;
	text-shadow: 0px 2px 1px rgba(0,0,0,.3);
}
#appGradeSheet #assessmentList #assessment_Container {
	float: left;
	width: 100%;
	height: 100%;
	overflow-x: scroll;
	display: inline-block;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding-bottom: 50px;
}
#appGradeSheet #totalScoreList {
	width: 150px;
	height: 100%;
	position: absolute;
	right: 0px;
	top: 0px;
	color: #121212;
	font-size: 13px;
	overflow: hidden;
	background: rgba(0,0,0,.2);
}
#appGradeSheet #totalScoreList .title {
	height: 100px;
	float: left;
	text-align: center;
	width: 100%;
	font-weight: bold;
	display: table;
	vertical-align: middle;
	text-transform: uppercase;
	font-size: 15px;
	text-shadow: 0px 2px 1px rgba(0,0,0,.3);
	border-bottom: 1px solid rgba(0,0,0,.1);
}
#appGradeSheet #totalScoreList .title span {
	vertical-align: middle;
	display: table-cell;
}

.mini_content {
	width: 980px;
	margin: 0px auto;
	font-size: 11px;
	text-align: center;
	overflow: hidden;
}
.bottomspace {
	margin-bottom: 300px !important;
}
table.gridView {
	font-size: 13px;
	table-layout: fixed;
	border-collapse: collapse;
}
table.gridView td {
	border: 1px solid rgba(0,0,0,.1);
	padding: 0px 10px;
	position: relative;
}
table.gridView tr:first-child td {
	border-top: 0px;
	font-size: 15px;
}
table.gridView tr:not(:nth-child(2)) td {
	border-top: 0px;
}
table.gridView tr td:first-child {
	border-left: 0px;
}
table.gridView tr td:last-child {
	border-right: 0px;
}
table.gridView td, table.gridView th {
	min-width: 100px;
	height: 50px;
}
table.gridView tr:first-child td {
	font-weight: bold;
	text-align: center;
}
table.gridView tr:not(:first-child) td {
	text-align: right;
}
table.gridView input.editGrade {
	background: none !important;
}
table.gridView input.editGrade {
	text-align: right;
}
table.gridView input.editGrade:hover, table.gridView input.editGrade:focus {
	box-shadow: 0px 0px 0px black !important;
}
table.totalTable {
	min-width: 100%;
	table-layout: fixed;
	border-collapse: collapse;
}
table.totalTable td {
	text-align: right;
	height: 50px;
	padding: 0px 20px;	
	border-bottom: 1px solid rgba(0,0,0,.1);
}

</style>
<script>
$(document).ready(function() {
	$('#appGradeSheet').click(function() {
		$('input.editGrade').prop('disabled', true);
		$('input.editGrade').attr('isEditable', '0');
		$('td.editGrade').css({
			"background": "none"
		});
		$('input.editGrade').css({
			"color": "#121212",
			"text-align": "right"
		});
	});
	$('input.editGrade').each(function(i, obj) {
		$gradeVal = $(obj).val();
		if($gradeVal == "")
			$(obj).val('0');
	});
	$newHeight = $('table.gridView tr#assessmentRow').outerHeight() + $('#assessmentList .title').outerHeight();
	$('#studentList .title').height($newHeight);
	$('#totalScoreList .title').height($newHeight);

	$("input.editGrade").keydown(function (e) {
		if ($.inArray(e.keyCode, [46, 8, 27, 13]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
});
</script>
				<div id="appGradeSheet">
					<div id="studentList">
						<div class="title" id="btnHideSidebar"><span>Students</span></div>
						<?php
						$query = mysql_query("SELECT * FROM CourseUser INNER JOIN Account ON UserID = ID WHERE CourseID = $id AND Type = 'Student' ORDER BY LastName ASC");

						$n = 0;
						while($row = mysql_fetch_array($query)) {
							$studID = $row['UserID'];
							$studFName = $row['FirstName'];
							$studLName = $row['LastName'];
							$studName = $studLName.', '.$studFName;
							$dp = $library['user']->GetProfilePicture($studID, 50);
							$n++;
?>
						<script>
						$(document).ready(function() {
							$('a#rowStudent_<?php echo $studID; ?>').hover(function() {
								$('table.gridView tr#rowStudent_<?php echo $studID; ?> td').css({
									"background": "#222",
								});
								$('table.gridView tr#rowStudent_<?php echo $studID; ?> input.editGrade[isEditable="0"]').css({
									"color": "white",
								});
								$('table.totalTable tr#rowStudent_<?php echo $studID; ?> td').css({
									"background": "#222",
									"color": "white"
								});
							}, function() {
								$('table.gridView tr#rowStudent_<?php echo $studID; ?> td').css({
									"background": "none",
								});
								$('table.gridView tr#rowStudent_<?php echo $studID; ?>  input.editGrade[isEditable="0"]').css({
									"color": "#121212",
								});
								$('table.totalTable tr#rowStudent_<?php echo $studID; ?> td').css({
									"background": "none",
									"color": "#121212"
								});
							});
						});
						</script>
						<a class="stud" id="rowStudent_<?php echo $studID; ?>"><div class="profpic" style="background-image: url(<?php echo $dp; ?>)"></div><?php echo $studName; ?></a>
<?php
						}
						?>
					</div>
					<div id="assessmentList">
						<div class="title"><span>Assessments</span></div>
						<div id="assessment_Container">
							<table class="gridView" style="min-width: 100%;">
								<tr id="assessmentRow">
								<?php
								$query = mysql_query("SELECT * FROM Assessment WHERE CourseID = $id ORDER BY DateFrom ASC");

								while($row = mysql_fetch_array($query)) {
									$aid = $row['AssessmentID'];
									$a_name = $row['Name'];
									$a_status = $row['Status'];
									echo '<td>'.$a_name.'</td>';
								}
								?>
								</tr>
								<?php
								$query = mysql_query("SELECT * FROM CourseUser INNER JOIN Account ON UserID = ID WHERE CourseID = $id AND Type = 'Student' ORDER BY LastName ASC");

								$trow = 1;
								while($row = mysql_fetch_array($query)) {
									$studID = $row['UserID'];
									$studFName = $row['FirstName'];
									$studLName = $row['LastName'];
									$studName = $studLName.', '.$studFName;
									$dp = $library['user']->GetProfilePicture($studID, 50);
									$trow++;
?>
								<script>
								</script>
								<tr id="rowStudent_<?php echo $studID; ?>">
									<?php
									$query1 = mysql_query("SELECT * FROM Assessment WHERE CourseID = $id ORDER BY DateFrom ASC");
									$tcol = 1;
									while($row1 = mysql_fetch_array($query1)) {
										$aid = $row1['AssessmentID'];
										$a_name = $row1['Name'];
										$a_status = $row1['Status'];
?>
									<script>
									$(document).ready(function() {
										$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?>').click(function(event) {
											event.stopPropagation();
											$('input.editGrade').prop('disabled', true);
											$('input.editGrade').attr('isEditable', '0');
											$('td.editGrade').css({
												"background": "none"
											});
											$('input.editGrade').css({
												"color": "#121212",
												"text-align": "right"
											});


											$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?> input.editGrade').prop('disabled', false);
											$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?> input.editGrade').attr('isEditable', '1');

											$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?>').css({
												"background": "rgba(0,124,90,1)"
											});
											$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?> input.editGrade').css({
												"color": "white",
												"text-align": "left"
											});
											$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?> input.editGrade').focus();
										});
										$('#editGrade_<?php echo $studID; ?>_<?php echo $aid; ?> input.editGrade').keydown(function(e) {
											if(e.keyCode == 9) {
												alert('You pressed Tab');
											}
										});
									});
									</script>
									<td id="editGrade_<?php echo $studID; ?>_<?php echo $aid; ?>" class="editGrade cell_<?php echo $trow; ?>_<?php echo $tcol; ?>"><input min="0" max="100" type="number" class="editGrade" isEditable="0" disabled></td>
<?php
										$tcol++;
									}
									?>
								</tr>
<?php
								}
								?>
							</table>
						</div>
					</div>
					<div id="totalScoreList">
						<div class="title"><span>Total<br>Score</span></div>
						<table class="totalTable">
							<?php
							$query = mysql_query("SELECT * FROM CourseUser INNER JOIN Account ON UserID = ID WHERE CourseID = $id AND Type = 'Student' ORDER BY LastName ASC");

							$n = 0;
							while($row = mysql_fetch_array($query)) {
								$studID = $row['UserID'];
								$studFName = $row['FirstName'];
								$studLName = $row['LastName'];
								$studName = $studLName.', '.$studFName;
								$dp = $library['user']->GetProfilePicture($studID, 50);
								$n++;
?>
							<tr id="rowStudent_<?php echo $studID; ?>">
								<td>0%</td>
							</tr>
<?php
							}
							?>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="mini_content bottomspace">
			Gradesheet v1.0
		</div>