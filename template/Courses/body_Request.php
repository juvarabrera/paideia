		<div id="body">
			<script>
			$(document).ready(function () {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=getrequests",
					data: {id: <?php echo $id; ?>, fromwhat: 'Course'},
					success: function(html) {
						$('#listRequestCourses').html(html);
					}
				});
			});
			$refreshlistRequestCourses = 0;
			$(document).everyTime(1000, function() {
				if($refreshlistRequestCourses == 1) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getrequests",
						data: {id: <?php echo $id; ?>, fromwhat: 'Course'},
						success: function(html) {
							$('#listRequestCourses').html(html);
						}
					});
					$refreshlistRequestCourses = 0;
				}
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>Requests</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<ul class="results" id="listRequestCourses">
					<br><br><center><small><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></small></center><br>
				</ul>
			</div>
		</div>
		<div id="sb2">
			<?php if($userType == "Faculty") require_once('widgets/course_AddPeople.php'); ?>
			<?php require_once('widgets/course_list_People.php'); ?>
		</div>