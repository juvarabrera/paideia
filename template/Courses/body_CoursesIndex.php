		<div id="sb1left">
			<div class="content">
				<table class="title"><tr>
					<td><a href="courses.php?id=<?php echo $id; ?>"><?php $coursename = $library['course']->GetCourseName($id); if(strlen($coursename) >= 15) $coursename = substr($coursename, 0, 15).'...'; echo $coursename; ?></a></td>

					<script>
					$(document).ready(function() {
						$('#btnPopup_EditCourse').click(function() {
							showPopup();
							$showPopup = "EditCourse";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'course_'+$showPopup, courseID: <?php echo $id; ?>},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<td><?php if(!$isGuest) if($userType == "Faculty") echo '<a id="btnPopup_EditCourse" title="Course Settings" class="settingsicon"></a>'; ?></td>
				</tr></table>
				<hr>
				<?php 
				$dp = $library['course']->GetProfilePicture($id, 200);
				$courseType = $library['course']->GetCourseType($id);
				?>
				<div class="groupprofilepicture" style="background-image: url(<?php echo $dp; ?>)">
				</div>
				<?php
				if(!$isGuest) {
					if($library['course']->GetCourseKey($id) != "") {
						echo '
					<p class="text"><b>Course Key:</b> '.$library['course']->GetCourseKey($id).'</p><hr>';
					}
					if ($library['course']->GetCourseDescription($id) != "")
						echo '
					<p class="text wrap">'.nl2br(htmlentities($library['course']->GetCourseDescription($id))).'</p><hr>';
				} elseif($isGuest) {
					if($courseType == "Closed") {
						$msg = "Only faculty and students with the Course Key can enroll to this course.";
						$fixedCourseKey = $id;
						$callEnrollCourse = 'id="btnPopup_EnrollCourse" ';
						$btnEnrollCourse_Text = "Enroll Course";
				?>
				<script>
				$(document).ready(function() {
					$('#btnPopup_EnrollCourse').click(function() {
						showPopup();
						$showPopup = "EnrollCourse";
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=showpopup",
							data: {popup: 'course_'+$showPopup, fixedCourseKey: <?php echo $id; ?>,},
							success: function(html) {
								$('#Popup').html(html);
								$heightPopup = $('div#popup_'+$showPopup).height()+5;
								$('#Popup').css({
									"width": "450px",
									"height": ($heightPopup)+"px",
									"margin-left": "-225px",
									"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
								});
								popup = 1;
							}
						});
					});
				});
				</script>
				<?php
					} else {
						$msg = "Anyone can enroll to this course with the permission of the faculty of the course.";
						$callEnrollCourse = 'id="btnEnrollCourse" ';
						$isRequested = $library['course']->IsRequested($id, $loggedUser);
						$btnEnrollCourse_Text = "Enroll Course";
						if($isRequested) 
							$btnEnrollCourse_Text = "Request Sent";
				?>
				<script>
				$(document).ready(function() {
					$('#btnEnrollCourse').click(function() {
						$text = $('#btnEnrollCourse').text();
						if($text == "Enroll Course") {
							$('#btnEnrollCourse').text('Sending Request...');
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=sendjoinrequest",
								data: {id: <?php echo $id; ?>, fromwhat: 'Course'},
								success: function() {
									$('#btnEnrollCourse').text('Request Sent');
								}
							});
						} else if ($text == "Cancel Request") {
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=deletejoinrequest",
								data: {id: <?php echo $id; ?>, fromwhat: 'Course', userid: <?php echo $loggedUser; ?>},
								success: function() {
									$('#btnEnrollCourse').text('Enroll Course');
								}
							});
						}
					})
					.mouseover(function() {
						$text = $('#btnEnrollCourse').text();
						if($text == "Request Sent")
							$('#btnEnrollCourse').text('Cancel Request');
					})
					.mouseleave(function() {
						$text = $('#btnEnrollCourse').text();
						if($text == "Cancel Request")
							$('#btnEnrollCourse').text('Request Sent');
					});
				});
				</script>
				<?php
					}
					echo '<p class="text"><b>Course Type:</b> '.$library['course']->GetCourseType($id).' Course</p><hr><p class="text">'.$msg.'
					<a '.$callEnrollCourse.'style="margin-bottom: 0px; margin-top: 10px;" class="inputbutton">'.$btnEnrollCourse_Text.'</a></p>';
				}
				?>
			</div>
			<?php if(!$isGuest) require_once($toRoot.'template/Courses/sidebarCourseMenu.php'); ?>
		</div>
		<div id="body" class="center">
			<?php


			// if user is a member

			if(!$isGuest) {
				if(isset($postID)) {
					$library['newsfeed']->ViewCoursePost($postID, $userType, $loggedUser, $skin, $library);
				} else {
					require_once('widgets/newsfeed_Course.php');
				}

			// If user is guest

			} else {
			?>
			<div class="content">
				<table class="title"><tr>
					<td>Description</td>
					<td></td>
				</tr></table>
				<hr>
				<?php
				if ($library['course']->GetCourseDescription($id) != "")
					echo '<p class="text wrap">'.nl2br(htmlentities($library['course']->GetCourseDescription($id))).'</p>';
				else
					echo '<p class="text wrap"><i>No description available.</i></p>';
				?>
			</div>
			<?php
			}
			?>
		</div>
		<div id="sb2">
			<?php
			require_once('widgets/course_list_People.php');
			if(!$isGuest && $userType == "Faculty" && $courseType == "Open") {
				require_once('widgets/course_list_Request.php');
			}
			?>
		</div>