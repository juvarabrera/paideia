
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $bTitle; ?></title>
	<link rel="stylesheet" href="styles/skin/<?php echo $skin; ?>/style.php">
	<script src="scripts/skin/<?php echo $skin; ?>/jquery.min-1.8.0.js"></script>
	<script src="scripts/skin/<?php echo $skin; ?>/jquery.timers-1.0.0.js"></script>
	<script>
	$(function() {
		$('a[href*=#]').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});
	</script>
</head>
<body onload="init();" style="background: #f8f8f8;">
<div id="container" style="background-image: url(images/skin/<?php echo $skin; ?>/bg/body1.jpg); background-repeat: no-repeat; background-position: center center; background-size: cover; background-attachment: fixed; height: 500px;">
<script>
$(document).ready(function() {
	function Login() {
		$u = $('.txt-dlsudsb-username').val();
		$p = $('.txt-dlsudsb-password').val();
		if($u != "" && $p != "") {
			$('#btnLogin').css({
				"display": "none"
			});
			$('.loadingGif').css({
				"display": "block"
			});
			$.ajax({
				type: "POST",
				cache: false,
				url: "process.php?action=login",
				data: {username: $u, password: $p},
				success: function() {
					location.reload();
				}
			});
			$('#btnLogin').prop('disabled', true);
			$('.txt-dlsudsb-username').prop('disabled', true);
			$('.txt-dlsudsb-password').prop('disabled', true);
		} else {
			$('#loginError').css({
				"padding": "10px 10px",
				"margin": "10px 0px",
				"height": "auto"
			});
			setTimeout(function() {
			$('#loginError').css({
				"padding": "0px 10px",
				"margin": "0px 0px",
				"height": "0px"
			});
			}, 3000);
		}
	}
	$('.txt-dlsudsb-username').keypress(function(e) {
		if(e.keyCode == 13 || e.which == 13) {
			Login();
		}
	});
	$('.txt-dlsudsb-password').keypress(function(e) {
		if(e.keyCode == 13 || e.which == 13) {
			Login();
		}
	});
	$('#btnLogin').click(function() {
		Login();
	})
	.keypress(function(e) {
		if(e.keyCode == 13 || e.which == 13) {
			Login();
		}
	});
	$('.txt-dlsudsb-username').focus();
})
</script>
	<div id="main">
		<div id="body">
			<div class="content" style="margin-top: 15px">
				<table class="title">
					<tr>
						<td>Register (Beta)</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<p>
<script>
$(document).ready(function() {
	$('#alphanum_regex').keypress(function (e) {
		var allowedChars = new RegExp("^[a-zA-Z0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (allowedChars.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	}).keyup(function() {
		var forbiddenChars = new RegExp("[^a-zA-Z0-9]", 'g');
		if (forbiddenChars.test($(this).val())) {
			$(this).val($(this).val().replace(forbiddenChars, ''));
		}
	});
	$('#alpha_regex1').keypress(function (e) {
		var allowedChars = new RegExp("^[a-zA-Z]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (allowedChars.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	}).keyup(function() {
		var forbiddenChars = new RegExp("[^a-zA-Z]", 'g');
		if (forbiddenChars.test($(this).val())) {
			$(this).val($(this).val().replace(forbiddenChars, ''));
		}
	});
	$('#alpha_regex2').keypress(function (e) {
		var allowedChars = new RegExp("^[a-zA-Z]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (allowedChars.test(str)) {
			return true;
		}
		e.preventDefault();
		return false;
	}).keyup(function() {
		var forbiddenChars = new RegExp("[^a-zA-Z]", 'g');
		if (forbiddenChars.test($(this).val())) {
			$(this).val($(this).val().replace(forbiddenChars, ''));
		}
	});
});
</script>
				<form action="process.php?action=register" method="post">
				<table class="margin-topbottom" cellspacing="0px">
					<tr>
						<td><input type="text" class="username" id="alphanum_regex" placeholder="Username" name="username" required></td>
					</tr>
					<tr>
						<td><input type="text" class="firstname" id="alpha_regex1" placeholder="First Name" name="firstname" required></td>
					</tr>
					<tr>
						<td><input type="text" class="lastname" id="alpha_regex2" placeholder="Last Name" name="lastname" required></td>
					</tr>
					<tr>
						<td>
						<select name="gender">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
						</td>
					</tr>
					<tr>
						<td>
						<select name="type">
							<option value="Student">Student</option>
							<option value="Faculty">Faculty</option>
						</select>
						</td>
					</tr>
					<tr>
						<td><input type="password" class="password" placeholder="Password" name="password" required></td>
					</tr>
					<tr>
						<td><input type="submit" name="submit" id="btnRegister" value="Register"></td>
					</tr>
				</table>
				</form>
				</p>
			</div>
		</div>
		<div id="sb1">
			&nbsp;
		</div>
		<div id="sb2" style="padding-top: 50px;">
			<div class="content login">
				<table class="title"><tr><td>Login</td><td></td></tr></table>
				<hr>
				<span class="error" id="loginError">Input username and password.</span>
				<p>
					<table class="margin-topbottom" cellspacing="0px">
						<tr>
							<td><input type="text" class="txt-dlsudsb-username" placeholder="Username" name="username"></td>
						</tr>
						<tr>
							<td><input type="password" class="txt-dlsudsb-password" placeholder="Password" name="password"></td>
						</tr>
					</table>
				</p>
			</div>
			<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif" style="display: none"></center>
			<input type="submit" id="btnLogin" value="Login">
		</div>
	</div>
</div>
<div id="top">
	<div class="base">
		<a href="index.php" class="logo"></a>
		<?php
		require_once('template/top_Login.php');
		?>
	</div>
</div>
<style>
#footer {
	width: 980px;
	margin: 0px auto;
	overflow: hidden;
	padding-top: 40px;
	margin-bottom: 200px;
}
#footer .col {
	float: left;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 5px 15px;
	width: 33%;
	line-height: 150%;
	font-size: 14px;
	text-align: justify;
}
#footer .col h2 {
	margin-bottom: 10px;
}
#footer .col .samplepic {
	width: 100%;
	height: 120px;
	background: rgba(0,0,0,.1);
	margin-bottom: 10px;
}
</style>
<div id="footer">
	<div class="col">
		<h2>Interact</h2>
		<div class="samplepic"></div>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero doloremque ea quisquam illo molestiae fuga.</div>
	<div class="col">
		<h2>Whenever, wherever</h2>
		<div class="samplepic"></div>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, earum illum veritatis, tempora vero ea!</div>
	<div class="col">
		<h2>World-class Learning</h2>
		<div class="samplepic"></div>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda ab delectus, cum odio velit harum!</div>
</div>
</body>
</html>