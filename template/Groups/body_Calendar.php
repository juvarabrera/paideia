<?php 
$minyear = 2014;
$maxyear = $minyear + 1;
if(isset($_GET['my'])) {
	$my = $_GET['my'];
	$my = explode("_", $my);
	if(sizeof($my) == 2 && strlen($my[1]) == 4 && $my[0] >= 1 && $my[0] <= 12 && is_numeric($my[0]) && is_numeric($my[1]) && $my[1] >= $minyear && $my[1] <= $maxyear) {
		$month = $my[0];
		$year = $my[1];
	} else {
		$month = date('m');
		$year = date('Y');
	}
} else {
	$month = date('m');
	$year = date('Y');
}
$nmonth = $month + 1;
$nyear = $year;
if($nmonth == 13) {
	$nmonth = 1;
	$nyear += 1;
	if($nyear == $maxyear+1) {
		$nyear = $minyear;
	}
}

$nextmonth = str_pad($nmonth, 2, '0', STR_PAD_LEFT).'_'.$nyear;
$nextmonth_value = date('F', strtotime("$nyear-$nmonth-1")).' '. $nyear;

$nmonth = $month - 1;
$nyear = $year;
if($nmonth == 0) {
	$nmonth = 12;
	$nyear -= 1;
	if($nyear == $minyear-1) {
		$nyear = $maxyear;
	}
}

$prevmonth = str_pad($nmonth, 2, '0', STR_PAD_LEFT).'_'.$nyear;
$prevmonth_value = date('F', strtotime("$nyear-$nmonth-1")).' '. $nyear;

$month = str_pad($month, 2, '0', STR_PAD_LEFT);
?>
		<div id="bigbody">
			<div class="content">
				<table class="titlecalendar">
					<tr>
						<td><a href="groups.php?id=<?php echo $id; ?>&show=Calendar&my=<?php echo $prevmonth; ?>" class="mini_icons prev" title="<?php echo $prevmonth_value; ?>"></a></td>
						<td><h5><?php echo date('F',strtotime("$year-$month-1")).' '.$year; ?></h5></td>
						<td><a href="groups.php?id=<?php echo $id; ?>&show=Calendar&my=<?php echo $nextmonth; ?>" class="mini_icons next" title="<?php echo $nextmonth_value; ?>"></a></td>
					</tr>
				</table>
				<hr>
				<table class="calendar">
					<?php
					$days = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
					echo '
					<tr>';
					foreach($days as $day) {
						echo "<td>$day</td>";
					}
					echo '
					</tr>';
					$numberofdaysinamonth = date('t',strtotime("$year-$month-1"));
					$dayToDisplay = 1;
					$startcounting = false;
					while($dayToDisplay <= $numberofdaysinamonth) {
						echo '<tr valign="top">';
						foreach($days as $day) {
							$today = "";
							if("$year-$month-$dayToDisplay" == date('Y-m-d'))
								$today = ' class="today"';
							if($dayToDisplay > $numberofdaysinamonth) {
								echo "<td$today></td>";
							} elseif($day == date('l',strtotime("$year-$month-$dayToDisplay"))) {
								echo "<td$today>$dayToDisplay";
								$row = $library['calendar']->GetEventIn($id, 'Group', date("Y-m-d", strtotime("$year-$month-$dayToDisplay")));
								for($i = 0; $i < sizeof($row); $i++) {
									$explode = explode("%$%", $row[$i]);
									$idx = $explode[0];
									$from = $explode[1];
									if($from == "Event") {
										echo '
										<form action="process.php?action=deleteevent&id='.$_GET['id'].'&fromwhat=Group" method="post">
											<span class="event">'.$library['calendar']->GetEventName($idx).'
											<input type="submit" value="" name="deleteevent" class="deleteevent">
											<input type="hidden" name="eventid" value="'.$idx.'">
											</span>
										</form>';
									} elseif ($from == "Assessment") {
										echo '<span class="assessment">'.$library['assessment']->GetAssessmentName($idx).'</span>';
									}
								}
								$startcounting = true;
								if($userType_Group == "Admin") {
								?>
									<script>
									<?php 
									$datetopass_x = $year.'-'.$month.'-'.str_pad($dayToDisplay, 2, '0', STR_PAD_LEFT);
									?>
									$(document).ready(function() {
										$('#btnPopup_AddEvent_<?php echo $dayToDisplay; ?>').click(function() {
											showPopup();
											$showPopup = "AddEvent";
											$.ajax({
												type: "POST",
												cache: false,
												url: "process.php?action=showpopup",
												data: {popup: 'group_'+$showPopup, date: "<?php echo $datetopass_x; ?>", courseID: <?php echo $id; ?>},
												success: function(html) {
													$('#Popup').html(html);
													$heightPopup = $('div#popup_'+$showPopup).height()+5;
													$('#Popup').css({
														"width": "450px",
														"height": ($heightPopup)+"px",
														"margin-left": "-225px",
														"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
													});
													popup = 1;
												}
											});
										});
									});
									</script>
									<a id="btnPopup_AddEvent_<?php echo $dayToDisplay; ?>" class="addeventicon" title="Add Event"></a></td>
								<?php
								}
							} else {
								echo "<td></td>";
							}
							if($startcounting)
								$dayToDisplay++;
						}
						echo '</tr>';
					}
					?>
				</table>
				<p class="text">
					<span class="event" style="float: left; width: auto; margin: 10px;">Event</span>
					<span class="assessment" style="float: left; width: auto; margin: 10px;">Assessment</span>
				</p>
			</div>
		</div>