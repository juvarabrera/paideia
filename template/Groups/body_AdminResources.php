		<div id="bigbody">
			<div class="content">
				<table class="title">
					<tr>
						<td>Resources</td>
						<td></td>
					</tr>
				</table>
				<table class="folder">
					<tr>
						<td><img src="images/skin/<?php echo $skin; ?>/bg/folder.png">/</td>
						<td></td>
						<td align="right">
							<script>
							$(document).ready(function() {
								$('#btnPopup_AddFolder').click(function() {
									showPopup();
									$showPopup = "AddFolder";
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=showpopup",
										data: {popup: 'group_'+$showPopup, parentfolder: "/", courseID: <?php echo $id; ?>},
										success: function(html) {
											$('#Popup').html(html);
											$heightPopup = $('div#popup_'+$showPopup).height()+5;
											$('#Popup').css({
												"width": "450px",
												"height": ($heightPopup)+"px",
												"margin-left": "-225px",
												"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
											});
											popup = 1;
										}
									});
								});
							});
							</script>
							<a id="btnPopup_AddFolder" class="addfile">Add folder</a>

							<script>
							$(document).ready(function() {
								$('#btnPopup_AddFile').click(function() {
									showPopup();
									$showPopup = "AddFile";
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=showpopup",
										data: {popup: 'group_'+$showPopup, parentfolder: "/", courseID: <?php echo $id; ?>},
										success: function(html) {
											$('#Popup').html(html);
											$heightPopup = $('div#popup_'+$showPopup).height()+5;
											$('#Popup').css({
												"width": "450px",
												"height": ($heightPopup)+"px",
												"margin-left": "-225px",
												"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
											});
											popup = 1;
										}
									});
								});
							});
							</script>
							<a id="btnPopup_AddFile" class="addfile">Add file</a>
						</td>
					</tr>
				</table>
				<ul class="filelist">
				<?php
				function GetFilesOnAFolder($folder, $numFolder, $skin) {
					try {
						$dir = new DirectoryIterator($folder);
						$files = 0;
						while($dir->valid()) {
							if ($dir->isFile()) {
								$extention = explode('.', $dir->getFileName());
								$extention = end($extention);
								$icon = "images/skin/$skin/file-icons/$extention.png";
								if(!file_exists($icon)) 
									$icon = "images/skin/$skin/file-icons/_blank.png";
								echo '
						<a class="list"><img src="'.$icon.'">'.$dir->getFileName().'
							<form action="process.php?action=deletefile" method="post">
								<input type="submit" onclick="if(!confirm(\'Are you sure you want to delete this file?\')) return false;" value="" class="deletefile button" title="Delete">
								<input type="hidden" name="filedir" value="'.$folder.'/'.$dir->getFileName().'">
								<input type="hidden" name="groupID" value="'.$_GET['id'].'">
							</form>
							<form action="process.php?action=downloadfile" target="_blank" method="post">
								<input type="submit" value="" class="downloadfile button" title="Download">
								<input type="hidden" name="filedir" value="'.$folder.'/'.$dir->getFileName().'">
							</form>
						</a>';
								$files++;
							}

							$dir->next();
						}
						if($files == 0) {
								echo '
						<a title="No files"><i>No files in this directory.</i></a>';
						}

						$dir->rewind();

						$maxFolderToOpen = 3;
						if($numFolder <= $maxFolderToOpen) {
							while($dir->valid()) {
								$parentfolder = str_replace("groups/".$_GET['id']."/resources", "", $folder).'/'.$dir->getFilename();
								$button = '
								<script>
								$(document).ready(function() {
									$(\'#btnPopup_AddFolder_'.$numFolder.'\').click(function() {
										showPopup();
										$showPopup = "AddFolder";
										$.ajax({
											type: "POST",
											cache: false,
											url: "process.php?action=showpopup",
											data: {popup: \'group_\'+$showPopup, parentfolder: "'.$parentfolder.'", courseID: '.$id.'},
											success: function(html) {
												$(\'#Popup\').html(html);
												$heightPopup = $(\'div#popup_\'+$showPopup).height()+5;
												$(\'#Popup\').css({
													"width": "450px",
													"height": ($heightPopup)+"px",
													"margin-left": "-225px",
													"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
												});
												popup = 1;
											}
										});
									});
								});
								</script>
								<a id="btnPopup_AddFolder_'.$numFolder.'" class="addfile">Add folder</a>

								<script>
								$(document).ready(function() {
									$(\'#btnPopup_AddFile_'.$numFolder.'\').click(function() {
										showPopup();
										$showPopup = "AddFile";
										$.ajax({
											type: "POST",
											cache: false,
											url: "process.php?action=showpopup",
											data: {popup: \'group_\'+$showPopup, parentfolder: "'.$parentfolder.'", courseID: '.$id.'},
											success: function(html) {
												$(\'#Popup\').html(html);
												$heightPopup = $(\'div#popup_\'+$showPopup).height()+5;
												$(\'#Popup\').css({
													"width": "450px",
													"height": ($heightPopup)+"px",
													"margin-left": "-225px",
													"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
												});
												popup = 1;
											}
										});
									});
								});
								</script>
								<a id="btnPopup_AddFile_'.$numFolder.'" class="addfile">Add file</a>';
								if($numFolder == $maxFolderToOpen) {
									$button = '
									<script>
									$(document).ready(function() {
										$(\'#btnPopup_AddFile_'.$numFolder.'\').click(function() {
											showPopup();
											$showPopup = "AddFile";
											$.ajax({
												type: "POST",
												cache: false,
												url: "process.php?action=showpopup",
												data: {popup: \'group_\'+$showPopup, parentfolder: "'.$parentfolder.'", courseID: '.$id.'},
												success: function(html) {
													$(\'#Popup\').html(html);
													$heightPopup = $(\'div#popup_\'+$showPopup).height()+5;
													$(\'#Popup\').css({
														"width": "450px",
														"height": ($heightPopup)+"px",
														"margin-left": "-225px",
														"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
													});
													popup = 1;
												}
											});
										});
									});
									</script>
									<a id="btnPopup_AddFile_'.$numFolder.'" class="addfile">Add file</a>';
								}
								if(!$dir->isDot() && $dir->isDir()) {
									echo '
						<table class="folder">
							<tr>
								<td>
									<img src="images/skin/'.$skin.'/bg/folder.png">/'.$dir->getFilename().'
								</td>
								<td>
									<form action="process.php?action=deletefolder" method="post">
										<input type="submit" onclick="if(!confirm(\'Are you sure you want to delete this folder?\n\nIt will also delete the files and folder under this folder.\')) return false;" value="">
										<input type="hidden" name="dir" value="'.$folder.'/'.$dir->getFileName().'">
										<input type="hidden" name="groupID" value="'.$_GET['id'].'">
									</form>
								</td>
								<td align="right">
									'.$button.'
								</td>
							</tr>
						</table>
						<ul class="filelist">';
									GetFilesOnAFolder($folder.'/'.$dir->getFilename(), $numFolder+1, $skin);
									echo '
						</ul>';
								}
								$dir->next();
							}
						}
					} catch(Exception $e) {
						echo $e->getMessage();
					}
				}
				GetFilesOnAFolder('groups/'.$id.'/resources', 0, $skin);
				?>
				</ul>
			</div>
		</div>