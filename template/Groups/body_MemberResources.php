		<div id="bigbody">
			<div class="content">
				<table class="title">
					<tr>
						<td>Resources</td>
						<td></td>
					</tr>
				</table>
				<table class="folder">
					<tr>
						<td><img src="images/skin/<?php echo $skin; ?>/bg/folder.png">/</td>
						<td></td>
						<td align="right">
						</td>
					</tr>
				</table>
				<ul class="filelist">
				<?php
				function GetFilesOnAFolder($folder, $numFolder, $skin) {
					try {
						$dir = new DirectoryIterator($folder);
						$files = 0;
						while($dir->valid()) {
							if ($dir->isFile()) {
								$extention = explode('.', $dir->getFileName());
								$extention = end($extention);
								$icon = "images/skin/$skin/file-icons/$extention.png";
								if(!file_exists($icon)) 
									$icon = "images/skin/$skin/file-icons/_blank.png";
								echo '
						<a class="list"><img src="'.$icon.'">'.$dir->getFileName().'
							<form action="process.php?action=downloadfile" target="_blank" method="post">
								<input type="submit" value="" class="downloadfile button" title="Download">
								<input type="hidden" name="filedir" value="'.$folder.'/'.$dir->getFileName().'">
							</form>
						</a>';
								$files++;
							}

							$dir->next();
						}
						if($files == 0) {
								echo '
						<a title="No files"><i>No files in this directory.</i></a>';
						}

						$dir->rewind();

						$maxFolderToOpen = 3;
						if($numFolder <= $maxFolderToOpen) {
							while($dir->valid()) {
								$parentfolder = str_replace("groups/".$_GET['id']."/resources", "", $folder).'/'.$dir->getFilename();
								$button = '<a onclick="showAddFolder();document.getElementById(\'txtParentFolder\').value=\''.$parentfolder.'\'" class="addfile">Add folder</a><a onclick="showAddFile();document.getElementById(\'txtFolderToUpload\').value = \''.$parentfolder.'\'" class="addfile">Add file</a>';
								if($numFolder == $maxFolderToOpen) {
									$button = '<a onclick="showAddFile();document.getElementById(\'txtFolderToUpload\').value = \''.$parentfolder.'\'" class="addfile one">Add file</a>';
								}
								if(!$dir->isDot() && $dir->isDir()) {
									echo '
						<table class="folder">
							<tr>
								<td>
									<img src="images/skin/'.$skin.'/bg/folder.png">/'.$dir->getFilename().'
								</td>
								<td>
								</td>
								<td align="right">
								</td>
							</tr>
						</table>
						<ul class="filelist">';
									GetFilesOnAFolder($folder.'/'.$dir->getFilename(), $numFolder+1, $skin);
									echo '
						</ul>';
								}
								$dir->next();
							}
						}
					} catch(Exception $e) {
						echo $e->getMessage();
					}
				}
				GetFilesOnAFolder('groups/'.$id.'/resources', 0, $skin);
				?>
				</ul>
			</div>
		</div>