<div id="sb1left">
		<?php require_once($toRoot.'template/sidebarMainMenu.php'); ?>
		</div>
		<div id="bigbody">
			<div class="content">
				<table class="title">
					<tr>
						<td>Groups</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<p class="text">
					<?php

					$row = $library['group']->GetAllGroupsOf($loggedUser);
					for($i = 0; $i < sizeof($row); $i++) {
						$numMembers = $library['group']->GetNumberOfMembersIn($row[$i]);
						if($numMembers == 0 || $numMembers == 1)
							$numMembers .= " member";
						else
							$numMembers .= " members";
						$dp = $library['group']->GetProfilePicture($row[$i], 200);
						echo '
					<div class="box-list">
						<a href="groups.php?id='.$row[$i].'" title="'.$library['group']->GetGroupName($row[$i]).'"><div style="background-image:url('.$dp.');"></div>
						<div>
							<span>'.$library['group']->GetGroupName($row[$i]).'</span>
							<span>'.$numMembers.'</span>
						</div>
						<div class="shadow"></div></a>
					</div>';
					}
					?>
					<script>
					$(document).ready(function() {
						$('#btnPopup_JoinGroup').click(function() {
							showPopup();
							$showPopup = "JoinGroup";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'group_'+$showPopup},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<div class="box-list">
						<a id="btnPopup_JoinGroup" title="Join a Group"><div class="createnew"></div>
						<div>
							<span>Join a Group</span>
							<span></span>
						</div>
						<div class="shadow"></div></a>
					</div>
					<script>
					$(document).ready(function() {
						$('#btnPopup_CreateGroup').click(function() {
							showPopup();
							$showPopup = "CreateGroup";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'group_'+$showPopup},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<div class="box-list">
						<a id="btnPopup_CreateGroup" title="Create New Group"><div class="createnew"></div>
						<div>
							<span>Create New Group</span>
							<span></span>
						</div>
						<div class="shadow"></div></a>
					</div>
				</p>
			</div>
		</div>