		<div id="sb1left">
			<div class="content">
				<table class="title"><tr>
					<td><a href="groups.php?id=<?php echo $id; ?>"><?php $groupname = $library['group']->GetGroupName($id); if(strlen($groupname) >= 15) $groupname = substr($groupname, 0, 15).'...'; echo $groupname; ?></a></td>
					
					<script>
					$(document).ready(function() {
						$('#btnPopup_EditGroup').click(function() {
							showPopup();
							$showPopup = "EditGroup";
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=showpopup",
								data: {popup: 'group_'+$showPopup, groupID: <?php echo $id; ?>},
								success: function(html) {
									$('#Popup').html(html);
									$heightPopup = $('div#popup_'+$showPopup).height()+5;
									$('#Popup').css({
										"width": "450px",
										"height": ($heightPopup)+"px",
										"margin-left": "-225px",
										"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
									});
									popup = 1;
								}
							});
						});
					});
					</script>
					<td><?php if(!$isGuest) if($userType_Group == "Admin") echo '<a id="btnPopup_EditGroup" title="Group Settings" class="settingsicon"></a>'; ?></td>
				</tr></table>
				<hr>
				<?php 
				$dp = $library['group']->GetProfilePicture($id, 200);
				$groupType = $library['group']->GetGroupType($id);
				?>
				<div class="groupprofilepicture" style="background-image: url(<?php echo $dp; ?>)">
				</div>
				<?php
				if(!$isGuest) {
					if($library['group']->GetGroupKey($id) != "") {
						echo '
					<p class="text"><b>Course Key:</b> '.$library['group']->GetGroupKey($id).'</p><hr>';
					}
					if ($library['group']->GetGroupDescription($id) != "")
						echo '
					<p class="text wrap">'.nl2br(htmlentities($library['group']->GetGroupDescription($id))).'</p><hr>';
				} elseif($isGuest) {
					if($groupType == "Closed") {
						$msg = "Only faculty and students with the Group Key can enroll to this group.";
						$fixedCourseKey = $id;
						$callJoinGroup = 'id="btnPopup_JoinGroup" ';
						$btnJoinGroup_Text = "Join Group";
				?>
				<script>
				$(document).ready(function() {
					$('#btnPopup_JoinGroup').click(function() {
						showPopup();
						$showPopup = "JoinGroup";
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=showpopup",
							data: {popup: 'group_'+$showPopup, fixedGroupKey: <?php echo $id; ?>,},
							success: function(html) {
								$('#Popup').html(html);
								$heightPopup = $('div#popup_'+$showPopup).height()+5;
								$('#Popup').css({
									"width": "450px",
									"height": ($heightPopup)+"px",
									"margin-left": "-225px",
									"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px - 30px)"
								});
								popup = 1;
								
							}
						});
					});
				});
				</script>
				<?php
					} else {
						$msg = "Anyone can join to this course with the permission of the administrator of the group.";
						$callJoinGroup = 'id="btnJoinGroup" ';
						$isRequested = $library['group']->IsRequested($id, $loggedUser);
						$btnJoinGroup_Text = "Join Group";
						if($isRequested) 
							$btnJoinGroup_Text = "Request Sent";
				?>
				<script>
				$(document).ready(function() {
					$('#btnJoinGroup').click(function() {
						$text = $('#btnJoinGroup').text();
						if($text == "Join Group") {
							$('#btnJoinGroup').text('Sending Request...');
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=sendjoinrequest",
								data: {id: <?php echo $id; ?>, fromwhat: 'Group'},
								success: function() {
									$('#btnJoinGroup').text('Request Sent');
								}
							});
						} else if ($text == "Cancel Request") {
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=deletejoinrequest",
								data: {id: <?php echo $id; ?>, fromwhat: 'Group', userid: <?php echo $loggedUser; ?>},
								success: function() {
									$('#btnJoinGroup').text('Join Group');
								}
							});
						}
					})
					.mouseover(function() {
						$text = $('#btnJoinGroup').text();
						if($text == "Request Sent")
							$('#btnJoinGroup').text('Cancel Request');
					})
					.mouseleave(function() {
						$text = $('#btnJoinGroup').text();
						if($text == "Cancel Request")
							$('#btnJoinGroup').text('Request Sent');
					});
				});
				</script>
				<?php
					}
					echo '<p class="text"><b>Group Type:</b> '.$library['course']->GetCourseType($id).' Group</p><hr><p class="text">'.$msg.'
					<a '.$callJoinGroup.'style="margin-bottom: 0px; margin-top: 10px;" class="inputbutton">'.$btnJoinGroup_Text.'</a></p>';
				}
				?>
			</div>
			<?php if(!$isGuest) require_once($toRoot.'template/Groups/sidebarGroupMenu.php'); ?>
		</div>
		<div id="body" class="center">
			<?php

			// if user is member

			if(!$isGuest) {
				if(isset($postID)) {
					$library['newsfeed']->ViewGroupPost($postID, $userType_Group, $loggedUser, $skin, $library);
				} else {
					require_once('widgets/newsfeed_Group.php');
				}

			// if user is guest
			} else {
			?>
			<div class="content">
				<table class="title"><tr>
					<td>Description</td>
					<td></td>
				</tr></table>
				<hr>
				<?php
				if ($library['group']->GetGroupDescription($id) != "")
					echo '<p class="text wrap">'.nl2br(htmlentities($library['group']->GetGroupDescription($id))).'</p>';
				else
					echo '<p class="text wrap"><i>No description available.</i></p>';
				?>
			</div>
			<?php
			}
			?>
		</div>
		<div id="sb2">
			<?php
			require_once('widgets/group_list_People.php');
			if(!$isGuest && $userType_Group == "Admin" && $groupType == "Open") {
				require_once('widgets/group_list_Request.php');
			}
			if(!$isGuest && $userType_Group == "Member") {
?>
			<form action="process.php?action=leavegroup" method="post">
				<input type="submit" name="leave" class="delete" value="Leave Group" onclick="if(!confirm('Are you sure you want to leave this group?')) { return false; }">
				<input type="hidden" name="groupID" value="<?php echo $id; ?>">
			</form>
<?php
			}
			?>
		</div>