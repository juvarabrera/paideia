		<div id="body">
			<script>
			$(document).ready(function () {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=getrequests",
					data: {id: <?php echo $id; ?>, fromwhat: 'Group'},
					success: function(html) {
						$('#listRequestGroups').html(html);
					}
				});
			});
			$refreshlistRequestGroups = 0;
			$(document).everyTime(1000, function() {
				if($refreshlistRequestGroups == 1) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getrequests",
						data: {id: <?php echo $id; ?>, fromwhat: 'Group'},
						success: function(html) {
							$('#listRequestGroups').html(html);
						}
					});
					$refreshlistRequestGroups = 0;
				}
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>Requests</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<ul class="results" id="listRequestGroups">
					<br><br><center><small><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></small></center><br>
				</ul>
			</div>
		</div>
		<div id="sb2">
			<?php if($userType_Group == "Admin") require_once('widgets/group_AddPeople.php'); ?>
			<?php require_once('widgets/group_list_People.php'); ?>
		</div>