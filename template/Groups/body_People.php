<?php
if($isGuest || $userType_Group == "Member") {
?>
		<div id="bigbody">
			<script>
			$(document).ready(function () {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=getpeopleongroup",
					data: {id: <?php echo $id; ?>},
					success: function(html) {
						$('#listPeopleGroups').html(html);
					}
				});
			});
			$refreshlistPeopleGroups = 0;
			$(document).everyTime(1000, function() {
				if($refreshlistPeopleGroups == 1) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getpeopleongroup",
						data: {id: <?php echo $id; ?>},
						success: function(html) {
							$('#listPeopleGroups').html(html);
						}
					});
					$refreshlistPeopleGroups = 0;
				}
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>People</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<div id="listPeopleGroups">
					<br><br><center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif" /></center><br>
				</div>
			</div>
		</div>
<?php
} else {
?>
		<div id="body">
			<script>
			$(document).ready(function () {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=getpeopleongroup",
					data: {id: <?php echo $id; ?>},
					success: function(html) {
						$('#listPeopleGroups').html(html);
					}
				});
			});
			$refreshlistPeopleGroups = 0;
			$(document).everyTime(1000, function() {
				if($refreshlistPeopleGroups == 1) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getpeopleongroup",
						data: {id: <?php echo $id; ?>},
						success: function(html) {
							$('#listPeopleGroups').html(html);
						}
					});
					$refreshlistPeopleGroups = 0;
				}
			});
			</script>
			<div class="content">
				<table class="title">
					<tr>
						<td>People</td>
						<td></td>
					</tr>
				</table>
				<hr>
				<div id="listPeopleGroups">
					<br><br><center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif" /></center><br>
				</div>
			</div>
		</div>
		<div id="sb2">
		<?php if($userType_Group == "Admin") require_once('widgets/group_AddPeople.php'); ?>
		<?php if($userType_Group == "Admin") require_once('widgets/group_list_Request.php'); ?>
		</div>
<?php
}
?>