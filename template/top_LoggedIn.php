<script>
$(document).ready(function() {
	$searchView = 0;
	$('#txtSearch').click(function(event) {
		hideDdls();
		event.stopPropagation();
		$text = $('#txtSearch').val();
		if($searchView == 0 && $text != "") {
			$('#ddlSearch').css({
				"display": "block"
			});
			$searchView = 1;
			$.ajax({
				type: "POST",
				cache: false,
				url: "process.php?action=topsearch",
				data: {searchQuery: $text},
				success: function(html) {
					$('#topSearchResults').html(html);
				}
			});
		}
	});
	$('#txtSearch').keyup(function(event) {
		$('#ddlMoreOptions').css({
			"display": "none"
		});
		$moreoptions_view = 0;
		$('#ddlNotification').css({
			"display": "none"
		});
		$notif_view = 0;
		$('#ddlMessage').css({
			"display": "none"
		});
		$message_view = 0;
		$('#ddlMessage').css({
			"display": "none"
		});
		$message_view = 0;
		$('#topSearchResults').css({
			"overflow-y": "hidden"
		});
		$text = $('#txtSearch').val();
		if($text != "") {
			$('#ddlSearch').css({
				"display": "block"
			});
			$('#topSearchResults').html('<h5>Searching...</h5><span style="display: block; width: 100%; text-align: center;color: white; font-size: 12px; font-style: italic; padding: 10px 0px; box-sizing: border-box;"><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></span>');
			$.ajax({
				type: "POST",
				cache: false,
				url: "process.php?action=topsearch",
				data: {searchQuery: $text},
				success: function(html) {
					$('#topSearchResults').html(html);
					if($('#topSearchResults').height() == $(window).height()-200) {
						$('#topSearchResults').css({
							"overflow-y": "scroll"
						});
					} else {
						$('#topSearchResults').css({
							"overflow-y": "hidden"
						});
					}
				}
			});
		} else {
			$('#ddlSearch').css({
				"display": "none"
			});
			$('#topSearchResults').html('<h5>Searching...</h5><span style="display: block; width: 100%; text-align: center;color: white; font-size: 12px; font-style: italic; padding: 10px 0px; box-sizing: border-box;"><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></span>');
		}
	});
	$('#topSearchResults').css({
		"max-height": $(window).height()-200+"px"
	});
	$('#notif_Container').css({
		"max-height": $(window).height()-200+"px"
	});
});
</script>

<li class="searchBox floatleft"><form action="search.php" method="get"><input autocomplete="off" type="text" name="q" id="txtSearch" placeholder="Search people, courses, groups." value=""></form><div class="searchicon"></div>

<div id="ddlSearch">
	<div class="menuup"></div>
	<div id="topSearchResults">
		<h5>Searching...</h5><span style="display: block; width: 100%; text-align: center;color: white; font-size: 12px; font-style: italic; padding: 10px 0px; box-sizing: border-box;"><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></span>
	</div>
</div>
</li>
<ul class="main_menu">
	
				<script>
				function hideDdls() {
					$('#ddlMoreOptions').css({
						"display": "none"
					});
					$moreoptions_view = 0;
					$('#ddlNotification').css({
						"display": "none"
					});
					$notif_view = 0;
					$('#ddlMessage').css({
						"display": "none"
					});
					$message_view = 0;
					$('#ddlMessage').css({
						"display": "none"
					});
					$message_view = 0;
					$('#ddlSearch').css({
						"display": "none"
					});
					$searchView = 0;
				}
				$(document).ready(function() {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=getnotification",
						data: {hash: '<?php echo substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999); ?>'},
						success: function(html) {
							$('#notif_Container').html(html);
							if($('#notif_Container').height() > $(window).height()-200) {
								$('#notif_Container').css({
									"overflow-y": "scroll"
								});
							} else {
								$('#notif_Container').css({
									"overflow-y": "hidden"
								});
							}
						}
					});
					setInterval(function() {
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=getnumnotification",
							data: {hash: '<?php echo substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999); ?>'},
							success: function(html) {
								$('#btnNotif').html(html);
								$text = $('#btnNotif').html();
								if($text != "") {
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=getnotification",
										data: {hash: '<?php echo substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999); ?>'},
										success: function(html) {
											$('#notif_Container').html(html);
											if($('#notif_Container').height() > $(window).height()-200) {
												$('#notif_Container').css({
													"overflow-y": "scroll"
												});
											} else {
												$('#notif_Container').css({
													"overflow-y": "hidden"
												});
											}
										}
									});
								}
							}
						});
					}, 1000);
					$('html').click(function() {
						hideDdls();
					});
					$moreoptions_view = 0;
					$('#btnMoreOptions').click(function(event) {
						event.stopPropagation();
						if($moreoptions_view == 0) {
							hideDdls();
							$('#ddlMoreOptions').css({
								"display": "block"
							});
							$moreoptions_view = 1;
						} else {
							$('#ddlMoreOptions').css({
								"display": "none"
							});
							$moreoptions_view = 0;
						}
					});
					$notif_view = 0;
					$('#btnNotif').click(function(event) {
						event.stopPropagation();
						if($notif_view == 0) {
							hideDdls();
							$('#ddlNotification').css({
								"display": "block"
							});
							$notif_view = 1;
						} else {
							$('#ddlNotification').css({
								"display": "none"
							});
							$notif_view = 0;
						}
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=readnotification",
							data: {hash: '<?php echo substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999); ?>'}
						});
					});
					$message_view = 0;
					$('#btnMessage').click(function(event) {
						event.stopPropagation();
						if($message_view == 0) {
							hideDdls();
							$('#ddlMessage').css({
								"display": "block"
							});
							$message_view = 1;
						} else {
							$('#ddlMessage').css({
								"display": "none"
							});
							$message_view = 0;
						}
					});
				});
				</script>
				<a href="index.php?-_-" class="name"><div style="background-image: url(<?php echo $toRoot.$library['user']->GetProfilePicture($loggedUser, 50); ?>);" class="profpicture"/></div><span><?php echo $library['user']->GetName($loggedUser); ?></span></a>
				<li class="ddlOption message floatleft">
				<a class="message mini-icons ddlOption" id="btnMessage"><span class="alert">2</span></a>
				<ul class="sub_menu message" id="ddlMessage">
					<div class="menuup"></div>
					<h6>Messages</h6>

				</ul></li>
				<li class="ddlOption notif floatleft">
				<a class="notif mini-icons ddlOption" id="btnNotif"></a>
				<ul class="sub_menu notif" id="ddlNotification">
					<div class="menuup"></div>
					<h6>Notifications</h6>
					<div id="notif_Container">
					<span style="display: block; width: 100%; text-align: center;color: white; font-size: 12px; font-style: italic; padding: 10px 0px; box-sizing: border-box;"><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif"></span>
					</div>
				</ul></li>
				<li class="ddlOption moreoptions"><a class="ddlOption moreoptions" id="btnMoreOptions"></a>
				<ul class="sub_menu" id="ddlMoreOptions">
					<div class="menuup"></div>
					<h6>Main Menu</h6>
					<li><a href="<?php echo $toRoot; ?>index.php">Home</a></li>
					<li><a href="<?php echo $toRoot; ?>courses.php">Courses</a>
					<ul class="right_sub">
						<div class="right_arrow"></div>
						<h6>Courses</h6>
					<?php
					$row = $library['course']->GetAllCoursesHandledBy($loggedUser);
					$max = 3;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$numStudents = $library['course']->GetNumberOfStudentsIn($row[$i]);
						$numProfessors = $library['course']->GetNumberOfProfessorsIn($row[$i]);
						$dp = $library['course']->GetProfilePicture($row[$i], 50);
						if($numStudents == 0 || $numStudents == 1)
							$numStudents .= " student";
						else
							$numStudents .= " students";
						if($numProfessors == 0 || $numProfessors == 1)
							$numProfessors .= " professor";
						else
							$numProfessors .= " professors";
						$coursename = $library['course']->GetCourseName($row[$i]);
						if(strlen($coursename) >= 15)
							$coursename = substr($coursename, 0, 15).'...';
						echo '<a href="courses.php?id='.$row[$i].'"><div class="profpic" style="background-image: url('.$dp.')"></div>'.$coursename.'<br><small><i>'.$numProfessors.', '.$numStudents.'</i></small></a>';
					}
					if(sizeof($row) > $max)
						echo '<a href="courses.php" align="right"><small>See all courses...</small></a>';
					if(sizeof($row) == 0)
						echo '<a>No courses to display.</a>';
					?>
					</ul></li>
					<li><a href="<?php echo $toRoot; ?>groups.php">Groups</a>
					<ul class="right_sub">
						<div class="right_arrow"></div>
						<h6>Groups</h6>
					<?php
					$row = $library['group']->GetAllGroupsOf($loggedUser);
					$max = 3;
					for($i = 0; $i < sizeof($row) && $i < $max; $i++) {
						$numMembers =$library['group']->GetNumberOfMembersIn($row[$i]);
						$dp = $library['group']->GetProfilePicture($row[$i], 50);
						if($numMembers == 0 || $numMembers == 1)
							$numMembers .= " member";
						else
							$numMembers .= " members";
						$groupname = $library['group']->GetGroupName($row[$i]);
						if(strlen($groupname) >= 15)
							$groupname = substr($groupname, 0, 15).'...';
						echo '<a href="groups.php?id='.$row[$i].'"><div class="profpic" style="background-image: url('.$dp.')"></div>'.$groupname.'<br><small><i>'.$numMembers.'</i></small></a>';
					}
					if(sizeof($row) > $max)
						echo '<a href="groups.php" align="right"><small>See all groups...</small></a>';
					if(sizeof($row) == 0)
						echo '<a>No groups to display.</a>';
					?>
					</ul></li>
					<li><a href="#">Cloud</a></li>
					<li><a href="#">Settings</a></li>
					<li><a href="<?php echo $toRoot; ?>logout.php">Logout</a></li>
				</ul></li>
			</ul>