<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $bTitle; ?></title>
	<link rel="stylesheet" href="styles/skin/<?php echo $skin; ?>/style.php">
	<?php require_once('scripts/skin/'.$skin.'/scripts.php'); ?>
	<?php
	if(file_exists("users/$loggedUser/cover.jpg"))
		echo '<style type="text/css">#bodyBg {background-image: url(users/'.$loggedUser.'/cover.jpg)}</style>';
	?>
</head>
<body onload="init();">
<script>
$(document).ready(function() {
	setInterval(function() {
		$.ajax({
			type: "POST",
			cache: false,
			url: "process.php?action=gettitlenotification",
			data: {hash: '<?php echo substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999); ?>', bTitle: '<?php echo $bTitle; ?>'},
			success: function(html) {
				document.title = html;
			}
		});
	}, 1000);
});
</script>
<div id="bodyBg"></div>
<div id="container">
<?php
$viewProfileOf = $library['user']->GetID($_GET['-_-']);
if($viewProfileOf == "")
	$viewProfileOf = $loggedUser;
$bg = "users/$viewProfileOf/cover.jpg";

if(!file_exists($bg))
	$bg = "images/skin/$skin/bg/body1.jpg";
?>
<style type="text/css">
#bodyBg {
	background-image: url(<?php echo $bg; ?>);
	background-color: #f8f8f8;
}
</style>
	<div id="main">
			<div id="profileHeader">
				<?php 
				$dp = $library['user']->GetProfilePicture($viewProfileOf, 200);
				?>
				<div class="userprofilepicture" style="background-image: url(<?php echo $dp; ?>)"></div>
			</div>
		<div id="sb1left">
			&nbsp;
		</div>
		<div id="body">
			<div class="content profileinfo">
				<table class="title profile">
					<tr>
						<td><h2><?php echo $library['user']->GetName($viewProfileOf); ?></h2></td>
					</tr>
				</table>
				<center>
				<ul class="profilemenu">
				<?php 
				$menus = array("Feed", "About", "Photos", "Courses", "Groups", "Portfolio");
				if(isset($_GET['-']))
					$show = $_GET['-']; 
				else
					$show = "Feed";
				$link = "?-_-";
				if($_GET['-_-'] != "") 
					$link = "?-_-=".$_GET['-_-'];
				for ($i = 0; $i < sizeof($menus); $i++) {
					$selected = "";
					if(!in_array($show, $menus) && $i == 0)
						$selected = "class=\"selected\"";
					if($show == $menus[$i])
						$selected = "class=\"selected\"";
					echo '<li><a href="'.$link.'&-='.$menus[$i].'"'.$selected.'>'.$menus[$i].'</a></li>';
				}
				?>
				</ul>
				</center>
				<hr>
			</div>
			<?php
			require_once("template/Profile/$show.php");
			?>
		</div>
		<div id="sb2">
		&nbsp;
		</div>
	</div>
</div>
<div id="top">
	<div class="base">
		<a href="index.php" class="logo"></a>
		<?php
		require_once('template/top_LoggedIn.php');
		?>
	</div>
</div>
</body>
<script>
init();
</script>
<?php
if(isset($viewProfileOf))
echo '<script>

$(function(){
	$("#txtSearch").val("'.$library['user']->GetName($viewProfileOf).'");
});

</script>';

require_once($toRoot.'popup.php'); 
?>
</html>