<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $bTitle; ?></title>
	<link rel="stylesheet" href="styles/skin/<?php echo $skin; ?>/style.php">
	<?php require_once('scripts/skin/'.$skin.'/scripts.php'); ?>
	<?php
	if(file_exists("users/$loggedUser/cover.jpg"))
		echo '<style type="text/css">#bodyBg {background-image: url(users/'.$loggedUser.'/cover.jpg)}</style>';
	?>
</head>
<body onload="init();">
<script>
$(document).ready(function() {
	setInterval(function() {
		$.ajax({
			type: "POST",
			cache: false,
			url: "process.php?action=gettitlenotification",
			data: {hash: '<?php echo substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999); ?>', bTitle: '<?php echo $bTitle; ?>'},
			success: function(html) {
				document.title = html;
			}
		});
	}, 1000);
});
</script>
<div id="bodyBg"></div>
<div id="container">
<div id="main">
		<div id="body">
			<?php require_once('widgets/post_Status.php'); ?>
			<?php require_once('widgets/newsfeed_Index.php'); ?>
		</div>
		<div id="sb1">
		<?php 
		require_once('widgets/list_Courses.php');
		?>
		</div>
		<div id="sb2">
		<?php
		require_once('widgets/list_Groups.php');
		?>
		</div>
	</div>
</div>
<div id="top">
	<div class="base">
		<a href="index.php" class="logo"></a>
		<?php
		require_once('template/top_LoggedIn.php');
		?>
	</div>
</div>
</body>
<script>
init();
</script>
<?php
if(isset($viewProfileOf))
echo '<script>

$(function(){
	$("#txtSearch").val("'.$library['user']->GetName($viewProfileOf).'");
});

</script>';

require_once($toRoot.'popup.php'); 
?>
</html>