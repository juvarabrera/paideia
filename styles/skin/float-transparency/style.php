<?php
header("Content-type: text/css");
$skin = "float-transparency";
$toRoot = "../../../";
$maincolor = "#06686c";
$maincolor_rgb = "6,104,108";
?>
@font-face {
    font-family: 'Open Sans';
    src: url('OpenSans-Regular-webfont.eot');
    src: url('OpenSans-Regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('OpenSans-Regular-webfont.woff') format('woff'),
         url('OpenSans-Regular-webfont.ttf') format('truetype'),
         url('OpenSans-Regular-webfont.svg#open_sansregular') format('svg');
    font-weight: normal;
    font-style: normal;

}
body {
	background-attachment: fixed;
	font-family: 'Open Sans', Trebuchet MS;
	margin: 0px;
}
b.font-weight_normal {
	font-weight: normal;
}
h1,h2,h3,h4,h5,h6 {
	margin: 0px;
	padding: 0px;
}
a {
	color: white;
	cursor: pointer;
	text-decoration: none;
}
ul,li {
	list-style-type: none;
	padding: 0px;
	margin: 0px;
}
hr {
	margin: 0px;
	padding: 0px;
	border: 0px;
}
:focus {
	outline: none;
}
table {
	border-spacing: 0px;
    border-collapse: separate;
}
input:not([type="radio"]),textarea, select {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	font-family: 'Open Sans', Trebuchet MS;
	font-size: 12px;
	border-radius: 4px;
	border: 0px;
	margin: 0px;
	width: 100%;
	padding: 8px;
	cursor: pointer;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
input:hover, input:focus, textarea:hover, textarea:focus {
	box-shadow: 0px 0px 5px rgba(0,0,0,.2);
}
input[type="file"]:hover{
	box-shadow: 0px 0px 0px transparent;
}
label {
	margin-right: 10px;
	cursor: pointer;
}
label input {
	box-shadow: 0px 0px 0px transparent;
}
label input:hover {
	box-shadow: 0px 0px 0px transparent;
}
input[type="submit"] {
	background: rgba(255,255,255,.5);
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	border: 2px solid <?php echo $maincolor; ?>;
	color: <?php echo $maincolor; ?>;
	font-weight: bold;
	cursor: pointer;	
}
#container {
	position: relative;
	overflow: hidden;
	margin-top: 55px;
	width: 100%;
}
#bodyBg {
	background-image: url(<?php echo $toRoot;?>images/skin/<?php echo $skin; ?>/bg/body1.jpg);
	background-color: #e8e8e8;
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	-webkit-filter: blur(10px);
	-moz-filter: blur(10px);
	-ms-filter: blur(10px);
	-o-filter: blur(10px);
	filter: blur(10px);
	position: fixed;
	width: 100%;
	height: 100%;
	top: 0px;
	left: 0px;
}
a.inputbutton {
	
	background: rgba(<?php echo $maincolor_rgb; ?>,.5);
	border: 2px solid white;
	color: white;

	background: rgba(255,255,255,.8);
	border: 2px solid <?php echo $maincolor; ?>;
	color: <?php echo $maincolor; ?>;
	
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	font-weight: bold;
	cursor: pointer;
	font-size: 12px;
	margin-bottom: 20px;
	border-radius: 4px;
	display: inline-block;
	text-align: center;
	width: 100%;
	padding: 8px;
	cursor: pointer;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
input[type="submit"]:hover, input[type="submit"]:focus, a.inputbutton:hover, a.inputbutton:focus {
	background-color: <?php echo $maincolor; ?>;
	color: white;
	border: 2px solid white;
}
input.delete, a.delete {
	background: rgba(211,41,41,.5);
	border: 2px solid rgba(255,255,255,.5);
	color: rgba(255,255,255,.5);
}
input.delete:hover {
	background: #d32929;
}
input.color, a.color {
	background: rgba(<?php echo $maincolor_rgb; ?>,.5);
	border: 2px solid rgba(255,255,255,.5);
	color: rgba(255,255,255,.5);
}


#top {
	width: 100%;
	height: 50px;
	background: rgba(255,255,255,.95);
	position: fixed;
	top: 0px;
	left: 0px;
	border-bottom: 3px solid rgba(<?php echo $maincolor_rgb; ?>,.9);
	-webkit-transition: all .3s ease-in-out;
	-moz-transition: all .3s ease-in-out;
	-ms-transition: all .3s ease-in-out;
	-o-transition: all .3s ease-in-out;
	transition: all .3s ease-in-out;
}
#top .base {
	width: 980px;
	height: 100%;
	position: relative;
	margin: 0px auto;
}
#top .base a.logo {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/logo/main.png) no-repeat left center;
	display: inline-block;
	width: 71px;
	height: 32px;
	height: 100%;
	float: left;
	color: #222;
	text-decoration: none;
	text-shadow: 0px 2px 2px rgba(0,0,0,.2);
	font-size: 18px;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	text-transform: uppercase;
	position: relative;
}
#top .base ul.main_menu {
	float: right;
	height: 100%;
	position: absolute;
	top: 0px;
	right: 0px;
}
#top .base ul.main_menu>a {
	color: #f8f8f8;
	text-decoration: none;	
	text-shadow: 0px 2px 1px rgba(0,0,0,.4);
	font-size: 14px;
	display: inline-block;
	float: left;
	margin: 0px 13px;
	position: relative;
	top: 8px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
#top .base>li.searchBox {
	top: 8px;
	margin-left: 20px;
	position: relative;
	float: left;
	overflow: visible;
}
#top .base>li.searchBox input {
	width: 300px;
	padding-left: 33px;
	background-color: rgba(255,255,255,.8);
	border: 1px solid <?php echo $maincolor; ?>
}
#top .base>li.searchBox input:hover, #top .base>li.searchBox input:focus {
	box-shadow: 0px 0px 10px rgba(255,255,255,1);
}
#top .base>li.searchBox div.searchicon {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/search.png);
	background-repeat: no-repeat;
	background-position: center center;
	background-size: cover;
	opacity: .5;
	width: 18px;
	height: 18px;
	position: absolute;
	left: 7px;
	top: 6px;
}
#top .searchBox #ddlSearch {
	background: rgba(0,0,0,.9);
	border-radius: 8px;
	width: 100%;
	float: left;
	position: absolute;
	left: 0px;
	top: calc(100% + 27px);
	padding: 10px 0px;
	display: none;
}
#top .searchBox #ddlSearch .menuup {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/menu_up.png);
	background-repeat: no-repeat;
	background-position: 90% bottom;
	position: absolute;
	top: -18px;
	right: 0px;
	width: 100%;
	height: 18px;
	opacity: .9;
}
#top .searchBox #ddlSearch #topSearchResults {
	font-size: 13px;
	color: white;
	width: 100%;
	display: block;
	overflow: hidden;
	height: auto;
	position: relative;
}
#top .searchBox #ddlSearch #topSearchResults::-webkit-scrollbar {
	width: 7px;
}
#top .searchBox #ddlSearch #topSearchResults::-webkit-scrollbar-track {
	background: transparent;
	border-radius: 5px 5px 5px 5px;
}
#top .searchBox #ddlSearch #topSearchResults::-webkit-scrollbar-thumb {
	border-radius: 5px 5px 5px 5px;
	background-color: rgba(255,255,255,.2);

}
#top .searchBox #ddlSearch #topSearchResults h5 {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 10px;
	text-transform: uppercase;
	border-bottom: 1px solid <?php echo $maincolor; ?>;
}
#top .searchBox #ddlSearch #topSearchResults a {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 10px;
	display: block;
	color: white;
	overflow: hidden;
	border-bottom: 1px solid rgba(255,255,255,.1);
}
#top .searchBox #ddlSearch #topSearchResults a div.profpic {
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	background-color: rgba(255,255,255,.1);
	width: 38px;
	height: 38px;
	float: left;
	display: block;
	margin-right: 10px;
	margin-right: 10px;
	border: 2px solid rgba(255,255,255,.6);
	border-radius: 3px;
}
#top .searchBox #ddlSearch #topSearchResults a span {
	font-weight: bold;
}
#top .searchBox #ddlSearch #topSearchResults a:hover {
	background: rgba(<?php echo $maincolor_rgb; ?>,.7);
}
#top .base ul.main_menu>li{
	display: block;
	float: left;
	height: 50px;
	position: relative;
	z-index: 100;
}
#top .base ul.main_menu a.ddlOption {
	display: block;
	width: 15px;
	height: 50px;
	top: 0px;
	padding: 0px 10px;
}
#top .base ul.main_menu a.ddlOption span.alert {
	background: #d21515;
	font-size: 10px;
	color: white;
	font-weight: bold;
	padding: 2px 2px 2px 3px;
	border-radius: 5px;
	position: absolute;
	top: 5px;
	right: 0px;
}
#top .base ul.main_menu a.moreoptions {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/top_sprite.png);
	background-repeat: no-repeat;
	background-position: -35px -100px;
}
#top .base ul.main_menu li.moreoptions a.moreoptions:hover {
	background-position: -70px -100px;
}
#top .base ul.main_menu>a:hover {
	text-shadow: 0px 0px 8px rgba(255,255,255,1);
}
#top .base ul.sub_menu {
	float: right;
	background: rgba(0,0,0,.95);
	position: absolute;
	top: 68px;
	right: -10px;
	width: 200px;
	padding: 0px;
	display: none;
	padding: 10px 0px;
	border-radius: 7px;
	overflow: visible;
	z-index: 2 !important;
}
#top .base ul.main_menu>li.ddlOption div.menuup {
	position: absolute;
	top: -18px;
	right: 0px;
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/menu_up.png);
	background-repeat: no-repeat;
	width: 100%;
	height: 18px;
	opacity: .9;
}
#top .base ul.sub_menu a {
	color: white;
	text-decoration: none;
	text-shadow: 0px 2px 3px rgba(0,0,0,.2);
	font-size: 12px;
	display: block;
	margin: 0px;
	padding: 12px 15px;
	position: relative;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
#top .base ul.sub_menu a:first-child {
	border-top: 1px solid rgba(255,255,255,.1);
	border-bottom: 1px solid rgba(255,255,255,.1);
}
#top .base ul.sub_menu a:not(:first-child) {
	border-bottom: 1px solid rgba(255,255,255,.1);
}
#top .base ul.sub_menu a.unread {
	background: rgba(255,255,255,.1);
}
#top .base ul.sub_menu a:hover {
	background: rgba(<?php echo $maincolor_rgb; ?>,.7);
}
#top .base ul.sub_menu h6 {
	padding: 10px;
	text-transform: uppercase;
	color: white;
	border-bottom: 1px solid rgba(255,255,255,.1);
	border-bottom: 1px solid <?php echo $maincolor; ?>;
}
#top .base ul.sub_menu li {
	position: relative;
	float: left;
	width: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
#top .base ul.notif {
	width: 422px;
}
#top .base ul.message {
	width: 422px;
	right: -55px;
}
#top .base ul.sub_menu .menuup {
	background-position: 163px bottom;
}
#top .base ul.message .menuup {
	background-position: 343px bottom;
}
#top .base ul.notif .menuup {
	background-position: 385px bottom;
}
#top .base ul.notif #notif_Container {
	float: left;
	overflow: hidden;
	width: 100%;
}
#top .base ul.notif #notif_Container::-webkit-scrollbar {
	width: 10px;
	padding: 10px;
}
#top .base ul.notif #notif_Container::-webkit-scrollbar-track {
	background-color: <?php echo $maincolor; ?>; 
}
#top .base ul.notif #notif_Container::-webkit-scrollbar-thumb {
	background-color: rgba(255,255,255,.3);

}
#top .base ul.notif #notif_Container::-webkit-scrollbar-thumb:hover {
	background-color: rgba(255,255,255,.8);
}
#top .base ul.notif #notif_Container a {
	overflow: hidden;
	resize: none;
	line-height: 170%;
}
#top .base ul.notif #notif_Container div.profpic {
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	background-color: rgba(255,255,255,.1);
	width: 38px;
	height: 38px;
	float: left;
	position: relative;
	left: -5px;
	display: inline-block;
	vertical-align: middle;
	margin-right: 5px;
	border: 2px solid rgba(255,255,255,.6);
	border-radius: 3px;
}
#top ul.main_menu a .profpicture {
	float: left;
	width: 30px;
	height: 30px;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	margin-bottom: 2px;
	margin-right: 10px;
	border: 2px solid transparent;
	border-radius: 5px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
#top ul.main_menu>a:not(.name) {
	top: 13px;
}
#top ul.main_menu a span {
	position: relative;
	top: 7px;
	color: <?php echo $maincolor; ?>;
	text-shadow: 0px 2px 1px rgba(0,0,0,.2);
}
#top ul.main_menu a:hover div {
	box-shadow: 0px 0px 8px rgba(255,255,255,.3);
}
#top ul.main_menu a.mini-icons {
	background-repeat: no-repeat;
	margin-left: 10px;
	width: 20px;
	height: 20px;
	display: inline-block;
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/top_sprite.png);
}
#top ul.main_menu a.notif {
	background-position: -35px -50px;
}
#top ul.main_menu a.message {
	background-position: -35px 0px;
}
#top ul.main_menu li.notif:hover a.notif {
	background-position: -70px -50px;
}
#top ul.main_menu li.message:hover a.message {
	background-position: -70px 0px;
}
#top ul.main_menu ul.sub_menu ul.right_sub {
	position: absolute;
	right: 208px;
	top: 0px;
	width: 300px;
	background: rgba(0,0,0,.9);
	overflow: hidden;
	padding: 0px;
	padding: 10px 0px;
	border-radius: 7px;
	overflow: visible;
	z-index: 101;
	display: none;
}
#top ul.main_menu ul.sub_menu ul.right_sub div.profpic {
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	background-color: rgba(255,255,255,.1);
	width: 28px;
	height: 28px;
	float: left;
	display: inline-block;
	vertical-align: middle;
	margin-right: 10px;
	border: 2px solid rgba(255,255,255,.6);
	border-radius: 3px;
}
#top ul.main_menu ul.sub_menu ul.right_sub div.right_arrow {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/menu_right.png) no-repeat center 10px;
	width: 8px;
	height: 100%;
	position: absolute;
	top: 0px;
	left: 100%;
}
#top ul.main_menu ul.sub_menu>li:hover ul.right_sub {
	display: block;
}


#main {
	width: 980px;
	margin: 0px auto;
	overflow: visible;
	margin-top: 30px;
	color: #333;
}
#main #body {
	width: 490px;
	float: left;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	padding-right: 10px;
}
#main #sb1, #main #sb2 {
	width: 245px;
	float: left;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
}
#main #sb1 {
	padding: 0px 10px;
}
#main #sb2 {
	padding-left: 10px;
}
#main #sb1left {
	width: 245px;
	float: left;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	padding-right: 20px;
}
#main .content {
	background: rgba(255,255,255,.75);
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-shadow: 0px 0px 10px rgba(0,0,0,.5);
	border-radius: 10px;
	margin-bottom: 20px;
	padding-bottom: 10px;
	float: left;
	width: 100%;
	overflow: hidden;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
#main .content:hover {
	background: rgba(255,255,255,.90);
}
.content h5 {
	color: white;
	text-decoration: none;
	text-shadow: 0px 2px 2px rgba(255,255,255,.3);
}
.content a.color {
	margin: 0px;
	margin-top: 10px;
}
.content hr {
	border-top: 1px solid rgba(0,0,0,.1);
}
table.title {
	width: 100%;
	margin: 0px;
	color: white;
	background: <?php echo $maincolor; ?>;
	margin-bottom: 10px;
}
table.title td {
	padding: 12px;
	overflow: visible;
	color: white;
}
table.title tr:first-child td:first-child {
	font-size: 12px;
	font-weight: bold;
}
table.title tr:first-child td:last-child {
	font-size: 12px;
	text-align: right;
}
table.title .mini_icons {
	background-repeat: no-repeat;
	background-position: top center;
	background-size: auto;
	width: 12px;
	height: 12px;
	display: block;
	margin: 0px 7px;
	position: relative;
	top: 2px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.newsfeed table.title .mini_icons {
	opacity: .5;
}
table.title a.close {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close.png);
	width: 16px;
	height: 16px;
	float: right;
}
table.title a.like {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/like.png);
	width: 17px;
	height: 16px;
	margin-top: 10px;
}
table.title a.deletepost {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close.png);
	width: 17px;
	height: 16px;
}
table.title a.moreoptions {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/menu_down.png);
	width: 16px;
	height: 8px;
	margin-top: 10px;
	position: relative;
	top: -5px;
}
table.title a.add {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/add_white.png);
	width: 14px;
	height: 14px;
	float: right;
}
table.title a.withtext {
	background-position: left center;
	padding-left: 19px;
	width: auto;
	height: auto;
	color: white;
}
table.title span:hover a.mini_icons {
	opacity: 1;
}
table.title a.mini_icons:hover {
	opacity: 1;
}
table.title span {
	display: inline-block;
	float: right;
	position: relative;
}
table.title span ul {
	background: rgba(0,0,0,.9);
	position: absolute;
	top: 16px;
	right: 5px;
	text-align: left;
	width: 120px;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 10px 0px;
	border-radius: 5px;
	display: none;
}
table.title span ul h4 {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 0px 10px;
	padding-bottom: 8px;
	font-size: 11px;
	border-bottom: 1px solid <?php echo $maincolor; ?>;
}
table.title span:not(.nohov):hover ul {
	display: block;
}
table.title span ul a {
	color: white;
	display: block;
	text-decoration: none;
	padding: 5px 10px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
table.title span ul a:hover {
	padding-left: 15px;
	background: rgba(0,0,0,.5);
}
table.title span ul a:first-child {
	border-top: 1px solid rgba(255,255,255,.1);
	border-bottom: 1px solid rgba(255,255,255,.1);
}
table.title span ul a:not(:first-child) {
	border-bottom: 1px solid rgba(255,255,255,.1);
}
table.title a.settingsicon {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/settings.png);
	background-size: contain;
	background-repeat: no-repeat;
	background-position: center center;
	width: 18px;
	height: 18px;
	display: block;
	float: right;
}
.content p.list {
	padding: 0px 7px;
	padding-bottom: 5px;
	margin: 0px;
	font-size: 12px;
	color: #222;
}
.content p.text {
	padding: 10px 15px;
	margin: 0px;
	font-size: 13px;
	color: #222;
}
.content p.default {
	padding: 5px 15px;
	padding-bottom: 10px;
	margin: 0px;
	font-size: 13px;
	color: #222;
}
.content ul.contentlist a {
	color: black;
	text-decoration: none;
	display: block;
	padding: 10px;
	font-size: 13px;
	overflow: hidden;
	box-sizing: border-box;
	position: relative;
	border-bottom: 1px solid rgba(0,0,0,.1);
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.content ul.contentlist a.selected {
	background: rgba(<?php echo $maincolor_rgb; ?>,.7);
	color: white;
}
.content ul.contentlist b.color {
	color: <?php echo $maincolor; ?>;
}
.content ul.contentlist a div.online_status {
	background: green;
	border-radius: 10px;
	width: 8px;
	height: 8px;
	display: inline-block;
	float: right;
	position: relative;
	top: 10px;
	right: 5px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.content ul.contentlist a:hover div.online_status {
	background: white;
}
.content ul.contentlist a:hover b.color {
	color: white;
}
.content ul.contentlist a div.profpic {
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	background-color: rgba(0,0,0,.2);
	width: 38px;
	height: 38px;
	float: left;
	display: inline-block;
	margin-right: 10px;
	border: 2px solid rgba(255,255,255,.6);
	border-radius: 3px;
}
.content ul.contentlist a.small div {
	width: 24px;
	height: 24px;
	vertical-align: text-bottom;
}
.content ul.contentlist a.small span {
	position: relative;
	top: 4px;
}
.content ul.contentlist a[href]:hover, .content ul.contentlist a.hoverit:hover {
	background: rgba(<?php echo $maincolor_rgb; ?>,.7);
	color: white;
}
.content table.margin-topbottom {
	width: 100%;
	margin: 0px;
}
.content table.margin-topbottom td {
	padding: 7px 10px;
}
.content .userprofilepicture {
	background-color: rgba(0,0,0,.1);
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	display: block;
	height: 225px;
	border-radius: 8px 8px 0px 0px;
}
.content .groupprofilepicture {
	background-color: rgba(0,0,0,.1);
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	display: block;
	height: 150px;
	position: relative;
}

.blackTrans {
	background: rgba(0,0,0,.8);
	width: 100%;
	height: 100%;
	position: fixed;
	top: 0px;
	left: 0px;
	display: none;
	cursor: pointer;
	z-index: 100;
	-webkit-transition: all 1s ease-in-out;
	-moz-transition: all 1s ease-in-out;
	-ms-transition: all 1s ease-in-out;
	-o-transition: all 1s ease-in-out;
	transition: all 1s ease-in-out;
}
.blackTrans:hover {
	background: rgba(0,0,0,.6);
}
.popup {
	background: #e8e8e8;
	position: fixed;
	top: 50%;
	left: 50%;
	display: none;
	border-radius: 10px;
	z-index: 103;
	cursor: pointer;
	box-shadow: 0px 0px 30px rgba(0,0,0,.9);
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.floatbutton {
	position: absolute;
	top: 100%;
	left: 0px;
	width: 100%;
}
.floatbutton input {
	margin-top: 20px;
}
.display_postlikes {
	width: 100%;
	overflow: hidden;
	max-height: 300px;
	display: block;
	position: relative;
	margin-bottom: 10px;
	overflow-y: scroll;
	border-top: 1px solid rgba(0,0,0,.1);
}
.display_postlikes::-webkit-scrollbar {
	width: 7px;
}
.display_postlikes::-webkit-scrollbar-track {
	background: transparent;
	border-radius: 5px 5px 5px 5px;
}
.display_postlikes::-webkit-scrollbar-thumb {
	border-radius: 5px 5px 5px 5px;
	background-color: rgba(0,0,0,.2);
}
.display_postlikes li {
	float: left;
	display: block;
	width: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 10px;
	font-size: 13px;
	line-height: 150%;
	border-bottom: 1px solid rgba(0,0,0,.1);
}
.display_postlikes li .profpic {
	width: 42px;
	height: 42px;
	margin-right: 8px;
	float: left;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	border: 2px solid rgba(255,255,255,.8);
	border-radius: 8px;
}
.display_postlikes li span {
	position: relative;
	top: 3px;
}
.display_postlikes li span a {
	color: <?php echo $maincolor; ?> !important;
}
table.form {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	margin: 0px;
	padding: 0px;
	font-size: 13px;
	width: 100%;
}
table.form tr[valign="top"]>td:first-child {
	padding-top: 20px;
}
table.form tr>td {
	padding: 10px 15px;
	padding-left: 15px;
}
table.nopad {
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	margin: 0px;
	padding: 0px;
	font-size: 13px;
	width: 100%;
}
table.nopad tr td {
	padding: 0px;
}
table.nopad tr td:not(:first-child) {
	padding-left: 10px;
}
.popup textarea {
	resize: none;
	width: 100%;
	height: 100px;
}
textarea.transtext {
	resize: none;
	height: auto;
	background: transparent;
	overflow: hidden;
	min-height: 50px;
	max-height: 200px;
	cursor: text;
	padding: 10px 20px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
	border-bottom: 1px solid rgba(0,0,0,.1);
}
textarea.transtext::-webkit-input-placeholder {
	color: #666;
	font-style: italic;
}
textarea.transtext::-moz-placeholder {
	color: #666;
	font-style: italic;
}
textarea.transtext:-ms-input-placeholder {
	color: #666;
	font-style: italic;
}
textarea.transtext:-moz-placeholder {
	color: #666;
	font-style: italic;
}
textarea.transtext:focus, textarea.transtext:hover {
	box-shadow: 0px 0px 0px transparent;
}
input[type="text"].transtext {
	font-size: 11px;
	padding: 10px;
}
input[type="text"].transtext::-webkit-input-placeholder {
	color: #666;
	font-style: italic;
}
input[type="text"].transtext::-moz-placeholder {
	color: #666;
	font-style: italic;
}
input[type="text"].transtext:-ms-input-placeholder {
	color: #666;
	font-style: italic;
}
input[type="text"].transtext:-moz-placeholder {
	color: #666;
	font-style: italic;
}
.newsfeed table.title a {
	text-decoration: none;
	color: white;
}
.newsfeed table.title div.profilepicture, ul.results div.profilepicture {
	width: 50px;
	height: 50px;
	margin-top: 8px;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	border: 2px solid rgba(255,255,255,.8);
	border-radius: 8px;
}
.newsfeed table.title div.profpic2 {
	width: 24px;
	height: 24px;
	margin: 0px 7px;
	position: relative;
	top: 10px;
	display: inline-block;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	border: 1px solid rgba(255,255,255,.8);
	border-radius: 4px;
}
.newsfeed table.title tr:first-child td:first-child {
	padding-left: 10px;
	width: 1px;
}
.newsfeed table.title tr:first-child td:nth-child(2) {
	line-height: 120%;
}
.newsfeed table.title tr:first-child td:last-child {
	width: 1px;
	padding-right: 10px;
}
.newsfeed table.title td.feedmsg {
	text-align: left;
	font-size: 12px;
	line-height: 150%;
	border-top: 1px solid rgba(0,0,0,.1);
}
.newsfeed table.title td.feedmsg textarea.editpost {
	resize: none;
	color: white;
	margin-top: 10px;
	padding: 10px;
	border-radius: 4px 4px 0px 0px;
	background: rgba(255,255,255,.2);
	height: auto;
	overflow: hidden;
	min-height: 200px;
	overflow-y: scroll;
	cursor: text;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
	border-bottom: 1px solid rgba(0,0,0,.1);
}
.newsfeed table.title td.feedmsg textarea.editpost::-webkit-scrollbar {
	width: 7px;
}
.newsfeed table.title td.feedmsg textarea.editpost::-webkit-scrollbar-track {
	background: transparent;
	border-radius: 5px 5px 5px 5px;
}
.newsfeed table.title td.feedmsg textarea.editpost::-webkit-scrollbar-thumb {
	border-radius: 5px 5px 5px 5px;
	background-color: rgba(0,0,0,.2);
}
.newsfeed table.title td.feedmsg a.editpost {
	background: rgba(255,255,255,.4);
	border-radius: 0px 0px 4px 4px;
	border: 0px;
	border-top: 0px solid rgba(0,0,0,.1);
}
.newsfeed table.title td.feedmsg a.editpost:hover {
	background: rgba(255,255,255,.8);
	color: <?php echo $maincolor; ?>;
	border: 0px;
	border-top: 0px solid rgba(0,0,0,.1);
}
.newsfeed table.title td.feedmsg table.nopad {
	color: white !important;
	width: 100%; 
}
.newsfeed table.title td.feedmsg table.nopad a.canceledit {
	background-repeat: no-repeat;
	background-position: center center;
	background-size: contain;
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close.png);
	float: right;
	width: 12px;
	height: 12px;
	display: block;
	margin: 0px 7px;
	position: relative;
	top: 2px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.newsfeed p.text {
	padding-top: 20px;
	padding-bottom: 10px;
	text-align: left;
}
.newsfeed table.title span.likes {
	font-size: 11px;
}
.newsfeed table.comments {
	font-size: 13px;
	float: left;
	display: block;
	width: 100%;
	margin: 0px;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	line-height: 150%;
}
.newsfeed table.comments a.viewallcomments {
	display: block;
	width: 100%;
	padding: 10px;
	text-align: right;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.newsfeed table.comments a.viewallcomments:hover {
	color: white;
	background: <?php echo $maincolor; ?>;
}
.newsfeed table.comments div.profilepicture {
	width: 36px;
	height: 36px;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	border: 2px solid rgba(255,255,255,.8);
	border-radius: 8px;
}
.newsfeed table.comments a {
	text-decoration: none;
	color: <?php echo $maincolor; ?>;
}
.newsfeed table.comments a.deletecomment {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close_green.png);
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	display: inline-block;
	width: 11px;
	height: 11px;
	margin-left: 7px;
	position: relative;
	top: 2px;
	opacity: 0;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.newsfeed table.comments textarea {
	resize: none;
	height: 35px;
	background: transparent;
	overflow: hidden;
	max-height: 200px;
	margin-top: 5px;
	cursor: text;
	padding-left: 0px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.newsfeed table.comments textarea::-webkit-input-placeholder {
	color: #666;
	font-style: italic;
}
.newsfeed table.comments textarea::-moz-placeholder {
	color: #666;
	font-style: italic;
}
.newsfeed table.comments textarea:-ms-input-placeholder {
	color: #666;
	font-style: italic;
}
.newsfeed table.comments textarea:-moz-placeholder {
	color: #666;
	font-style: italic;
}
textarea#poststatus:focus,textarea#poststatus:hover {
	box-shadow: 0px 0px 0px transparent;
}
.newsfeed table.comments textarea:focus, .newsfeed table.comments textarea:hover {
	box-shadow: 0px 0px 0px transparent;
}
.newsfeed table.comments tr td {
	padding: 8px;
	margin: 0px;
}
.newsfeed table.comments tr td:last-child {
	width: 100%;
	padding-left: 3px;
	padding-right: 15px;
}
.newsfeed table.comments tr td{
	border-bottom: 1px solid rgba(0,0,0,.1);
}
.newsfeed table.comments tr a {
	font-weight: bold;
	color: <?php echo $maincolor; ?>;
}
.newsfeed table.comments img {
	margin-left: 3px;
	position: relative;
	top: 2px;
}
.newsfeed span.date {
	font-size: 11px;
	text-align: left;
	width: 100%;
}
.newsfeed span.datec {
	font-size: 11px;
	display: block;
	color: gray;
}
.newsfeed span.likes {
	display: block;
	font-size: 12px;
	margin-bottom: 10px;
	padding: 0px 10px;
}
.newsfeed span.likes a {
	text-decoration: none;
	color: <?php echo $maincolor; ?>;
}
.newsfeed span.likes img {
	margin-right: 5px;
	position: relative;
	top: 2px;
}

.postform {
	width: 100%;
}
.postform tr td {
	padding: 0px 10px;
	padding-top: 10px;
}
.postform tr td:first-child {
	color: <?php echo $maincolor; ?>;
	font-size: 12px;
}
.postform tr td:first-child span {
	float: left;
	margin-top: 6px;
}
.postform tr td:last-child {
	padding-left: 0px;
}
.postform .profpic {
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	width: 28px;
	height: 28px;
	float: left;
	display: inline-block;
	vertical-align: middle;
	margin-right: 10px;
	border: 2px solid rgba(255,255,255,.6);
	border-radius: 3px;
}


#bigbody {
	width: 735px;
	float: left;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	padding-left: 0px 10px;
}
#superbigbody {
	width: 100%;
	float: left;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	padding-left: 0px 10px;
}
a.box-list {
	display: inline-block;
	border: 1px solid red;
}
div.box-list {
	display: inline-block;
	position: relative;
	width: 148px;
	height: 205px;
	float: left;
	margin: 10px;
	margin-left: 18px;
	overflow: hidden;
	border-top: 1px solid rgba(0,0,0,.1);
	border-bottom: 1px solid rgba(0,0,0,.1);
	z-index: 0;
}
div.box-list a div:first-child {
	width: 148px;
	height: 148px;
	background-color: rgba(0,0,0,.1);
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	position: absolute;
	top: 0px;
	left: 0px;
	display: inline-block;
}
div.box-list a div:nth-child(2) {
	width: 148px;
	position: absolute;
	top: 148px;
	color: <?php echo $maincolor; ?>;
	padding-top: 8px;
	height: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	left: 0px;
	float: left;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
div.box-list a div.createnew {
	background-image: url(<?php echo $toRoot; ?>courses/add_black.png);
	background-repeat: no-repeat;
	background-position: center center;
	background-size: initial;
}
div.box-list a div:nth-child(2) span {
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
	padding: 0px;
}
div.box-list a div:nth-child(2) span:first-child {
	display: block;
	width: 100%;
	height: 20px;
	font-weight: bold;
	overflow: hidden;
}
div.box-list a div:nth-child(2) span:last-child {
	display: block;
	width: 100%;
	height: 20px;
	overflow: hidden;
	font-size: 11px;
	color: gray;
	position: relative;
	top: 5px;
}
div.box-list a div.shadow {
	position: absolute;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/left-shadow.png) repeat-y right center;
}
div.box-list:hover a div:nth-child(2) {
	background: rgba(<?php echo $maincolor_rgb; ?>,1);
}
div.box-list:hover a div:nth-child(2) span {
	color: white;
	padding-left: 10px;
}
table.calendar {
	width: 100%;
	font-size: 13px;
	width: 100%;
	border-spacing: 0px;
    border-collapse: separate;
    margin: 0px;
}
table.calendar tr:first-child {
	background: rgba(0,0,0,.3);
	color: white;
	font-weight: bold;
}
table.calendar tr:first-child td {
	padding: 8px;
	text-align: center;
}
table.calendar tr:not(:first-child) td {
	padding: 10px;
	width: 14%;
	height: 100px;
	border-right: 1px solid rgba(0,0,0,.2);
	border-bottom: 1px solid rgba(0,0,0,.2);
	position: relative;
}
table.calendar td.today {
	background: rgba(246,217,56,.7);
}
span.event {
	color: white;
	margin-top: 10px;
	display: block;
	width: 100%;
	padding: 7px;
	font-size: 12px;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	box-shadow: 0px 0px 5px rgba(0,0,0,.5);
	background: #4884a6;
	position: relative;
}
span.event input.deleteevent {
	position: absolute;
	right: 0px;
	top: -20px;
	width: 20px;
	height: 20px;
	display: block;
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close.png);
	background-repeat: no-repeat;
	background-position: center center;
	background-size: 10px 10px;
	background-color: rgba(0,0,0,.3);
	opacity: 0;
	border: 0px;
	border-radius: 0px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
span.event:hover input.deleteevent {
	opacity: 1;
}
span.event:hover input.deleteevent:hover {
	background-color: rgba(0,0,0,.7);
}
span.assessment {
	color: white;
	margin-top: 10px;
	display: block;
	width: 100%;
	padding: 7px;
	font-size: 12px;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	box-shadow: 0px 0px 5px rgba(0,0,0,.5);
	background: #aa1616;
}
table.calendar a.addeventicon {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/add_black.png);
	width: 14px;
	height: 14px;
	display: block;
	position: absolute;
	top: 8px;
	right: 8px;
	opacity: 0;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
table.calendar td:hover a.addeventicon {
	opacity: .5;
}
table.calendar td:hover a.addeventicon:hover {
	opacity: 1;
}


table.titlecalendar {
	width: 100%;
	margin: 0px;
	color: white;
	background: <?php echo $maincolor; ?>;
	border-radius: 8px 8px 0px 0px;
	margin-bottom: 10px;
}
table.titlecalendar td {
	padding: 10px;
}
table.titlecalendar tr:first-child td:last-child, table.titlecalendar tr:first-child td:first-child {
	font-size: 12px;
	width: 1px;
}
table.titlecalendar tr:first-child td:nth-child(2) {
	text-align: center;
}
table.titlecalendar .mini_icons {
	width: 15px;
	height: 15px;
	display: block;
	opacity: .8;
	margin: 0px 5px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
table.titlecalendar a.prev {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/arrow_left_white.png) no-repeat center center;
}
table.titlecalendar a.next {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/arrow_right_white.png) no-repeat center center;
}
table.titlecalendar a.mini_icons:hover {
	opacity: 1;
}

table.folder {
	color: white;
	font-size: 14px;
	font-weight: bold;
	width: 100%;
	border
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
table.folder td:first-child {
	padding: 10px 0px 7px 15px;
	background: rgba(0,0,0,.4);
}
table.folder td:nth-child(2) {
	background: rgba(0,0,0,.4);
	border-radius: 0px 10px 10px 0px;
	text-align: right;
	padding-right: 15px;
}
table.folder td:nth-child(2) input {
	width: 14px;
	height: 14px;
	border: 0px;
	background-color: transparent;
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close.png);
	background-size: 14px 14px;
	padding: 0px;
	margin: 0px;
	margin-top: 5px;
	opacity: 0;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
table.folder:hover td:nth-child(2) input {
	opacity: .8;
}
table.folder td:nth-child(2) input:hover {
	background-color: transparent;
	box-shadow: 0px 0px 0px transparent;
}
table.folder td:last-child {
	width: 200px;
	padding-right: 20px;
}
table.folder a {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/add_white.png) no-repeat 10px center, rgba(<?php echo $maincolor_rgb; ?>,.5);
	right: 0px;
	padding: 7px;
	font-size: 12px;
	padding-left: 30px;
	border-radius: 10px;
	border: 2px solid white;
	color: white;
	opacity: 0;
	display: inline-block;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
table.folder img {
	margin-right: 10px;
	vertical-align: middle;
	margin-bottom: 5px;
}
table.folder:hover a {
	opacity: .8;
}
table.folder a:first-child {
	border-radius: 10px 0px 0px 10px;
	border-right: 0px;
	margin-right: -4px;
}
table.folder a:not(:first-child):not(:last-child) {
	border-radius: 0px;
	border-right: 0px;
	margin-right: -4px;
}
table.folder a:last-child {
	border-radius: 0px 10px 10px 0px;
}
table.folder a:hover {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/add_white.png) no-repeat 10px center, <?php echo $maincolor; ?>;
}
table.folder a.one {
	border-radius: 10px;
	border: 3px solid white;
}



.content ul.filelist>a {
	color: black;
	text-decoration: none;
	display: block;
	padding: 10px;
	font-size: 13px;
	cursor: default;
	padding: 10px 10px 7px 15px;
	border-bottom: 1px solid rgba(0,0,0,.1);
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.content ul.filelist ul a.list {
	border-radius: 10px 0px 0px 10px;
}
.content ul.filelist>a>img {
	margin-right: 10px;
	vertical-align: middle;
	margin-bottom: 5px;
}
.content ul.filelist>a div.profpic {
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	width: 24px;
	height: 24px;
	display: inline-block;
	vertical-align: middle;
	margin-right: 10px;
	border: 2px solid rgba(255,255,255,.6);
	border-radius: 3px;
}
.content ul.filelist>a:not([title="No files"]):hover {
	background: rgba(<?php echo $maincolor_rgb; ?>,.7);
	padding-left: 15px;
	color: white;
}
.content ul.filelist form {
	float: right;
	margin-top: 2px;
	overflow: visible;
}
.content ul.filelist input.button {
	float: left;
	width: 14px;
	margin-left: 10px;
	margin-right: 5px;
	height: 14px;
	padding: 0px;
	border: 0px;
	position: relative;
	top: 3px;
	opacity: 0;
}
.content ul.filelist input.downloadfile {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/download_white.png);
}
.content ul.filelist input.deletefile {
	background: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/close.png);
	background-size: 14px auto;
}
.content ul.filelist a:hover input.button {
	opacity: .5;
}
.content ul.filelist a:hover input.button:hover {
	opacity: 1;
	box-shadow: 0px 0px 0px transparent;
	border: 0px solid transparent;
}

.content ul.filelist ul {
	padding-left: 30px;
}
.content ul.filelist table {
	margin-left: 30px;
	margin-top:30px;
	width: calc(100% - 30px);
}
.content ul.filelist table td:first-child {
	border-radius: 10px 0px 0px 10px;
}
.content ul.filelist table td:last-child a:first-child {
	margin-right: -2px;
}
.content ul.filelist table td:last-child a:not(:first-child):not(:last-child) {
	margin-right: -2px;
}


.content div.bottommenu {
	width: 100%;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 10px;
	background: rgba(0,0,0,.2);
	color: white;
	display: block;
	position: relative;
	top: 10px;
	border-radius: 0px 0px 8px 8px;
	overflow: hidden;
}
.content div.bottommenu a {
	float: left;
	color: white;
	display: inline-block;
	height: 14px;
	padding-left: 20px;
	margin-right: 10px;
	padding-bottom: 2px;
	background-repeat: no-repeat;
	background-position: left center;
	font-size: 12px;
	opacity: .5;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.content div.bottommenu a:hover {
	opacity: 1;
}
.content div.bottommenu a.add {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/add_white.png);
}
.content table.results {
	width: 100%;
	font-size: 13px;
}
.content table.results td:first-child {
	width: 64px;
}
.content table.results td:nth-child(2) {
	padding-left: 10px;
	padding-top: 15px;
	font-weight: bold;
}
.content table.results td.button {
	width: 100px;
	padding-top: 15px;
	padding-left: 10px;
}

ul.results li {
	padding: 10px;
	border-bottom: 1px solid rgba(0,0,0,.1);
}

#profileHeader {
	width: 100%;
	float: left;
	display: block;
	margin-bottom: 20px;
	overflow: hidden;
}
#profileHeader .userprofilepicture {
	background-color: rgba(0,0,0,.1);
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center center;
	display: block;
	width: 200px;
	height: 200px;
	border-radius: 150px;
	margin: 0px auto;
	margin-bottom: 7px;
	box-shadow: 0px 0px 5px rgba(255,255,255,.3);
}
#profileHeader h2 {
	color: white;
	text-shadow: 0px 2px 1px rgba(0,0,0,.5);
	text-align: center;
}
.profileinfo table.profile h2 {
	text-align: center;
	text-shadow: 0px 2px 1px rgba(0,0,0,.3);
}
.profileinfo ul.profilemenu {
	display: inline;
	margin: 0px auto;
}
.profileinfo ul.profilemenu li {
	display: inline;
	margin: 0px;
}
.profileinfo ul.profilemenu li a {
	padding: 5px 10px 10px 10px;
	display: inline-block;
	font-size: 12px;
	color: <?php echo $maincolor; ?>;
	margin: 0px;
	border-bottom: 2px solid transparent;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.profileinfo ul.profilemenu li a:hover,.profileinfo ul.profilemenu li a.selected {
	border-bottom: 2px solid <?php echo $maincolor; ?>;
	font-weight: bold;
}

.newsfeedRefresh {
	float: left;
	width: 100%;
}
.newsfeedRefresh span {
	background: rgba(255,255,255,.75);
	font-size: 12px;
	margin-bottom: 0px;
	overflow: hidden;
	width: 100%;
	display: block;
	height: 0px;
	font-weight: bold;
	color: <?php echo $maincolor; ?>;
	text-align: center;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 0px 0px;
	border-radius: 10px;
	cursor: pointer;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.newsfeedRefresh span:hover {
	background: rgba(255,255,255,.9);
}

span.message {
	background: <?php echo $maincolor; ?>;
	font-size: 12px;
	color: white;
	width: 100%;
	display: block;
	padding: 0px 10px;
	height: 0px;
	overflow: hidden;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
span.error {
	background: #aa1616;
	font-size: 12px;
	color: white;
	width: 100%;
	display: block;
	padding: 0px 10px;
	height: 0px;
	overflow: hidden;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.wrap {
	white-space: pre-wrap; 
	white-space: -moz-pre-wrap;
	white-space: -pre-wrap;
	white-space: -o-pre-wrap; 
	word-wrap: break-word;
}
img.loadingGif {
	width: 24px;
	height: 24px;
}
.transwhitetext {
	background: rgba(0,0,0,.1);
}
.datetimewidget h1 {
	margin: 0px;
	padding: 5px 10px;
	text-align: center;
}
.datetimewidget img.loadingGif {
	margin-top: 10px;
}
.margintop10 {
	margin-top: 10px;
}








.content table.list {
	font-size: 13px;
}
table.list {
	font-size: 14px;
	width: 100%;
}
table.list tr.title {
	background: rgba(0,0,0,.3);
	color: white;
	font-weight: bold;
}
table.list td {
	padding: 12px;
}
table.list tr.title td {
	padding: 10px;
}
table.list tr:not(:first-child) td {
	border-bottom: 1px solid rgba(0,0,0,.1);
}
table.list tr.borderbottom td {
	border-bottom: 1px solid rgba(0,0,0,.1);
}
table.list .mini_icons {
	background-repeat: no-repeat;
	background-position: top center;
	background-size: cover;
	width: 16px;
	height: 16px;
	opacity: .5;
	display: inline-block;
	float: left;
	margin: 0px 7px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
table.list .mini_icons:hover {
	opacity: 1;
}
table.list .view {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/edit_green.png);
}
table.list .deletebtn {
	background-image: url(<?php echo $toRoot; ?>images/skin/<?php echo $skin; ?>/bg/delete_green.png);
}
.assessment_mini_icons {
	background-repeat: no-repeat;
	background-position: center center;
	background-size: cover;
	width: 36px;
	height: 36px;
	display: inline-block;
	margin: 8px 0px;
	margin-left: 15px;
	overflow: hidden;
}
.copyicon {
	background-image: url(<?php echo $toRoot; ?>/images/skin/<?php echo $skin; ?>/bg/copy_green.png);
}
.giveicon {
	background-image: url(<?php echo $toRoot; ?>/images/skin/<?php echo $skin; ?>/bg/give_green.png);
}
.deleteicon {
	background-image: url(<?php echo $toRoot; ?>/images/skin/<?php echo $skin; ?>/bg/delete_green.png);
}

.qlist {
	z-index: 50;
	-moz-box-sizing: border-box;
	box-sizing: border-box;	;
	overflow: hidden;
	float: left;
	display: block;
	width: 100%;
	height: 0px;
	min-height: 50px;
	font-size: 13px;
	border-bottom: 1px solid rgba(0,0,0,.1);
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
	cursor: pointer;
}
.qlist .showQuestion {
	padding: 10px;
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out;
}
.qlist .editQuestion {
	padding: 10px 5px;
}
.qlist:nth-child(even) {
	background: rgba(0,0,0,.1);
}
.qlist[isview="0"]:hover {
	background: rgba(<?php echo $maincolor_rgb; ?>,.3);
}
.qlist[isview="1"] {
	height: auto;
	cursor: default;
	line-height: 150%;
	border-top: 3px solid <?php echo $maincolor; ?>;
}
.qlist[isview="1"] .showQuestion {
	padding: 20px;
}
.qlist .qsettings {
	float: right;
	margin: 10px;
	margin-top: 0px;
	width: 24px;
	display: none;
}
.qlist .editq, .qlist .deleteq {
	background-position: center center;
	background-repeat: no-repeat;
	width: 24px;
	height: 24px;
	float: left;
	margin-bottom: 10px;
	display: block;
	cursor: pointer;
}
.qlist .editq {
	background-image: url(<?php echo $toRoot;?>images/skin/<?php echo $skin; ?>/bg/edit_green.png);
}
.qlist .deleteq {
	background-image: url(<?php echo $toRoot;?>images/skin/<?php echo $skin; ?>/bg/close_green.png);
}
.qlist ul.choices {
	padding: 10px 20px;
}
.qlist ul.choices li {
	border-left: 3px solid rgba(0,0,0,.4);
	padding: 5px 0px;
	padding-left: 5px;
	margin-bottom: 8px;
}
.qlist ul.choices li.correct {
	border-left: 10px solid rgba(<?php echo $maincolor_rgb; ?>,1);
}

/*
.gridView {
	overflow-x: scroll;
	display: inline-block;
	float: left;
	width: 100%;
	padding: 100px 0px;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
table.gradesheet {
	font-size: 13px;
	table-layout: fixed;
}
table.gradesheet, table.gradesheet td, table.gradesheet th {
	border-collapse: collapse;
	border: 1px solid rgba(0,0,0,.1);
	padding: 10px;
}
table.gradesheet div.profpic {
	width: 20px;
	height: 20px;
	margin-right: 8px;
	float: left;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	border-radius: 18px;
}
table.gradesheet tr.assessment_list td {
	min-width: 100px;
}
table.gradesheet tr.stud td:first-child {
	min-width: 300px;
}
table.gradesheet tr.stud td:not(:first-child) {
	text-align: right;
}
table.gradesheet tr.stud td:last-child {
	min-width: 150px;
}*/



@media(max-width: 980px) {
	#top {
		position: absolute;
	}
}