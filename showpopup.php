<?php
require_once('library/User.php');
require_once('library/Course.php');
require_once('library/Group.php');
require_once('library/Calendar.php');
require_once('library/Resources.php');
require_once('library/Assessment.php');
require_once('library/ImageHandling.php');
require_once('library/Newsfeed.php');
session_start();
date_default_timezone_set('Asia/Manila');
if(isset($_SESSION['username'])) {
	$status = "LoggedIn";
	$library = array(
		'user' => new User(),
		'course' => new Course(),
		'group' => new Group(),
		'calendar' => new Calendar(),
		'assessment' => new Assessment(),
		'resources' => new Resources(),
		'imageh' => new ImageHandling(),
		'newsfeed' => new Newsfeed()
	);
	$loggedUser = $library['user']->GetID($_SESSION['username']);
	$userType = $library['user']->GetType($loggedUser);
}
$skin = "float-transparency";
if(isset($_GET['action'])) {
	$action = $_GET['action'];
	if($action == "showpopup") {
		if(isset($_POST['popup'])) {
			$popup = $_POST['popup'];


			if(isset($_POST['date']))
				$date = $_POST['date'];

			if(isset($_POST['fixedCourseKey']))
				$fixedCourseKey = $_POST['fixedCourseKey'];

			if(isset($_POST['fixedGroupKey']))
				$fixedGroupKey = $_POST['fixedGroupKey'];

			if(isset($_POST['courseID']))
				$courseID = $_POST['courseID'];

			if(isset($_POST['groupID']))
				$groupID = $_POST['groupID'];

			if(isset($_POST['assessID']))
				$assessID = $_POST['assessID'];

			if(isset($_POST['parentfolder']))
				$parentfolder = $_POST['parentfolder'];

			require_once("popup/$popup.php");
		}
	}
}
?>