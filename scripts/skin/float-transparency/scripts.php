<script src="scripts/skin/<?php echo $skin; ?>/jquery.min-1.8.0.js"></script>
<script src="scripts/skin/<?php echo $skin; ?>/jquery.timers-1.0.0.js"></script>
<script>
$(function() {
	$('a[href*=#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});
});
</script>









<script type="text/javascript">
var observe;
if (window.attachEvent) {
	observe = function (element, event, handler) {
		element.attachEvent('on'+event, handler);
	};
}
else {
	observe = function (element, event, handler) {
		element.addEventListener(event, handler, false);
	};
}
function init () {
	$('.resizabletext').css('height','35px');
	$('.resizabletext').focusout(function() {
		$(this).css('height','35px');
	})
	.on('keyup',function(){
		$(this).css('height','auto');
		$(this).height(this.scrollHeight-28);
	});

	//preload images
	imageObj = new Image();


	images = new Array();
	<?php 
	$imgarr = 0;
	$bg = array("add_black.png", "add_white.png", "loading.gif", "download_black.png", "download_green.png", "left-shadow.png", "menu_right.png", "menu_up.png", "top_sprite.png", "search.png", "settings.png", "edit_black.png", "edit_white.png", "edit_green.png", "edit_yellow.png");
	for($i = 0; $i < sizeof($bg); $i++) {
		echo 'images['.$imgarr.']="images/skin/'.$skin.'/bg/'.$bg[$i].'";';
		$imgarr++;
	}
	$mini_icons = array("close.png", "close_green.png", "like.png", "like_comment.png");
	for($i = 0; $i < sizeof($mini_icons); $i++) {
		echo 'images['.$imgarr.']="images/skin/'.$skin.'/mini-icons/'.$mini_icons[$i].'";';
		$imgarr++;
	}
	?>
	for(i=0; i<<?php echo $imgarr; ?>; i++) 
	{
	  imageObj.src=images[i];
	}
}
$(window).scroll(function(){ 
	var x = 30;
	var pos = $(window).scrollTop();
	if(pos > x) {
		/*$("#top").css({
			"background": "rgba(0,124,90,.95)",
			"box-shadow": "0px 5px 0px rgba(255,255,255, .6)"
		});
		$("#top ul.main_menu a span:not(.alert)").css({
			"color": "white"
		});
		$("#top .base a.logo").css({
			"color": "white"
		});
		$("#top ul.main_menu a .profpicture").css({
			"border": "2px solid transparent"
		});
		$("#top .base ul.main_menu a.notif").css({
			"background-position": "0px -50px"
		});
		$("#top .base ul.main_menu a.message").css({
			"background-position": "0px 0px"
		});
		$("#top .base ul.main_menu a.moreoptions").css({
			"background-position": "0px -100px"
		});
		$("#top .base ul.main_menu li.notif").hover(function(e) { 
			$("#top .base ul.main_menu a.notif").css("background-position",e.type === "mouseenter"?"-70px -50px":"0px -50px") 
		});
		$("#top .base ul.main_menu li.message").hover(function(e) { 
			$("#top .base ul.main_menu a.message").css("background-position",e.type === "mouseenter"?"-70px 0px":"0px 0px") 
		});
		$("#top .base ul.main_menu li.moreoptions").hover(function(e) { 
			$("#top .base ul.main_menu a.moreoptions").css("background-position",e.type === "mouseenter"?"-70px -100px":"0px -100px") 
		});*/
	} else {
		/*$("#top").css({
			"background": 'rgba(255,255,255,.8)',
			"box-shadow": '0px 5px 0px rgba(0,124,90, .6)'
		});
		$("#top ul.main_menu a span:not(.alert)").css({
			"color": "#007c5a"
		});
		$("#top .base a.logo").css({
			"color": "#007c5a"
		});
		$("#top ul.main_menu a .profpicture").css({
			"border": "2px solid transparent"
		});
		$("#top .base ul.main_menu a.notif").css({
			"background-position": "-35px -50px"
		});
		$("#top .base ul.main_menu a.message").css({
			"background-position": "-35px 0px"
		});
		$("#top .base ul.main_menu a.moreoptions").css({
			"background-position": "-35px -100px"
		});
		$("#top .base ul.main_menu li.notif").hover(function(e) { 
			$("#top .base ul.main_menu a.notif").css("background-position",e.type === "mouseenter"?"0px -50px":"-35px -50px") 
		});
		$("#top .base ul.main_menu li.message").hover(function(e) { 
			$("#top .base ul.main_menu a.message").css("background-position",e.type === "mouseenter"?"0px 0px":"-35px 0px") 
		});
		$("#top .base ul.main_menu li.moreoptions").hover(function(e) { 
			$("#top .base ul.main_menu a.moreoptions").css("background-position",e.type === "mouseenter"?"0px -100px":"-35px -100px") 
		});*/
	}
	
});
</script>