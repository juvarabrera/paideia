<?php
$toRoot = "";
require_once('library/Config.php');

if($status == "LoggedIn") {
	if(isset($_GET['-_-']) && isset($_GET['post'])) {
		$postID = $_GET['post'];
		$viewProfileOf = $library['user']->GetID($_GET['-_-']);
		if($viewProfileOf == "")
			$viewProfileOf = $loggedUser;
		if(!$library['user']->CheckIfUserPost($viewProfileOf, $postID)) {
			header("Location: index.php?-_-=".$library['user']->GetUsername($viewProfileOf));
		}
		$bTitle =  $library['user']->GetName($viewProfileOf);
	} elseif(isset($_GET['-_-'])) {
		$viewProfileOf = $library['user']->GetID($_GET['-_-']);
		if($viewProfileOf == "")
			$viewProfileOf = $loggedUser;
		$bTitle =  $library['user']->GetName($viewProfileOf);
	} else
		$bTitle = $library['user']->GetName($loggedUser).' - Schoolbook';
	if(isset($_GET['-_-']))
		require_once('template/body_Profile.php');
	else 
		require_once('template/body_LoggedIn.php');
} else {
	$bTitle = 'Paideia | Online Learning';
	require_once('template/body_Login.php');
}
?>
