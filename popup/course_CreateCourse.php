<div id="popup_CreateCourse">
	<table class="title"><tr>
		<td>Create Course</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=createcourse" method="post">
	<table class="form">
		<tr>
			<td width="95px">Course Name:</td>
			<td><input type="text" name="coursename" maxlength="30" required></td>
		</tr>
		<tr valign="top">
			<td>Description:</td>
			<td><textarea name="coursedescription" maxlength="500"></textarea></td>
		</tr>
		<tr>
			<td>Course Type:</td>
			<td><label><input type="radio" name="type" value="Open" checked="checked">Open</label><label><input type="radio" name="type" value="Close">Close</label></td>
		</tr>
	</table>
	<div class="floatbutton"><input type="submit" class="color" value="Create Course"></div>
	</form>
</div>