<div id="popup_EditCourse">
	<table class="title"><tr>
		<td>Edit Course</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=editcourse" method="post" enctype="multipart/form-data">
	<table class="form">
		<tr>
			<td width="95px">Course Name:</td>
			<td><input type="text" name="coursename" maxlength="30" value="<?php echo $library['course']->GetCourseName($courseID)?>" required></td>
		</tr>
		<tr valign="top">
			<td>Description:</td>
			<td><textarea name="coursedescription" maxlength="500"><?php echo str_replace("<br />", "\n", $library['course']->GetCourseDescription($courseID)); ?></textarea></td>
		</tr>
		<tr>
			<td>Course Type:</td>
			<td>
			<?php
			$courseType = $library['course']->GetCourseType($courseID);
			$types = array("Open", "Closed");
			foreach($types as $type) {
				$checked = "";
				if($courseType == $type)
					$checked = " checked=\"checked\"";
				echo '<label><input type="radio" name="type" value="'.$type.'"'.$checked.'>'.$type.'</label>';
			}
			?>
		</tr>
	</table>
	<input type="hidden" name="courseid" value="<?php echo $courseID; ?>">
	<div class="floatbutton">
		<input type="submit" class="color" value="Save"></form>
		<form action="process.php?action=deletecourse" method="post">
			<input type="submit" class="delete" value="Delete Course" onclick="if(!confirm('Are you sure you want to delete this course?')) { return false; }">
			<input type="hidden" name="idToDelete" value="<?php echo $courseID; ?>">
		</form>
	</div>
</div>