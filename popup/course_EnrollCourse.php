<div id="popup_EnrollCourse">
	<table class="title"><tr>
		<td>Enroll Course</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=enrollcourse" method="post" autocomplete="off">
	<table class="form">
		<tr>
			<td width="95px">Course Key:</td>
			<td><input type="text" name="coursekey" maxlength="6" required></td>
		</tr>
		<tr>
			<td></td>
			<td><small>*Course Key is provided by your professors.</small></td>
		</tr>
	</table>
	<?php
	if(isset($fixedCourseKey))
		echo '<input type="hidden" name="courseid" value="'.$fixedCourseKey.'">';
	?>
	<div class="floatbutton"><input type="submit" class="color" value="Enroll Course"></div>
	</form>
</div>