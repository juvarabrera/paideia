<div id="popup_GiveAssessment">
	<table class="title"><tr>
		<td>Give Out Assessment</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=giveassessment" method="post">
	<table class="form">
		<tr>
			<td></td>
			<td><label id="lblDateAndTime"><input type="radio" name="set" value="dateandtime" checked>Set exact date and time.</label></td>
		</tr>
		<tr>
			<td><b>Due Date: </b></td><td><input type="date" name="duedate" id="i_duedate" required min="<?php echo $dateToday; ?>" max="2015-12-31"></td>
		</tr>
		<tr>
			<td><b>Due Time: </b></td><td><input type="time" name="duetime" id="i_duetime" required></td>
		</tr>
		<tr>
			<td></td>
			<td><label id="lblDayHourMinute"><input type="radio" name="set" value="dayhourminute">Set exact days, hours, and minutes.</label></td>
		</tr>
		<tr>
			<td><b>Due after: </b></td><td>
			<table class="nopad">
				<tr>
					<td><input type="number" max="7" min="0" name="timer_Days" id="i_timer_Days" placeholder="Days"></td>
					<td>d</td>
					<td><input type="number" max="23" min="0" name="timer_Hours" id="i_timer_Hours" placeholder="Hours"></td>
					<td>h</td>
					<td><input type="number" max="59" min="0" name="timer_Minutes" id="i_timer_Minutes" placeholder="Minutes"></td>
					<td>m</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><small>*Set the due of the assessment.</small></td>
		</tr>
	</table>
	<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
	<div class="floatbutton"><input type="submit" name="submit" class="color" value="Give Assessment to Students"></div>
	</form>
</div>
<script>
$(document).ready(function() {
	$('#lblDateAndTime, #i_duedate, #i_duetime').click(function() {
		$('#i_duedate').prop('required', true);
		$('#i_duetime').prop('required', true);
		$('#i_timer_Days').prop('required', false);
		$('#i_timer_Hours').prop('required', false);
		$('#i_timer_Minutes').prop('required', false);
		$('#lblDateAndTime input[type="radio"]').prop('checked', true);
		$('#lblDayHourMinute input[type="radio"]').prop('checked', false);
	});
	$('#lblDayHourMinute, #i_timer_Days, #i_timer_Hours, #i_timer_Minutes').click(function() {
		$('#i_duedate').prop('required', false);
		$('#i_duetime').prop('required', false);
		$('#i_timer_Days').prop('required', true);
		$('#i_timer_Hours').prop('required', true);
		$('#i_timer_Minutes').prop('required', true);
		$('#lblDateAndTime input[type="radio"]').prop('checked', false);
		$('#lblDayHourMinute input[type="radio"]').prop('checked', true);
	});
});
</script>