<div id="popup_CreateQuestion_TrueOrFalse">
	<table class="title"><tr>
		<td>Create Question (True or False)</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=createquestion_trueorfalse" method="post">
	<table class="form">
		<tr valign="top">
			<td width="95px">Question:</td>
			<td><textarea name="question" maxlength="500" required></textarea></td>
		</tr>
		<tr>
			<td>Answer:</td>
			<td>
			<select name="answer">
				<option value="True">True</option>
				<option value="False">False</option>
			</select>
			</td>
		</tr>
		<tr>
			<td>Points:</td>
			<td>
			<input type="number" min="1" max="100" name="points" placeholder="Default value: 1 point" required></td>
		</tr>
	</table>
	<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
	<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
	<div class="floatbutton"><input type="submit" name="submit" class="color" value="Create Question"></div>
	</form>
</div>