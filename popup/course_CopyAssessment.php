<div id="popup_CopyAssessment">
	<table class="title"><tr>
		<td>Copy Assessment</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=copyassessment" method="post">
	<table class="form">
		<tr>
			<td width="95px">Handled Courses:</td>
			<td>
			<select name="courseID">
				<?php
				$row = $library['course']->GetAllCoursesHandledBy($userID);
				for($i = 0; $i < sizeof($row); $i++) {
					$coursename = $library['course']->GetCourseName($row[$i]);
					echo '<option value="'.$row[$i].'">'.$coursename.'</option>';
				}
				?>
			</select>
			</td>
		</tr>
		<tr>
			<td></td>
			<td><small>*This assessment will be copy to other courses.</small></td>
		</tr>
	</table>
	<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
	<div class="floatbutton"><input type="submit" name="submit" class="color" value="Make a Copy"></div>
	</form>
</div>