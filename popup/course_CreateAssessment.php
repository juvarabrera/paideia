<div id="popup_CreateAssessment">
	<table class="title"><tr>
		<td>Create Assessment</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=createassessment" method="post">
	<table class="form">
		<tr>
			<td width="95px">Name:</td>
			<td><input type="text" name="assessname" maxlength="30" required></td>
		</tr>
		<tr>
			<td>Max Attempts:</td>
			<td>
			<select style="width: 100%;" name="attempts">
				<?php
				for($i = 1; $i <= 10; $i++) {
					echo '<option value="'.$i.'">'.$i.'</option>';
				}
				?>
			</select></td>
		</tr>
		<tr>
			<td>Timer:</td>
			<td>
			<table class="nopad">
				<tr>
					<td><input type="number" max="5" min="0" name="timer_Hours" id="timer_Hours" placeholder="Hours" required></td>
					<td>h</td>
					<td><input type="number" max="59" min="0" name="timer_Minutes" id="timer_Minutes" placeholder="Minutes" required></td>
					<td>m</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
	<div class="floatbutton"><input type="submit" class="color" value="Create Assessment"></div>
	</form>
</div>