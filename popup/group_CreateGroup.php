<div id="popup_CreateGroup">
	<table class="title"><tr>
		<td>Create Group</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=creategroup" method="post" autocomplete="off">
	<table class="form">
		<tr>
			<td width="95px">Group Name:</td>
			<td><input type="text" name="groupname" required></td>
		</tr>
		<tr valign="top">
			<td>Description:</td>
			<td><textarea name="groupdescription"></textarea></td>
		</tr>
		<tr>
			<td>Group Type:</td>
			<td><label><input type="radio" name="type" value="Open" checked="checked">Open</label><label><input type="radio" name="type" value="Close">Close</label></td>
		</tr>
	</table>
	<div class="floatbutton"><input type="submit" class="color" value="Create Group"></div>
	</form>
</div>