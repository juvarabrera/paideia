<div id="popup_EditGroup">
	<table class="title"><tr>
		<td>Edit Group</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=editgroup" method="post" enctype="multipart/form-data">
	<table class="form">
		<tr>
			<td width="95px">Group Name:</td>
			<td><input type="text" name="groupname" value="<?php echo $library['group']->GetGroupName($groupID); ?>" required></td>
		</tr>
		<tr valign="top">
			<td>Description:</td>
			<td><textarea name="groupdescription"><?php echo str_replace("<br />", "\n", $library['group']->GetGroupDescription($groupID)); ?></textarea></td>
		</tr>
		<tr>
			<td>Course Type:</td>
			<td>
			<?php
			$courseType = $library['group']->GetGroupType($groupID);
			$types = array("Open", "Closed");
			foreach($types as $type) {
				$checked = "";
				if($courseType == $type)
					$checked = " checked=\"checked\"";
				echo '<label><input type="radio" name="type" value="'.$type.'"'.$checked.'>'.$type.'</label>';
			}
			?>
		</tr>
	</table>
	<input type="hidden" name="groupid" value="<?php echo $groupID; ?>">
	<div class="floatbutton">
		<input type="submit" class="color" value="Save"></form>
		<form action="process.php?action=deletegroup" method="post">
			<input type="submit" class="delete" value="Delete Group" onclick="if(!confirm('Are you sure you want to delete this group?')) { return false; }">
			<input type="hidden" name="idToDelete" value="<?php echo $groupID; ?>">
		</form>
	</div>
</div>