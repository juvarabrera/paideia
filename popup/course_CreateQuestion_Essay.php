<div id="popup_CreateQuestion_Essay">
	<table class="title"><tr>
		<td>Create Question (Essay)</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=createquestion_essay" method="post">
	<table class="form">
		<tr valign="top">
			<td width="95px">Question:</td>
			<td><textarea name="question" maxlength="500" required></textarea></td>
		</tr>
		<tr>
			<td>Minimum Characters:</td>
			<td>
			<input type="number" min="1" max="5000" name="chars" placeholder="Default value: 100 characters" required>
			</td>
		</tr>
		<tr>
			<td>Points:</td>
			<td>
			<input type="number" min="1" max="100" name="points" placeholder="Default value: 1 point" required></td>
		</tr>
	</table>
	<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
	<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
	<div class="floatbutton"><input type="submit" name="submit" class="color" value="Create Question"></div>
	</form>
</div>