<?php
unset($_SESSION['choices']);
?>
<div id="popup_CreateQuestion_MultipleChoice">
	<table class="title"><tr>
		<td>Create Question (Multiple Choice)</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=createquestion_multiplechoice" method="post">
	<table class="form">
		<tr valign="top">
			<td width="95px">Question:</td>
			<td><textarea name="question" maxlength="500" style="resize: none;" required></textarea></td>
		</tr>
		<tr>
			<td>Add Choice:</td>
			<td>
			<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.Gif" class="loadingGif" id="loadingAddChoice" style="display: none; margin-top: 8px;"></center>
			<input type="text" name="addchoice" id="txtAddChoice" placeholder="Press 'Enter' to add choice">
			<script>
			$(document).ready(function() {
				$('#txtAddChoice').keypress(function(event) {
					if(event.keyCode == 13) {
						$text = $(this).val();
						if($text != "") {
							$('#loadingAddChoice').css({"display": "block"});
							$(this).css({"display": "none"});
							$(this).val('');
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=addchoice",
								data: {choice: $text},
								success: function() {
									$.ajax({
										cache: false,
										url: "process.php?action=refreshchoices",
										success: function(html) {
											$('#popupChoices').html(html);
											$('#loadingAddChoice').css({"display": "none"});
											$('#txtAddChoice').css({"display": "block"});
											$('#txtAddChoice').focus();
										}
									});
								}
							});
						}
						event.preventDefault();
						return false;
					}
				});
			});
			</script>
			</td>
		</tr>
		<tr valign="top">
			<td>Choices:<br><br><small>*<i>Select the correct answer</i></small></td>
			<td id="popupChoices">
			<select name="answer" id="choicestoanswer" disabled>
				<option value="no">-- You have not added any choices --</option>
			</select>
			<a class="inputbutton" id="btnRemoveChoice" style="margin: 0px; margin-top: 20px;" disabled>Remove from Choices</a>
			</td>
		</tr>
		<tr>
			<td>Points:</td>
			<td>
			<input type="number" min="1" max="100" name="points" placeholder="Default value: 1 point" required></td>
		</tr>
	</table>
	<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
	<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
	<div class="floatbutton"><input type="submit" name="submit" id="btnSubmit_CreateQuestion_MultipleChoice" class="color" value="Create Question"></div>
	</form>
</div>
<script>	
$(document).ready(function() {
	$('#btnSubmit_CreateQuestion_MultipleChoice').click(function(event) {
		$val = $('#choicestoanswer').val();
		if($val == "no") {
			alert("Please add choices to question.");
			event.preventDefault();
			return false;
		}
	});
});
</script>