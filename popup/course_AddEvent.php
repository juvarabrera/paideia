<div id="popup_AddEvent">
	<table class="title"><tr>
		<td>Add Event</td>
		<td><a class="mini_icons close" onclick="showPopup()"></a></td>
	</tr></table>
	<form action="process.php?action=addevent" method="post">
	<table class="form">
		<tr>
			<td width="95px">Event Name:</td>
			<td><input type="text" name="eventname" maxlength="30" required></td>
		</tr>
		<tr>
			<td width="95px">Start Date:</td>
			<td><input type="date" id="sd" value="" name="startdate" min="2014-01-01" max="2015-12-31" required></td>
		</tr>
		<tr>
			<td width="95px">Start Time:</td>
			<td><input type="time" id="st" value="" name="starttime" required></td>
		</tr>
		<tr>
			<td width="95px">End Date:</td>
			<td><input type="date" id="ed" name="enddate" min="2014-01-01" max="2015-12-31" required></td>
		</tr>
		<tr>
			<td width="95px">End Time:</td>
			<td><input type="time" id="et" name="endtime" required></td></td>
		</tr>
	</table>
	<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
	<div class="floatbutton"><input type="submit" class="color" value="Add Event"></div>
	</form>
</div>
<script>
document.getElementById('sd').value = '<?php echo $date; ?>';
document.getElementById('ed').value = '<?php echo $date; ?>';
document.getElementById('st').value = '00:00';
document.getElementById('et').value = '23:59';
</script>