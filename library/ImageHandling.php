<?php
class ImageHandling {




	// Resizes the given image and saving it to directory
	// $imagefile = the photo
	// $max_pixels = pixels to resize
	// $saveToDir = location to save the file

	public function resizeImage($imagefile, $max_pixels, $saveToDir) {
		$image = imagecreatefromjpeg($imagefile);
		$max_width = $max_pixels;
		$max_height = $max_pixels;
		$img_width = $this->getWidth($image);
		$img_height = $this->getHeight($image);
		if($img_width > $max_pixels && $img_height > $max_pixels) {
			if($img_width > $img_height) {
				$new_height = $max_height;
				$ratio = $new_height / $img_height;
				$new_width = $img_width * $ratio;
				imagejpeg($image,$saveToDir,100);
			} elseif ($img_height >= $img_width) {
				$new_width = $max_width;
				$ratio = $new_width / $img_width;
				$new_height = $img_height * $ratio;
			}
			$new_image = imagecreatetruecolor($new_width, $new_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $img_width, $img_height);
			$image = $new_image;
			imagejpeg($image,$saveToDir,100);
		} else {
			$new_image = imagecreatetruecolor($img_width, $img_height);
			imagecopyresampled($new_image, $image, 0, 0, 0, 0, $img_width, $img_height, $img_width, $img_height);
			$image = $new_image;
			imagejpeg($image,$saveToDir,100);
		}
	}




	// Get width of an image

	public function getWidth($image) {
		return imagesx($image);
	}




	// Get height of an image

	public function getHeight($image) {
		return imagesy($image);
	}
}
?>