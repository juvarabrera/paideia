<?php
class Group {




	// Get group name

	public function GetGroupName($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		$query = mysql_query("SELECT Name FROM GroupBlock WHERE GroupID = '$groupID'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Name'];
		}
		return $x;
	}




	// Get all join groups by a user

	public function GetAllGroupsOf($loggedUser) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$x = array();
		$query = mysql_query("SELECT GB.GroupID FROM GroupBlock GB LEFT JOIN GroupUser GU ON GB.GroupID = GU.GroupID WHERE GU.UserID = '$loggedUser'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['GroupID'];
		}
		return $x;
	}




	// Get group description

	public function GetGroupDescription($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		$query = mysql_query("SELECT Description FROM GroupBlock WHERE GroupID = '$groupID'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Description'];
		}
		return $x;
	}




	// Get group key 

	public function GetGroupKey($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		$query = mysql_query("SELECT GroupKey FROM GroupBlock WHERE GroupID = '$groupID'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['GroupKey'];
		}
		return $x;
	}




	// Get profile picture ($size = 50, 200, 800, orig)

	public function GetProfilePicture($groupID, $size) {
		$groupID = mysql_real_escape_string($groupID);
		$x = "groups/$groupID/dp/$size.jpg";
		if(!file_exists($x)) {
			$x = "groups/nopic.png";
		}
		return $x;
	}




	// Get group type (Open, Closed)

	public function GetGroupType($groupID) {
		$type = "Closed";
		$query = mysql_query("SELECT * FROM GroupBlock WHERE GroupID = $groupID AND GroupKey IS NULL");
		if(mysql_num_rows($query) >= 1)
			$type = "Open";
		return $type;
	}




	// Get number of members in a group

	public function GetNumberOfMembersIn($group) {
		$group = mysql_real_escape_string($group);
		$x = 0;
		$query = mysql_query("SELECT COUNT(UserID) AS Count FROM GroupUser WHERE GroupID = '$group'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Count'];
		}
		return $x;
	}




	// Get all members in a group

	public function GetAllMembers($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		$x = array();
		$query = mysql_query("SELECT GU.UserID FROM GroupUser GU LEFT JOIN Account A ON GU.UserID = A.ID WHERE GU.GroupID = '$groupID'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['UserID'];
		}
		return $x;
	}




	// Check if user is a member of a group

	public function IsMember($loggedUser, $groupID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$groupID = mysql_real_escape_string($groupID);
		$member = false;
		$query = mysql_query("SELECT * FROM GroupUser WHERE GroupID = '$groupID' AND UserID = '$loggedUser' LIMIT 1");
		if(mysql_num_rows($query) == 1)
			$member = true;
		return $member;
	}




	// Check if group is open or closed group

	public function IsOpenGroup($groupID) {
		$isopen = false;
		$query = mysql_query("SELECT * FROM GroupBlock WHERE GroupID = $groupID AND GroupKey IS NULL");
		if(mysql_num_rows($query) >= 1)
			$isopen = true;
		return $isopen;
	}




	// Check if group exists

	public function CheckIfGroupExist($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		$exists = false;
		$query = mysql_query("SELECT * FROM GroupBlock WHERE GroupID = '$groupID'");
		if(mysql_num_rows($query) >= 1)
			$exists = true;
		return $exists;
	}




	// Get group id from group key

	public function GetGroupIDFromKey($key) {
		$key = mysql_real_escape_string($key);
		$x = "";
		$query = mysql_query("SELECT GroupID FROM GroupBlock WHERE GroupKey = '$key'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['GroupID'];
		}
		return $x;
	}




	// Check if user is join to a group

	public function CheckIfJoined($loggedUser, $groupID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$courseID = mysql_real_escape_string($groupID);
		$joined = false;
		$query = mysql_query("SELECT * FROM GroupUser  WHERE GroupID = '$groupID' AND UserID = '$loggedUser'");
		if(mysql_num_rows($query) >= 1) 
			$joined = true;
		return $enrolled;
	}




	// Join a user to a group

	public function JoinToAGroup($loggedUser, $groupID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$groupID = mysql_real_escape_string($groupID);
		mysql_query("INSERT INTO GroupUser VALUES ('$groupID', '$loggedUser', 'Member')");
	}




	// Get user type of a user in a group (Admin, Member)

	public function GetUserType($loggedUser, $groupID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$groupID = mysql_real_escape_string($groupID);
		$groupType = "";
		$query = mysql_query("SELECT Type FROM GroupUser WHERE GroupID = '$groupID' AND UserID = '$loggedUser'");
		while($row = mysql_fetch_array($query))
			$groupType = $row['Type'];
		return $groupType;
	}




	// Genererates group key (AB1234 format)

	public function GenerateGroupKey() {
		$key = "";
		$key = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999);
		return $key;
	}




	// Check if group exists from group key

	public function CheckIfGroupExistsWithGroupKey($groupKey) {
		$groupKey = mysql_real_escape_string($groupKey);
		$exists = false;
		$query = mysql_query("SELECT GroupID FROM GroupBlock WHERE GroupKey = '$groupKey'");
		if(mysql_num_rows($query) >= 1)
			$exists = true;
		return $exists;
	}




	// Create a group

	public function CreateGroup($loggedUser, $name, $desc, $ctype) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$name = mysql_real_escape_string($name);
		$desc = mysql_real_escape_string($desc);
		$uniqueKey = false;
		while(!$uniqueKey) {
			$generatedKey = $this->GenerateGroupKey();
			$query = mysql_query("SELECT * FROM GroupBlock WHERE GroupKey = '$generatedKey'");
			if(mysql_num_rows($query) == 0) {
				$uniqueKey = true;
			}
		}
		if($ctype == "Open")
			mysql_query("INSERT INTO GroupBlock (Name, Description) VALUES ('$name', '$desc')");
		else
			mysql_query("INSERT INTO GroupBlock (Name, GroupKey, Description) VALUES ('$name', '$generatedKey', '$desc')");
		
		$query = mysql_query("SELECT GroupID FROM GroupBlock ORDER BY GroupID DESC LIMIT 1");
		$groupid = 0;
		while($row = mysql_fetch_array($query)) {
			$groupid = $row['GroupID'];
		}
		if(!is_dir("groups/".$groupid))
			mkdir("groups/".$groupid,0777);
		if(!is_dir("groups/".$groupid."/dp"))
			mkdir("groups/".$groupid."/dp",0777);
		if(!is_dir("groups/".$groupid."/resources"))
			mkdir("groups/".$groupid."/resources",0777);
		copy("groups/index.php", "groups/$groupid/index.php");
		copy("groups/index.php", "groups/$groupid/dp/index.php");
		copy("groups/index.php", "groups/$groupid/resources/index.php");
		mysql_query("INSERT INTO GroupUser VALUES ('$groupid', '$loggedUser', 'Admin')");
		return $groupid;
	}




	// Edit group information

	public function EditGroup($groupid, $name, $desc, $type) {
		$groupid = mysql_real_escape_string($groupid);
		$name = mysql_real_escape_string($name);
		$desc = mysql_real_escape_string($desc);
		if($type == "Open") {
			mysql_query("UPDATE GroupBlock SET Name = '$name', Description = '$desc', GroupKey = NULL WHERE GroupID = '$groupid'");
		} else {
			$groupKey = $this->GenerateGroupKey();
			mysql_query("UPDATE GroupBlock SET Name = '$name', Description = '$desc', GroupKey = '$groupKey' WHERE GroupID = '$groupid'");
		}
	}




	// Delete a group

	public function DeleteGroup($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		mysql_query("DELETE FROM GroupBlock WHERE GroupID = '$groupID'");
		mysql_query("DELETE FROM GroupUser WHERE GroupID = '$groupID'");
		mysql_query("DELETE FROM Calendar WHERE ID = '$groupID' AND FromWhat = 'Group'");
		mysql_query("DELETE FROM Post WHERE ID = '$groupID' AND FromWhat = 'Group'");
		mysql_query("DELETE FROM Notification WHERE ActionID = '$groupID'");
		$path = "groups/".$groupID;
		echo $path;
		if(file_exists($path))
			$this->deleteDir($path);
	}




	// Delete group directory folder in server

	public function deleteDir($dirPath) {
		$dirPath = $dirPath;
		if(!is_dir($dirPath)) {
			throw new InvalidArgumentException("error");
		}
		if(substr($dirPath, strlen($dirPath)-1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach($files as $file)	 {
			if(is_dir($file)) {
				$this->deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}




	// Check if user sent a join request to group

	public function IsRequested($groupID, $userID) {
		$requested = false;
		$query = mysql_query("SELECT * FROM GroupBlockRequest WHERE GroupID = $groupID AND UserID = $userID");
		if(mysql_num_rows($query) >= 1)
			$requested = true;
		return $requested;
	}




	// Get all request

	public function GetRequest($groupID) {
		$groupID = mysql_real_escape_string($groupID);
		$x = array();
		$query = mysql_query("SELECT UserID FROM GroupBlockRequest WHERE GroupID = '$groupID'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['UserID'];
		}
		return $x;
	}




	// Get all request to approve

	public function GetRequestToApprove($groupID, $loggedUser, $library, $skin) {
		$query = mysql_query("SELECT UserID FROM GroupBlockRequest WHERE GroupID = '$groupID'");
		while($row = mysql_fetch_array($query)) {
			$userID = $row['UserID'];
			$username = $library['user']->GetUsername($userID);
			$dp = $library['user']->GetProfilePicture($userID,50);
			$name = $library['user']->GetName($userID);
			$type = $library['user']->GetType($userID);
			$hash = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2) . rand(1000,9999) . $userID;
			if(strlen($name) > 20)
				$name = substr($name, 0, 20).'...';
?>
				<script>
				$(document).ready(function () {
					$('#btnAcceptRequest_<?php echo $hash;?>').click(function () {
						$('#lstRequest_<?php echo $hash;?>').css({
							"display": "none"
						});
						$('#msgRequest_<?php echo $hash;?>').text('has been added to this group.');
						$('#lstRequest_<?php echo $hash;?>_click').css({
							"display": "block"
						});
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=joinpeople",
							data: {id: <?php echo $groupID; ?>, fromwhat: 'Group', userid: <?php echo $userID; ?>},
							success: function(html) {
								setTimeout(function() {
									$refreshlistRequestGroups= 1;
								}, 2000);
							}
						});
					});
					$('#btnRejectRequest_<?php echo $hash;?>').click(function () {
						$('#lstRequest_<?php echo $hash;?>').css({
							"display": "none"
						});
						$('#msgRequest_<?php echo $hash;?>').text('has been rejected to this group.');
						$('#lstRequest_<?php echo $hash;?>_click').css({
							"display": "block"
						});
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=deletejoinrequest",
							data: {id: <?php echo $groupID; ?>, fromwhat: 'Group', userid: <?php echo $userID; ?>},
							success: function(html) {
								setTimeout(function() {
									$refreshlistRequestGroups= 1;
								}, 2000);
							}
						});
					});
				});
				</script>
				<li id="lstRequest_<?php echo $hash;?>"><table class="results"><tr valign="top">
					<td><a href="index.php?-_-=<?php echo $username; ?>" title="<?php echo $library['user']->GetName($userID); ?>"><div style="background-image: url(<?php echo $dp; ?>)" class="profilepicture"></div></a></td>
					<td><a href="index.php?-_-=<?php echo $username; ?>" title="<?php echo $library['user']->GetName($userID); ?>"><?php echo $name; ?></a><br><small><?php echo $type; ?></small></td>
					<td class="button"><a id="btnAcceptRequest_<?php echo $hash;?>" class="inputbutton">Accept</a></td>
					<td class="button"><a id="btnRejectRequest_<?php echo $hash;?>" class="inputbutton">Reject</a></td>
				</tr></table></li>
				<li id="lstRequest_<?php echo $hash;?>_click" style="display: none;"><table class="results"><tr valign="top">
					<td><a href="index.php?-_-=<?php echo $username; ?>" title="<?php echo $library['user']->GetName($userID); ?>"><div style="background-image: url(<?php echo $dp; ?>)" class="profilepicture"></div></a></td>
					<td><a href="index.php?-_-=<?php echo $username; ?>" title="<?php echo $library['user']->GetName($userID); ?>"><?php echo $library['user']->GetName($userID); ?></a> <span id="msgRequest_<?php echo $hash;?>"></span></td>
				</tr></table></li>
<?php
		}
		if(mysql_num_rows($query) == 0) {
			echo '<center><p class="text">There are no people who requested to join this group.</p></center>';
		}
	}




	// Checks if group members posted this post

	public function CheckIfGroupPost($groupID, $postID) {
		$query = mysql_query("SELECT * FROM Post WHERE FromWhat = 'Group' AND ID = $groupID AND PostID = $postID");
		$userpost = false;
		if(mysql_num_rows($query) >= 1)
			$userpost = true;
		return $userpost;
	}




	// Force enroll people to course

	public function AcceptRequest($groupID, $userID) {
		mysql_query("INSERT INTO GroupUser (GroupID, UserID, Type) VALUES ($groupID, $userID, 'Member')");
		mysql_query("DELETE FROM GroupBlockRequest WHERE GroupID = $groupID AND UserID = $userID");
	}




	// Reject request

	public function RejectRequest($groupID, $userID) {
		mysql_query("DELETE FROM GroupBlockRequest WHERE GroupID = $groupID AND UserID = $userID");
	}




	// Gets people on group

	public function GetPeopleOnGroup($groupID, $loggedUser, $library, $skin) {
?>
				<ul class="contentlist">
					<?php
					$row = $library['group']->GetAllMembers($groupID);
					for($i = 0; $i < sizeof($row); $i++) {
						$dp = $library['user']->GetProfilePicture($row[$i],50);
						echo '<a id="btnPopup_UserOptions_'.$library['user']->GetUsername($row[$i]).'"><div class="profpic" style="background-image: url('.$dp.');"></div>'.$library['user']->GetName($row[$i]).'<br><small>'.$library['user']->GetUsername($row[$i]).'</small></a>'; ?>
						<script>
						$(document).ready(function() {
							$('#btnPopup_UserOptions_<?php echo $library['user']->GetUsername($row[$i]); ?>').click(function() {
								showPopup();
								$showPopup = "UserOptions";
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=showpopup",
									data: {popup: 'group_'+$showPopup, userID: '<?php echo $row[$i]; ?>'},
									success: function(html) {
										$('#Popup').html(html);
										$heightPopup = $('div#popup_'+$showPopup).height()+5;
										$('#Popup').css({
											"width": "450px",
											"height": ($heightPopup)+"px",
											"margin-left": "-225px",
											"margin-top": "calc(-" + (($heightPopup + 60)/2)+"px)"
										});
										popup = 1;
									}
								});
							});
						});
						</script>
	<?php
					}
					if(sizeof($row) == 0) 
						echo '<a>There are no members in this group.</a>';
					?>
				</ul>
<?php
	}




	// Gets people to invite on group

	public function GetPeopleToInvite($searchQuery, $groupID, $loggedUser, $library, $skin) {
		$query = mysql_query("SELECT * FROM Account WHERE ID NOT IN (SELECT UserID FROM GroupUser WHERE GroupID = $groupID) AND (FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR CONCAT(FirstName, ' ', LastName) LIKE '%$searchQuery%') LIMIT 5");
		while($row = mysql_fetch_array($query)) {
			$userid = $row['ID'];
			$hash = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2) . rand(1000,9999) . $userid;
			$fullname = $row['FirstName']. ' ' . $row['LastName'];
			$title = $fullname;
			if(strlen($fullname) > 20)
				$fullname = substr($fullname, 0, 20).'...';
			$type = $row['Type'];
			$dp = $library['user']->GetProfilePicture($userid, 50);
		?>
		<script>
		$(document).ready(function () {
			$('#InvitationResult_<?php echo $hash; ?>').click(function() {
				$text = $('#InvitationResult_<?php echo $hash; ?>').text();
				if($text != "<?php echo $title; ?> is now added to this group.") {
					$('#InvitationResult_<?php echo $hash; ?>').css({
						"display": "none"
					});
					$('#InvitationResult_<?php echo $hash; ?>_success').css({
						"display": "block"
					});
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=joinpeople",
						data: {id: <?php echo $groupID; ?>, fromwhat: 'Group', userid: <?php echo $userid ?>, request: 1},
						success: function() {
							$refreshlistPeopleGroups = 1;
							$refreshlistRequestGroups = 1;
						}
					});
				}
			});
		});
		</script>
		<a id="InvitationResult_<?php echo $hash; ?>" class="hoverit" title="<?php echo $title; ?>"><div class="profpic" style="background-image: url(<?php echo $dp; ?>);"></div><?php echo $fullname; ?><br><small><?php echo $type; ?></small></a>
		<a id="InvitationResult_<?php echo $hash; ?>_success" class="hoverit" style="display: none;" title="<?php echo $title; ?> is now added to this group."><div class="profpic" style="float: left; width: 40px; height: 40px; background-image: url(<?php echo $dp; ?>);"></div><small><?php echo $title; ?> is now added to this group.</small></a>
		<?php
		}
		if(mysql_num_rows($query) == 0) {
			echo '<a><center><small><i>No user found.</i></small></center></a>';
		}
	}
}
?>