<?php
class Newsfeed {




	// Gets time interval of post or comment from date and time posted to today.

	// For PHP ver 5.2 and above

	/*public function GetDateTimeInterval($date, $time) {
		date_default_timezone_set('Asia/Manila');
		$timeSpan = date('F j, Y', strtotime($date)).' - '.date('g:i a', strtotime($time));
		$dateToday = date('Y-m-d');
		$timeToday = date('H:i:s');
		$date1 = new DateTime($date);
		$date2 = new DateTime($dateToday);
		$dateInterval = $date1->diff($date2);
		$time1 = new DateTime($time);
		$time2 = new DateTime($timeToday);
		$timeInterval = $time1->diff($time2);
		if($dateInterval->d == 0) {
			if($timeInterval->h == 1) {
				$timeSpan = "1 hour ago";
			} elseif ($timeInterval->h > 1) {
				$timeSpan = $timeInterval->h.' hours ago';
			} else {
				if($timeInterval->i == 1) {
					$timeSpan = "A minute ago";
				} elseif ($timeInterval->i > 1) {
					$timeSpan = $timeInterval->i." minutes ago";
				} elseif ($timeInterval->s >= 10 && $timeInterval->s <= 59) {
					$timeSpan = "Less than a minute ago";
				} else {
					$timeSpan = "Just now";
				}
			}
		} elseif ($dateInterval->d == 1){
			$timeSpan = "Yesterday at ".date('g:i a', strtotime($time));
		} elseif ($dateInterval->d > 1 && $dateInterval->d < 7) {
			$timeSpan = $dateInterval->d." days ago at ".date('g:i a', strtotime($time));
		} elseif ($dateInterval->d == 7) {
			$timeSpan = "About a week ago (week ago) at ".date('g:i a', strtotime($time));
		}
		return $timeSpan;
	}*/




	// Gets time interval of post or comment from date and time posted to today.

	// For PHP ver 5.2 and below

	public function GetDateTimeInterval($date, $time) {
		date_default_timezone_set('Asia/Manila');
		$timeSpan = date('F j, Y', strtotime($date)).' - '.date('g:i a', strtotime($time));
		$dateToday = date('Y-m-d');
		$timeToday = date('H:i:s');
		$date1 = new DateTime($date);
		$date2 = new DateTime($dateToday);
		$dateInterval_d = round(($date2->format('U') - $date1->format('U')) / (60*60*24));
		$time1 = strtotime($time);
		$time2 = strtotime($timeToday);
		$timeInterval_h = ($time2 - $time1) / (60*60);
		$timeInterval_i = ($time2 - $time1) / (60);
		$timeInterval_s = ($time2 - $time1);
		if(round($dateInterval_d) == 0) {
			if(round($timeInterval_h) == 1) {
				$timeSpan = "1 hour ago";
			} elseif (round($timeInterval_h) > 1) {
				$timeSpan = round($timeInterval_h).' hours ago';
			} else {
				if(round($timeInterval_i) == 1) {
					$timeSpan = "A minute ago";
				} elseif (round($timeInterval_i) > 1) {
					$timeSpan = round($timeInterval_i)." minutes ago";
				} elseif (round($timeInterval_s) >= 31 && round($timeInterval_s) <= 59) {
					$timeSpan = "Less than a minute ago";
				} elseif (round($timeInterval_s) >= 10 && round($timeInterval_s) <= 30) {
					$timeSpan = "A few seconds ago";
				} else {
					$timeSpan = "Just now";
				}
			}
		} elseif (round($dateInterval_d) == 1){
			$timeSpan = "Yesterday at ".date('g:i a', strtotime($time));
		} elseif (round($dateInterval_d) > 1 && round($dateInterval_d) < 7) {
			$timeSpan = round($dateInterval_d)." days ago at ".date('g:i a', strtotime($time));
		} elseif (round($dateInterval_d) == 7) {
			$timeSpan = "About a week ago (week ago) at ".date('g:i a', strtotime($time));
		}
		return $timeSpan;
	}




	// Generates hash for post to execute commands specifically on the post

	public function GeneratePostHash($postid) {
		return substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2) . rand(1000,9999) . $postid;
	}




	// Gets number post likes 

	public function GetPostLikes($postid, $loggedUser, $skin) {
		$query1 = mysql_query("SELECT * FROM PostLikes WHERE PostID = $postid");
?>
		<script>
		$(document).ready(function() {
			$('#btnPopup_DisplayPostLikes_<?php echo $postid; ?>').click(function() {
				showPopup();
				$showPopup = "DisplayPostLikes";
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=showpopup",
					data: {popup: 'newsfeed_'+$showPopup, postID: <?php echo $postid; ?>},
					success: function(html) {
						$('#Popup').html(html);
						$heightPopup = $('div#popup_'+$showPopup).height()+5;
						$('#Popup').css({
							"width": "450px",
							"height": ($heightPopup)+"px",
							"margin-left": "-225px",
							"margin-top": "-" + (($heightPopup + 60)/2)+"px"
						});
						popup = 1;
					}
				});
			});
		});
		</script>
<?php
		if(mysql_num_rows($query1) >= 2) {
			$query4 = mysql_query("SELECT * FROM PostLikes WHERE PostID = $postid AND UserID = $loggedUser");
			if(mysql_num_rows($query4) == 1) {
				echo '<span class="likes"><img src="images/skin/'.$skin.'/bg/like_comment.png">You and <a id="btnPopup_DisplayPostLikes_'.$postid.'">'.(mysql_num_rows($query1)-1).' people</a> likes this</span>';
			} else {
				echo '<span class="likes"><img src="images/skin/'.$skin.'/bg/like_comment.png"><a id="btnPopup_DisplayPostLikes_'.$postid.'">'.mysql_num_rows($query1).' people</a> likes this</span>';
			}
		} else if (mysql_num_rows($query1) == 1) {
			$query4 = mysql_query("SELECT * FROM PostLikes WHERE PostID = $postid AND UserID = $loggedUser");
			if(mysql_num_rows($query4) == 1) {
				echo '<span class="likes"><img src="images/skin/'.$skin.'/bg/like_comment.png">You like this</span>';
			} else {
				echo '<span class="likes"><img src="images/skin/'.$skin.'/bg/like_comment.png"><a id="btnPopup_DisplayPostLikes_'.$postid.'">1 people</a> likes this</span>';
			}
		}
	}




	// Gets number post likes 

	public function GetNumPostLikes($postid) {
		$query1 = mysql_query("SELECT * FROM PostLikes WHERE PostID = $postid");
		return mysql_num_rows($query1);
	}




	// Gets number comments likes 

	public function GetUserComment($commentID) {
		$query = mysql_query("SELECT UserID FROM Post WHERE PostID = $commentID");
		$row = mysql_fetch_array($query);
		return $row['UserID'];
	}




	// Gets number comments likes 

	public function GetCommentLikes($postid, $postHashID, $postLink, $commentid, $commentHashID, $type, $loggedUser, $skin) {
?>
			<script>
			$(document).ready(function() {
				$('#btnLikeFor_<?php echo $commentHashID; ?>').click(function() {
					if($('#btnLikeFor_<?php echo $commentHashID; ?>').attr('isLike') == 1) {
						$('#btnLikeFor_<?php echo $commentHashID; ?>').text('Like');
						$('#btnLikeFor_<?php echo $commentHashID; ?>').attr('isLike', '0');
						$('#btnLikeFor_<?php echo $commentHashID; ?>').attr('title', 'Like this comment');
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=unlikeacomment",
							data: {type: '<?php echo $type; ?>', posthashid: '<?php echo $postHashID; ?>', postid: <?php echo $postid; ?>, postLink: '<?php echo $postLink; ?>', commenthashid: '<?php echo $commentHashID; ?>', commentid: <?php echo $commentid; ?>},
							success: function(html) {
								$('#displayLikeInfoFor_<?php echo $commentHashID; ?>').html(html);
							}
						});
					} else {
						$('#btnLikeFor_<?php echo $commentHashID; ?>').text('Unlike');
						$('#btnLikeFor_<?php echo $commentHashID; ?>').attr('isLike', '1');
						$('#btnLikeFor_<?php echo $commentHashID; ?>').attr('title', 'Unlike this comment');
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=likeacomment",
							data: {type: '<?php echo $type; ?>', posthashid: '<?php echo $postHashID; ?>', postid: <?php echo $postid; ?>, postLink: '<?php echo $postLink; ?>', commenthashid: '<?php echo $commentHashID; ?>', commentid: <?php echo $commentid; ?>},
							success: function(html) {
								$('#displayLikeInfoFor_<?php echo $commentHashID; ?>').html(html);
							}
						});
					}
				});
				$('#btnDeleteCommentFor_<?php echo $commentHashID; ?>').click(function () {
					var e = confirm("Are you sure you want to delete this comment?");
					if(e) {
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=deletecomment",
							data: {commentid: '<?php echo $commentid; ?>'},
							success: function(data) {
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=refreshcomments",
									data: {type: '<?php echo $type; ?>', postID: '<?php echo $postid; ?>', postHash: '<?php echo $postHashID; ?>', postLink: '<?php echo $postLink; ?>'},
									success: function(html){
										$("table#tblCommentsFor_<?php echo $postHashID; ?>").html(html);
									}
								});
							}
						});
						$('#commentHashID_<?php echo $commentHashID; ?>').html('<td></td><td>This comment has been deleted.</td>');
					}
				});
				$('#btnPopup_DisplayPostLikes_<?php echo $commentid; ?>').click(function() {
					showPopup();
					$showPopup = "DisplayPostLikes";
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=showpopup",
						data: {popup: 'newsfeed_'+$showPopup, postID: <?php echo $commentid; ?>},
						success: function(html) {
							$('#Popup').html(html);
							$heightPopup = $('div#popup_'+$showPopup).height()+5;
							$('#Popup').css({
								"width": "450px",
								"height": ($heightPopup)+"px",
								"margin-left": "-225px",
								"margin-top": "-" + (($heightPopup + 60)/2)+"px"
							});
							popup = 1;
						}
					});
				});
			});
			</script>
<?php
		$query3 = mysql_query("SELECT * FROM PostLikes WHERE PostID = $commentid");
		if(mysql_num_rows($query3) != 0) {
			$query4 = mysql_query("SELECT * FROM PostLikes WHERE PostID = $commentid AND UserID = $loggedUser");
			if(mysql_num_rows($query4) == 1)
				echo '<a id="btnLikeFor_'.$commentHashID.'" isLike="1" title="Unlike this comment">Unlike</a>&nbsp;&nbsp;·&nbsp;&nbsp;<a id="btnPopup_DisplayPostLikes_'.$commentid.'">'.mysql_num_rows($query3).'<img src="images/skin/'.$skin.'/bg/like_comment.png"></a>';
			else
				echo '<a id="btnLikeFor_'.$commentHashID.'" isLike="0" title="Like this comment">Like</a>&nbsp;&nbsp;·&nbsp;&nbsp;<a id="btnPopup_DisplayPostLikes_'.$commentid.'">'.mysql_num_rows($query3).'<img src="images/skin/'.$skin.'/bg/like_comment.png"></a>';
		} else {
			echo '<a id="btnLikeFor_'.$commentHashID.'" isLike="0" title="Like this comment">Like</a>';
		}
		if($this->GetUserComment($commentid) == $loggedUser) {
			echo '<a id="btnDeleteCommentFor_'.$commentHashID.'" class="deletecomment"></a>';
		}
	}




	// Get all comments to posts

	public function GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library) {
		if($type == "All") {
			$query = mysql_query("SELECT * FROM Post WHERE ReferTo = $postid ORDER BY PostID");
			while($row2 = mysql_fetch_array($query)) {
				$commentid = $row2['PostID'];
				$commentuserid = $row2['UserID'];
				$commentmessage = nl2br(htmlentities(trim($row2['Message'])));;
				$commentdate = $row2['DatePosted'];
				$commenttime = $row2['TimePosted'];
				$datetimeInterval = $this->GetDateTimeInterval($commentdate, $commenttime);
				$commentHashID = $this->GeneratePostHash($commentid);
				$commentLink = "$postLink#$commentid";
	?>
				<script>
				$(document).ready(function () {
					$('#commentHashID_<?php echo $commentHashID; ?>').mouseover(function() {
						$('#btnDeleteCommentFor_<?php echo $commentHashID; ?>').css({
							"opacity": ".9"
						});
					})
					.mouseout(function() {
						$('#btnDeleteCommentFor_<?php echo $commentHashID; ?>').css({
							"opacity": "0"
						});
					});
				});
				</script>
				<tr valign="top" id="commentHashID_<?php echo $commentHashID; ?>">
					<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($commentuserid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($commentuserid, 50); ?>)" class="profilepicture"></div></a></td>
					<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($commentuserid); ?>"><?php echo $library['user']->GetName($commentuserid); ?></a> <?php echo $commentmessage; ?><br><span class="datec"><a href="<?php echo $commentLink; ?>" style="color: gray;font-weight: 100;"><?php echo $datetimeInterval; ?></a>&nbsp;&nbsp;·&nbsp;&nbsp;
					<span id="displayLikeInfoFor_<?php echo $commentHashID; ?>">
					<?php
					$this->GetCommentLikes($postid, $postHashID, $postLink, $commentid, $commentHashID, $type, $loggedUser, $skin);
					?>
					</span>
					</span></td>
				</tr>
	<?php
			}
		} elseif($type == "Limited") {
			$query = mysql_query("SELECT * FROM Post WHERE ReferTo = $postid ORDER BY PostID");
			$ids = array();
			while($row = mysql_fetch_array($query)) 
				$ids[] = $row['PostID'];
			$maxCommentsToShow = 5;
			$start = 0;
			$end = sizeof($ids);
			if(sizeof($ids) > $maxCommentsToShow)
				$start = sizeof($ids) - $maxCommentsToShow;
			for($i = $start; $i < $end; $i++) {
				$commentid = $ids[$i];
				$query2 = mysql_query("SELECT * FROM Post WHERE PostID = $commentid");
				$row2 = mysql_fetch_array($query2);
				$commentid = $row2['PostID'];
				$commentuserid = $row2['UserID'];
				$commentmessage = nl2br(htmlentities(trim($row2['Message'])));;
				$commentdate = $row2['DatePosted'];
				$commenttime = $row2['TimePosted'];
				$datetimeInterval = $this->GetDateTimeInterval($commentdate, $commenttime);
				$commentHashID = $this->GeneratePostHash($commentid);
				$commentLink = "$postLink#$commentid";
				if($i == $start && sizeof($ids) > $maxCommentsToShow) {
	?>
				<tr>
					<td colspan="2" style="padding: 0px;"><a href="<?php echo $postLink; ?>" class="viewallcomments">View all comments...</a></td>
				</tr>
	<?php
				}
	?>
				<script>
				$(document).ready(function () {
					$('#commentHashID_<?php echo $commentHashID; ?>').mouseover(function() {
						$('#btnDeleteCommentFor_<?php echo $commentHashID; ?>').css({
							"opacity": ".9"
						});
					})
					.mouseout(function() {
						$('#btnDeleteCommentFor_<?php echo $commentHashID; ?>').css({
							"opacity": "0"
						});
					});
				});
				</script>
				<tr valign="top" id="commentHashID_<?php echo $commentHashID; ?>">
					<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($commentuserid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($commentuserid, 50); ?>)" class="profilepicture"></div></a></td>
					<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($commentuserid); ?>"><?php echo $library['user']->GetName($commentuserid); ?></a> <?php echo $commentmessage; ?><br><span class="datec"><a href="<?php echo $commentLink; ?>" style="color: gray;font-weight: 100;"><?php echo $datetimeInterval; ?></a>&nbsp;&nbsp;·&nbsp;&nbsp;
					<span id="displayLikeInfoFor_<?php echo $commentHashID; ?>">
					<?php
					$this->GetCommentLikes($postid, $postHashID, $postLink, $commentid, $commentHashID, $type, $loggedUser, $skin);
					?>
					</span>
					</span></td>
				</tr>
	<?php
			}
		}
	}




	// Echo comment text box for user to enter comment

	public function DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library) {
?>
			<tr valign="top">
				<td><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($loggedUser, 50); ?>)" class="profilepicture"></div></td>
				<td><textarea id="<?php echo $postHashID; ?>" placeholder="Write a comment..."></textarea></td>
			</tr>
			<script>
			$('textarea#<?php echo $postHashID; ?>').keyup(function(event) {
				if (event.keyCode == 13 && event.shiftKey) {
					var content = this.value;
					var caret = getCaret(this);
					this.value = content.substring(0,caret)+"\n"+content.substring(caret,content.length);
					event.stopPropagation();
				} else if (event.keyCode == 13) {
					$text = $('#<?php echo $postHashID; ?>').val().substring(0,$('#<?php echo $postHashID; ?>').val().length-1);
					$text = $.trim($text);
					if($text != "") {
						$('textarea#<?php echo $postHashID; ?>').val('');
						$("table#tblCommentsFor_<?php echo $postHashID; ?>").append('<tr valign="top"><td><a><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($loggedUser, 50); ?>)" class="profilepicture"></div></a></td><td><a><?php echo $library['user']->GetName($loggedUser); ?></a> '+ $text +'<br><span class="datec"><a style="color: gray;font-weight: 100;">Just now</a>&nbsp;&nbsp;·&nbsp;&nbsp;<span">Posting..</span></span></td></tr>');
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=savecomment",
							data: {postMsg: $text, postID: '<?php echo $postid; ?>', postHash: '<?php echo $postHashID; ?>'},
							success: function(data) {
								$.ajax({
									type: "POST",
									cache: false,
									url: "process.php?action=refreshcomments",
									data: {type: '<?php echo $type; ?>', postID: '<?php echo $postid; ?>', postHash: '<?php echo $postHashID; ?>', postLink: '<?php echo $postLink; ?>'},
									success: function(html){
										$("table#tblCommentsFor_<?php echo $postHashID; ?>").html(html);
										$('textarea#<?php echo $postHashID; ?>').focus();
									}
								});
							}
						});
					} else {
						$('#<?php echo $postHashID; ?>').val($.trim($text));
					}
				}
			});
			</script>
<?php
	}




	// Gets other users newsfeed

	public function isLike($postID, $loggedUser) {
		$query = mysql_query("SELECT * FROM PostLikes WHERE PostID = $postID AND UserID = $loggedUser"); 
		$x = 0;
		if(mysql_num_rows($query) >= 1) {
			$x = 1;
		}
		return $x;
	}




	// jQuery script for Post

	public function GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, $reloadDelete) {
?>
		<script>
		$(document).ready(function () {
			setInterval(function() {
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=refreshdatetimeinterval",
					data: {date: '<?php echo $date; ?>', time: '<?php echo $time; ?>', postLink: '<?php echo $postLink; ?>'},
					success: function(html) {
						$('#datetimeInterval_<?php echo $postHashID; ?>').html(html);
					}
				});
				$.ajax({
					type: "POST",
					cache: false,
					url: "process.php?action=refreshpostlikes",
					data: {postid: '<?php echo $postid; ?>'},
					success: function(html) {
						$('#postLikesFor_<?php echo $postHashID; ?>').html(html);
					}
				});
			}, 5000);
			$('#btnLikeFor_<?php echo $postHashID; ?>').attr('isLike', '<?php echo $this->isLike($postid, $loggedUser); ?>');
			if($('#btnLikeFor_<?php echo $postHashID; ?>').attr('isLike') == 1) {
				$('#btnLikeFor_<?php echo $postHashID; ?>').css({
					"opacity": "1"
				});
				$('#btnLikeFor_<?php echo $postHashID; ?>').attr('title', 'Unlike this post');
			}
			$('#btnLikeFor_<?php echo $postHashID; ?>').click(function () {
				if($('#btnLikeFor_<?php echo $postHashID; ?>').attr('isLike') == 0) {
					$('#btnLikeFor_<?php echo $postHashID; ?>').attr('isLike', '1');
					$('#btnLikeFor_<?php echo $postHashID; ?>').attr('title', 'Unlike this post');
					$('#btnLikeFor_<?php echo $postHashID; ?>').css({
						"opacity": "1"
					});
					$('#btnLikeFor_<?php echo $postHashID; ?>').hover(function () {
						$('#btnLikeFor_<?php echo $postHashID; ?>').css({
							"opacity": "1"
						});
					}, function () {
						$('#btnLikeFor_<?php echo $postHashID; ?>').css({
							"opacity": "1"
						});
					});
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=likeapost",
						data: {postid: <?php echo $postid; ?>},
						success: function(html) {
							$('#postLikesFor_<?php echo $postHashID; ?>').html(html);
						}
					});
				} else {
					$('#btnLikeFor_<?php echo $postHashID; ?>').attr('isLike', '0');
					$('#btnLikeFor_<?php echo $postHashID; ?>').attr('title', 'Like this post');
					$('#btnLikeFor_<?php echo $postHashID; ?>').css({
						"opacity": ".5"
					});
					$('#btnLikeFor_<?php echo $postHashID; ?>').hover(function () {
						$('#btnLikeFor_<?php echo $postHashID; ?>').css({
							"opacity": "1"
						});
					}, function () {
						$('#btnLikeFor_<?php echo $postHashID; ?>').css({
							"opacity": ".5"
						});
					});
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=unlikeapost",
						data: {postid: <?php echo $postid; ?>},
						success: function(html) {
							$('#postLikesFor_<?php echo $postHashID; ?>').html(html);
						}
					});
				}
			});
			$('html').click(function() {
				$('.morepostddl').css({
					"display": "none"
				})
			});
			$('#morePostIco_<?php echo $postHashID; ?>').click(function() {
				if($('#morePostIco_<?php echo $postHashID; ?>').attr('canClick') == 1) {
					event.stopPropagation();
					$('.morepostddl').css({
						"display": "none"
					})
					$('#morePost_<?php echo $postHashID; ?>').css({
						"display": "block"
					});
				}
			});
			<?php
			if($userid == $loggedUser) {
			?>
			$('#btnCancelEdit_<?php echo $postHashID; ?>').click(function() {
				$('#morePostIco_<?php echo $postHashID; ?>').attr('canClick', '1');
				$('#showPost_<?php echo $postHashID; ?>').css({"display": "block"});
				$('#editPost_<?php echo $postHashID; ?>').css({"display": "none"});
			});
			$('#btnEditFor_<?php echo $postHashID; ?>').click(function() {
				$('#morePostIco_<?php echo $postHashID; ?>').attr('canClick', '0');
				$('#showPost_<?php echo $postHashID; ?>').css({"display": "none"});
				$('#editPost_<?php echo $postHashID; ?>').css({"display": "block"});
			});
			$('#saveEditedPost_<?php echo $postHashID; ?>').click(function() {
				$text = $('#textareaPost_<?php echo $postHashID; ?>').val();
				if($text != "") {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=editpost",
						data: {postid: <?php echo $postid; ?>, postMsg: $text},
						success: function(html) {
							$('#morePostIco_<?php echo $postHashID; ?>').attr('canClick', '1');
							$('#showPost_<?php echo $postHashID; ?>').css({"display": "block"});
							$('#editPost_<?php echo $postHashID; ?>').css({"display": "none"});
							$('#showPost_<?php echo $postHashID; ?>').html(html);
						}
					});
				}
			});
			<?php
			}
			?>
			$('#btnDeleteFor_<?php echo $postHashID; ?>').click(function() {
				var e = confirm("Are you sure you want to delete this post?");
				if(e) {
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=deletepost",
						data: {postid: <?php echo $postid; ?>},
						success: function(html) {
							$postSuccess = 1;
						}
					});
					$secondsToHide = 3;
					$('#containerPost_<?php echo $postHashID; ?>').html('<p class="text">This post has been deleted.</p>');
					setTimeout(function() {
						$('#containerPost_<?php echo $postHashID; ?>').css({
							"opacity": "0",
							"height": "0px",
							"padding": "0px",
							"margin": "0px"
						});
					}, $secondsToHide * 1000);
					setTimeout(function() {
						$('#containerPost_<?php echo $postHashID; ?>').css({
							"display": "none"
						});
						<?php
						if($reloadDelete) {
						?>
						location.reload();
						<?php
						}
						?>
					}, ($secondsToHide * 1000) + 200);
				}
			});
		});
		</script>
<?php
	}




	// Gets other users newsfeed

	public function GetOtherUserNewsfeed($viewProfileOf, $loggedUser, $skin, $library) {
		$type = "Limited";
		$select_Personal_Post = "SELECT * FROM Post WHERE UserID = $viewProfileOf AND ReferTo IS NULL AND ID IS NULL AND FromWhat IS NULL";
		$select_OtherUser_Post = "SELECT * FROM Post WHERE ReferTo IS NULL AND ID = $viewProfileOf AND FromWhat = 'Newsfeed'";
		$query = mysql_query("$select_Personal_Post UNION $select_OtherUser_Post ORDER BY PostID DESC");
		while($row = mysql_fetch_array($query)) {
			$postid = $row['PostID'];
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message = nl2br(htmlentities(trim($row['Message'])));
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$uid = $row['ID'];
			$postTo = "";

			if($viewProfileOf == $loggedUser) {
				if($userid != $loggedUser) {
					$postTo = '<b class="font-weight_normal"> posted to you</b>';
					$postLink = "index.php?-_-=".$library['user']->GetUsername($uid)."&post=$postid";
				} else {
					$postLink = "index.php?-_-=".$library['user']->GetUsername($viewProfileOf)."&post=$postid";
				}
			} else {
				if($userid != $loggedUser) {
					$postLink = "index.php?-_-=".$library['user']->GetUsername($viewProfileOf)."&post=$postid";
				} else {
					$postTo = '<b class="font-weight_normal"> posted to</b><a href="index.php?-_-='.$library['user']->GetUsername($uid).'"><div class="profpic2" style="background-image: url('.$library['user']->GetProfilePicture($uid, 50).');"></div>'.$library['user']->GetName($uid).'</a>';
					$postLink = "index.php?-_-=".$library['user']->GetUsername($uid)."&post=$postid";
				}
			}
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, false);
?>
		<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
			<table class="title">
				<tr>
					<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
					<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a><?php echo $postTo; ?></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
					<td width="1" align="center"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
					<?php
					if($userid == $loggedUser) {
					?>
					<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
						<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
							<h4>Post Menu</h4>
							<?php
							if($userid == $loggedUser) {
							?>
							<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
							<?php
							}
							?>
							<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
						</ul>
					</span></td>
					<?php
					}
					?>
				</tr>
				<?php
				if($userid == $loggedUser) {
				?>
				<tr>
					<td colspan="4" class="feedmsg">
					<div id="showPost_<?php echo $postHashID; ?>">
					<?php echo nl2br(htmlentities(trim($message))); ?>
					</div>
					<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
					<table class="nopad">
						<tr>
							<td><h4>Edit Post</h4></td>
							<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
						</tr>
					</table>
					<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
					<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
					</div>
					</td>
				</tr>
				<?php 
				} else {
				?>
				<tr>
					<td colspan="3" class="feedmsg">
					<?php echo nl2br(htmlentities(trim($message))); ?>
					</td>
				</tr>
				<?php
				}
				?>
			</table>
			<span id="postLikesFor_<?php echo $postHashID; ?>">
			<?php
			$this->GetPostLikes($postid, $loggedUser, $skin);
			?>
			</span>
			<hr>
			<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
			<?php
			$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
			?>
			</table>
			<table class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library);
				?>
			</table>
		</div>
<?php
		}
	}




	// Gets logged user newsfeed

	public function GetUserNewsfeed($loggedUser, $skin, $library) {
		$type = "Limited";
		$select_Personal_Post = "SELECT * FROM Post WHERE UserID = $loggedUser AND ReferTo IS NULL AND ID IS NULL AND FromWhat IS NULL";
		$select_OtherUser_Post = "SELECT * FROM Post WHERE ReferTo IS NULL AND ID = $loggedUser AND FromWhat = 'Newsfeed'";
		$select_UserToOther_Post = "SELECT * FROM Post WHERE ReferTo IS NULL AND UserID = $loggedUser AND FromWhat = 'Newsfeed' AND ReferTo IS NULL";
		$select_EnrolledCourse_Post = "SELECT * FROM Post WHERE ID IN (SELECT CourseID FROM CourseUser WHERE UserID = $loggedUser) AND FromWhat = 'Course' AND ReferTo IS NULL";
		$select_JoinedGroup_Post = "SELECT * FROM Post WHERE ID IN (SELECT GroupID FROM GroupUser WHERE UserID = $loggedUser) AND FromWhat = 'Group' AND ReferTo IS NULL";
		$query = mysql_query("
				$select_Personal_Post UNION 
				$select_OtherUser_Post UNION 
				$select_EnrolledCourse_Post UNION 
				$select_JoinedGroup_Post UNION 
				$select_UserToOther_Post ORDER BY PostID DESC
		");
		while($row = mysql_fetch_array($query)) {
			$postid = $row['PostID'];
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message =  $row['Message'];
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$postTo = "";
			if($row['FromWhat'] == "Course") {
				$courseID = $row['ID'];
				$postTo = '<b class="font-weight_normal"> posted to course</b><a href="courses.php?id='.$courseID.'"><div class="profpic2" style="background-image: url('.$library['course']->GetProfilePicture($courseID, 50).');"></div>'.$library['course']->GetCourseName($courseID).'</a>';
				$postLink = "courses.php?id=$courseID&post=$postid";
			} elseif($row['FromWhat'] == "Group") {
				$groupID = $row['ID'];
				$postTo = '<b class="font-weight_normal"> posted to group</b><a href="groups.php?id='.$groupID.'"><div class="profpic2" style="background-image: url('.$library['group']->GetProfilePicture($groupID, 50).');"></div>'.$library['group']->GetGroupName($groupID).'</a>';
				$postLink = "groups.php?id=$groupID&post=$postid";
			} elseif($row['FromWhat'] == "Newsfeed") {
				$uid = $row['ID'];
				if($uid != $loggedUser) {
					$postTo = '<b class="font-weight_normal"> posted to</b><a href="index.php?-_-='.$library['user']->GetUsername($uid).'"><div class="profpic2" style="background-image: url('.$library['user']->GetProfilePicture($uid, 50).');"></div>'.$library['user']->GetName($uid).'</a>';
				}
				$postLink = "index.php?-_-=".$library['user']->GetUsername($uid)."&post=$postid";
			}
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, false);
?>
		<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
			<table class="title">
				<tr>
					<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
					<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a><?php echo $postTo; ?></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
					<td width="1" align="center"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
					<?php
					if(	($userid == $loggedUser) || 
						(isset($uid) && $uid == $loggedUser) ||
						($row['FromWhat'] == "Course" && $library['user']->GetType($loggedUser) == "Faculty") ||
						($row['FromWhat'] == "Group" && $library['group']->GetUserType($loggedUser, $groupID) == "Admin")) {
					?>
					<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
						<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
							<h4>Post Menu</h4>
							<?php
							if($userid == $loggedUser) {
							?>
							<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
							<?php
							}
							?>
							<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
						</ul>
					</span></td>
					<?php
					}
					?>
				</tr>
				<?php
				if( ($userid == $loggedUser) || 
					(isset($uid) && $uid == $loggedUser) ||
					($row['FromWhat'] == "Course" && $library['user']->GetType($loggedUser) == "Faculty") ||
					($row['FromWhat'] == "Group" && $library['group']->GetUserType($loggedUser, $groupID) == "Admin")) {
				?>
				<tr>
					<td colspan="4" class="feedmsg">
					<div id="showPost_<?php echo $postHashID; ?>">
					<?php echo nl2br(htmlentities(trim($message))); ?>
					</div>
					<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
					<table class="nopad">
						<tr>
							<td><h4>Edit Post</h4></td>
							<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
						</tr>
					</table>
					<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
					<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
					</div>
					</td>
				</tr>
				<?php 
				} else {
				?>
				<tr>
					<td colspan="3" class="feedmsg">
					<?php echo nl2br(htmlentities(trim($message))); ?>
					</td>
				</tr>
				<?php
				}
				?>
			</table>
			<span id="postLikesFor_<?php echo $postHashID; ?>">
			<?php
			$this->GetPostLikes($postid, $loggedUser, $skin);
			?>
			</span>
			<hr>
			<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
			<?php
			$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
			?>
			</table>
			<table class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library);
				?>
			</table>
		</div>
<?php
		}
	}




	// Get course newsfeed

	public function GetCourseNewsfeed($id, $loggedUser, $skin, $library) {
		$type = "Limited";
		$query = mysql_query("SELECT * FROM Post WHERE ReferTo IS NULL AND ID = $id AND FromWhat = 'Course' ORDER BY PostID DESC");
		while($row = mysql_fetch_array($query)) {
			$postid = $row['PostID'];
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message =  $row['Message'];
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$courseID = $row['ID'];
			$postLink = "courses.php?id=$courseID&post=$postid";
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, false);
			$userType = $library['user']->GetType($loggedUser);
?>
			<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
				<table class="title">
					<tr>
						<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
						<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
						<td width="1" align="center"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
						<?php
						if($userid == $loggedUser || $userType == "Faculty") {
						?>
						<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
							<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
								<h4>Post Menu</h4>
								<?php
								if($userid == $loggedUser) {
								?>
								<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
								<?php
								}
								?>
								<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
							</ul>
						</span></td>
						<?php
						}
						?>
					</tr>
					<?php
					if($userid == $loggedUser || $userType == "Faculty") {
					?>
					<tr>
						<td colspan="4" class="feedmsg">
						<div id="showPost_<?php echo $postHashID; ?>">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</div>
						<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
						<table class="nopad">
							<tr>
								<td><h4>Edit Post</h4></td>
								<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
							</tr>
						</table>
						<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
						<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
						</div>
						</td>
					</tr>
					<?php 
					} else {
					?>
					<tr>
						<td colspan="3" class="feedmsg">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</td>
					</tr>
					<?php
					}
					?>
				</table>
				<span id="postLikesFor_<?php echo $postHashID; ?>">
				<?php
				$this->GetPostLikes($postid, $loggedUser, $skin);
				?>
				</span>
				<hr>
				<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
				?>
				</table>
				<table class="comments" cellspacing="0" cellpadding="0">
					<?php
					$this->DisplayCommentBox($postid, $postHashID, $postLink,  $type,$loggedUser, $library);
					?>
				</table>
			</div>
<?php
		}
	}




	// Get group newsfeed

	public function GetGroupNewsfeed($id, $loggedUser, $skin, $library) {
		$type = "Limited";
		$query = mysql_query("SELECT * FROM Post WHERE ReferTo IS NULL AND ID = $id AND FromWhat = 'Group' ORDER BY PostID DESC");
		while($row = mysql_fetch_array($query)) {
			$postid = $row['PostID'];
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message =  $row['Message'];
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$groupID = $row['ID'];
			$postLink = "groups.php?id=$groupID&post=$postid";
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, false);
			$userType_Group = $library['group']->GetUserType($loggedUser, $groupID);
?>
			<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
				<table class="title">
					<tr>
						<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
						<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
						<td width="1" align="center"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
						<?php
						if($userid == $loggedUser || $userType_Group == "Admin") {
						?>
						<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
							<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
								<h4>Post Menu</h4>
								<?php
								if($userid == $loggedUser) {
								?>
								<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
								<?php
								}
								?>
								<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
							</ul>
						</span></td>
						<?php
						}
						?>
					</tr>
					<?php
					if($userid == $loggedUser || $userType_Group == "Admin") {
					?>
					<tr>
						<td colspan="4" class="feedmsg">
						<div id="showPost_<?php echo $postHashID; ?>">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</div>
						<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
						<table class="nopad">
							<tr>
								<td><h4>Edit Post</h4></td>
								<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
							</tr>
						</table>
						<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
						<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
						</div>
						</td>
					</tr>
					<?php 
					} else {
					?>
					<tr>
						<td colspan="3" class="feedmsg">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</td>
					</tr>
					<?php
					}
					?>
				</table>
				<span id="postLikesFor_<?php echo $postHashID; ?>">
				<?php
				$this->GetPostLikes($postid, $loggedUser, $skin);
				?>
				</span>
				<hr>
				<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
				?>
				</table>
				<table class="comments" cellspacing="0" cellpadding="0">
					<?php
					$this->DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library);
					?>
				</table>
			</div>
<?php
		}
	}




	// View specific user post

	public function ViewUserPost($postid, $viewProfileOf, $loggedUser, $skin, $library) {
		$type = "All";
		$query = mysql_query("SELECT * FROM Post WHERE PostID = $postid AND ReferTo IS NULL");
		while($row = mysql_fetch_array($query)) {
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message =  $row['Message'];
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$uid = $row['ID'];
			if($viewProfileOf == $loggedUser) {
				if($uid != "") {
					$postTo = '<b class="font-weight_normal"> posted to you</b>';
					$postLink = "index.php?-_-=".$library['user']->GetUsername($uid)."&post=$postid";
				} else {
					$postLink = "index.php?-_-=".$library['user']->GetUsername($viewProfileOf)."&post=$postid";
				}
			} else {
				if($uid != "") {
					$postLink = "index.php?-_-=".$library['user']->GetUsername($uid)."&post=$postid";
				} else {
					$postLink = "index.php?-_-=".$library['user']->GetUsername($viewProfileOf)."&post=$postid";
				}
			}
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, true);
?>
			<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
				<table class="title">
					<tr>
						<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
						<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
						<td width="1"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
						<?php
						if(	($userid == $loggedUser) || 
							(isset($uid) && $uid == $loggedUser)) {
						?>
						<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
							<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
								<h4>Post Menu</h4>
								<?php
								if($userid == $loggedUser) {
								?>
								<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
								<?php
								}
								?>
								<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
							</ul>
						</span></td>
						<?php
						}
						?>
					</tr>
					<tr>
					<?php
					if($userid == $loggedUser) {
					?>
					<tr>
						<td colspan="4" class="feedmsg">
						<div id="showPost_<?php echo $postHashID; ?>">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</div>
						<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
						<table class="nopad">
							<tr>
								<td><h4>Edit Post</h4></td>
								<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
							</tr>
						</table>
						<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
						<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
						</div>
						</td>
					</tr>
					<?php
					} else {
					?>
					<tr>
						<td colspan="3" class="feedmsg">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</td>
					</tr>
					<?php
					}
					?>
					</tr>
				</table>
				<span id="postLikesFor_<?php echo $postHashID; ?>">
				<?php
				$this->GetPostLikes($postid, $loggedUser, $skin);
				?>
				</span>
				<hr>
				<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
				?>
				</table>
				<table class="comments" cellspacing="0" cellpadding="0">
					<?php
					$this->DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library);
					?>
				</table>
			</div>&nbsp;
<?php			
		}
		if(mysql_num_rows($query) == 0) {
?>
			<div class="content newsfeed">
				<p class="text">Post not found.</p>
			</div>
<?php
		}
	}




	// View specific course post

	public function ViewCoursePost($postid, $userType, $loggedUser, $skin, $library) {
		$type = "All";
		$query = mysql_query("SELECT * FROM Post WHERE PostID = $postid AND ReferTo IS NULL");
		while($row = mysql_fetch_array($query)) {
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message =  $row['Message'];
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$courseID = $row['ID'];
			$postLink = "courses.php?id=$courseID&post=$postid";
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, true);
?>
			<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
				<table class="title">
					<tr>
						<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
						<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
						<td width="1"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
						<?php
						if($userid == $loggedUser || $userType == "Faculty") {
						?>
						<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
							<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
								<h4>Post Menu</h4>
								<?php
								if($userid == $loggedUser) {
								?>
								<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
								<?php
								}
								?>
								<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
							</ul>
						</span></td>
						<?php
						}
						?>
					</tr>
					<tr>
					<?php
					if($userid == $loggedUser || $userType == "Faculty") {
					?>
					<tr>
						<td colspan="4" class="feedmsg">
						<div id="showPost_<?php echo $postHashID; ?>">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</div>
						<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
						<table class="nopad">
							<tr>
								<td><h4>Edit Post</h4></td>
								<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
							</tr>
						</table>
						<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
						<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
						</div>
						</td>
					</tr>
					<?php
					} else {
					?>
					<tr>
						<td colspan="3" class="feedmsg">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</td>
					</tr>
					<?php
					}
					?>
					</tr>
				</table>
				<span id="postLikesFor_<?php echo $postHashID; ?>">
				<?php
				$this->GetPostLikes($postid, $loggedUser, $skin);
				?>
				</span>
				<hr>
				<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
				?>
				</table>
				<table class="comments" cellspacing="0" cellpadding="0">
					<?php
					$this->DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library);
					?>
				</table>
			</div>&nbsp;
<?php			
		}
		if(mysql_num_rows($query) == 0) {
?>
			<div class="content newsfeed">
				<p class="text">Post not found.</p>
			</div>
<?php
		}
	}




	// View specific group post

	public function ViewGroupPost($postid, $userType_Group, $loggedUser, $skin, $library) {
		$type = "All";
		$query = mysql_query("SELECT * FROM Post WHERE PostID = $postid AND ReferTo IS NULL");
		while($row = mysql_fetch_array($query)) {
			$postHashID = $this->GeneratePostHash($postid);
			$userid = $row['UserID'];
			$message =  $row['Message'];
			$date = $row['DatePosted'];
			$time = $row['TimePosted'];
			$datetimetitle = date('F d, Y', strtotime($date)).' - '.date('h:i a', strtotime($time));
			$datetimeInterval = $this->GetDateTimeInterval($date, $time);
			$courseID = $row['ID'];
			$postLink = "groups.php?id=$courseID&post=$postid";
			$this->GetjQueryScript($postid, $postHashID, $userid, $type, $date, $time, $postLink, $loggedUser, $library, $skin, true);
?>
			<div class="content newsfeed" id="containerPost_<?php echo $postHashID; ?>">
				<table class="title">
					<tr>
						<td><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><div style="background-image: url(<?php echo $library['user']->GetProfilePicture($userid, 50); ?>)" class="profilepicture"></div></a></td>
						<td><h5><a href="index.php?-_-=<?php echo $library['user']->GetUsername($userid); ?>"><?php echo $library['user']->GetName($userid); ?></a></h5><span id="datetimeInterval_<?php echo $postHashID; ?>" class="date"><a title="<?php echo $datetimetitle; ?>" href="<?php echo $postLink; ?>"><?php echo $datetimeInterval; ?></a></span></td>
						<td width="1"><a id="btnLikeFor_<?php echo $postHashID; ?>" isLike="0" title="Like this post" class="mini_icons like"></a></td>
						<?php
						if($userid == $loggedUser || $userType_Group == "Admin") {
						?>
						<td width="1"><span class="nohov" style="margin-top: 10px;"><a canClick="1" id="morePostIco_<?php echo $postHashID; ?>" class="mini_icons moreoptions"></a>
							<ul class="morepostddl" id="morePost_<?php echo $postHashID; ?>">
								<h4>Post Menu</h4>
								<?php
								if($userid == $loggedUser) {
								?>
								<a id="btnEditFor_<?php echo $postHashID; ?>">Edit Post</a>
								<?php
								}
								?>
								<a id="btnDeleteFor_<?php echo $postHashID; ?>">Delete Post</a>
							</ul>
						</span></td>
						<?php
						}
						?>
					</tr>
					<tr>
					<?php
					if($userid == $loggedUser || $userType_Group == "Admin") {
					?>
					<tr>
						<td colspan="4" class="feedmsg">
						<div id="showPost_<?php echo $postHashID; ?>">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</div>
						<div style="display: none;" id="editPost_<?php echo $postHashID; ?>">
						<table class="nopad">
							<tr>
								<td><h4>Edit Post</h4></td>
								<td><a id="btnCancelEdit_<?php echo $postHashID; ?>" class="canceledit"></a></td>
							</tr>
						</table>
						<textarea id="textareaPost_<?php echo $postHashID; ?>" class="resizabletext editpost"><?php echo $message; ?></textarea>
						<a id="saveEditedPost_<?php echo $postHashID; ?>" class="inputbutton editpost">Save</a>
						</div>
						</td>
					</tr>
					<?php
					} else {
					?>
					<tr>
						<td colspan="3" class="feedmsg">
						<?php echo nl2br(htmlentities(trim($message))); ?>
						</td>
					</tr>
					<?php
					}
					?>
					</tr>
				</table>
				<span id="postLikesFor_<?php echo $postHashID; ?>">
				<?php
				$this->GetPostLikes($postid, $loggedUser, $skin);
				?>
				</span>
				<hr>
				<table id="tblCommentsFor_<?php echo $postHashID; ?>" class="comments" cellspacing="0" cellpadding="0">
				<?php
				$this->GetCommentsToPost($postid, $postHashID, $postLink, $type, $loggedUser, $skin, $library);
				?>
				</table>
				<table class="comments" cellspacing="0" cellpadding="0">
					<?php
					$this->DisplayCommentBox($postid, $postHashID, $postLink, $type, $loggedUser, $library);
					?>
				</table>
			</div>&nbsp;
<?php			
		}
		if(mysql_num_rows($query) == 0) {
?>
			<div class="content newsfeed">
				<p class="text">Post not found.</p>
			</div>
<?php
		}
	}
}
?>