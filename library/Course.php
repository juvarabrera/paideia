<?php
class Course {




	// Get course name

	public function GetCourseName($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$query = mysql_query("SELECT Name FROM Course WHERE CourseID = '$courseID'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Name'];
		}
		return $x;
	}




	// Get course key

	public function GetCourseKey($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$query = mysql_query("SELECT CourseKey FROM Course WHERE CourseID = '$courseID'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['CourseKey'];
		}
		return $x;
	}




	// Get course description

	public function GetCourseDescription($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$query = mysql_query("SELECT Description FROM Course WHERE CourseID = '$courseID'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Description'];
		}
		return $x;
	}




	// Get profile picture ($size = 50, 200, 800, orig)

	public function GetProfilePicture($courseID, $size) {
		$courseID = mysql_real_escape_string($courseID);
		$x = "courses/$courseID/dp/$size.jpg";
		if(!file_exists($x)) {
			$x = "courses/nopic.png";
		}
		return $x;
	}




	// Get course type (Open, Closed)

	public function GetCourseType($courseID) {
		$type = "Closed";
		$query = mysql_query("SELECT * FROM Course WHERE CourseID = $courseID AND CourseKey IS NULL");
		if(mysql_num_rows($query) >= 1)
			$type = "Open";
		return $type;
	}




	// Get all courses enrolled by a user

	public function GetAllCoursesHandledBy($loggedUser) {
		$x = array();
		$query = mysql_query("SELECT CourseID FROM CourseUser WHERE UserID = '$loggedUser'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['CourseID'];
		}
		return $x;
	}




	// Get all professors or faculty 

	public function GetAllProfessors($course) {
		$course = mysql_real_escape_string($course);
		$x = array();
		$query = mysql_query("SELECT CU.UserID FROM CourseUser CU LEFT JOIN Account A ON CU.UserID = A.ID WHERE CU.CourseID = '$course' AND A.Type = 'Faculty'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['UserID'];
		}
		return $x;
	}




	// Get all students

	public function GetAllStudents($course) {
		$course = mysql_real_escape_string($course);
		$x = array();
		$query = mysql_query("SELECT CU.UserID FROM CourseUser CU LEFT JOIN Account A ON CU.UserID = A.ID WHERE CU.CourseID = '$course' AND A.Type = 'Student'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['UserID'];
		}
		return $x;
	}




	// Get number of students

	public function GetNumberOfStudentsIn($course) {
		$course = mysql_real_escape_string($course);
		$x = 0;
		$query = mysql_query("SELECT COUNT(CU.UserID) AS Count FROM CourseUser CU LEFT JOIN Account A ON A.ID = CU.UserID WHERE CU.CourseID = '$course' AND A.Type = 'Student'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Count'];
		}
		return $x;
	}




	// Get number of faculty

	public function GetNumberOfProfessorsIn($course) {
		$course = mysql_real_escape_string($course);
		$x = 0;
		$query = mysql_query("SELECT COUNT(CU.UserID) AS Count FROM CourseUser CU LEFT JOIN Account A ON A.ID = CU.UserID WHERE CU.CourseID = '$course' AND A.Type = 'Faculty'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['Count'];
		}
		return $x;
	}




	// Generate course key

	public function GenerateCourseKey() {
		$key = "";
		$key = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 2) . rand(1000,9999);
		return $key;
	}




	// Edit course name and description

	public function EditCourse($courseid, $name, $desc, $type) {
		$courseid = mysql_real_escape_string($courseid);
		$name = mysql_real_escape_string($name);
		$desc = mysql_real_escape_string($desc);
		if($type == "Open") {
			mysql_query("UPDATE Course SET Name = '$name', Description = '$desc', CourseKey = NULL WHERE CourseID = '$courseid'");
		} else {
			$courseKey = $this->GenerateCourseKey();
			mysql_query("UPDATE Course SET Name = '$name', Description = '$desc', CourseKey = '$courseKey' WHERE CourseID = '$courseid'");
		}
	}




	// Create a course

	public function CreateCourse($loggedUser, $name, $desc, $ctype) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$name = mysql_real_escape_string($name);
		$desc = mysql_real_escape_string($desc);
		$uniqueKey = false;
		while(!$uniqueKey) {
			$generatedKey = $this->GenerateCourseKey();
			$query = mysql_query("SELECT * FROM Course WHERE CourseKey = '$generatedKey'");
			if(mysql_num_rows($query) == 0) {
				$uniqueKey = true;
			}
		}
		if($ctype == "Open")
			mysql_query("INSERT INTO Course (Name, Description) VALUES ('$name', '$desc')");
		else
			mysql_query("INSERT INTO Course (Name, CourseKey, Description) VALUES ('$name', '$generatedKey', '$desc')");
		
		$query = mysql_query("SELECT CourseID FROM Course ORDER BY CourseID DESC LIMIT 1");
		$courseid = 0;
		while($row = mysql_fetch_array($query)) {
			$courseid = $row['CourseID'];
		}
		if(!is_dir("courses/$courseid"))
			mkdir("courses/$courseid",0777);
		if(!is_dir("courses/$courseid/dp"))
			mkdir("courses/$courseid/dp",0777);
		if(!is_dir("courses/$courseid/resources"))
			mkdir("courses/$courseid/resources",0777);
		copy("courses/index.php", "courses/$courseid/index.php");
		copy("courses/index.php", "courses/$courseid/dp/index.php");
		copy("courses/index.php", "courses/$courseid/resources/index.php");
		mysql_query("INSERT INTO CourseUser VALUES ('$courseid', '$loggedUser')");
		return $courseid;
	}




	// Check if user is enrolled to a course

	public function IsEnrolled($loggedUser, $courseID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$courseID = mysql_real_escape_string($courseID);
		$enrolled = false;
		$query = mysql_query("SELECT * FROM CourseUser WHERE CourseID = '$courseID' AND UserID = '$loggedUser' LIMIT 1");
		if(mysql_num_rows($query) == 1)
			$enrolled = true;
		return $enrolled;
	}




	// Check if course if open or closed course

	public function IsOpenCourse($courseID) {
		$isopen = false;
		$query = mysql_query("SELECT * FROM Course WHERE CourseID = $courseID AND CourseKey IS NULL");
		if(mysql_num_rows($query) >= 1)
			$isopen = true;
		return $isopen;
	}




	// Get course id from key

	public function GetCourseIDFromKey($key) {
		$key = mysql_real_escape_string($key);
		$x = "";
		$query = mysql_query("SELECT CourseID FROM Course WHERE CourseKey = '$key'");
		while($row = mysql_fetch_array($query)) {
			$x = $row['CourseID'];
		}
		return $x;
	}




	// Check if course exists from course key

	public function CheckIfCourseExistsWithCourseKey($ck) {
		$ck = mysql_real_escape_string($ck);
		$exists = false;
		$query = mysql_query("SELECT CourseID FROM Course WHERE CourseKey = '$ck'");
		if(mysql_num_rows($query) >= 1)
			$exists = true;
		return $exists;
	}




	// Check if course exists from course id

	public function CheckIfCourseExistWithCourseID($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$exists = false;
		$query = mysql_query("SELECT * FROM Course WHERE CourseID = '$courseID'");
		if(mysql_num_rows($query) >= 1)
			$exists = true;
		return $exists;
	}




	// Check user is enrolled to a course

	public function CheckIfEnrolled($loggedUser, $courseID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$courseID = mysql_real_escape_string($courseID);
		$enrolled = false;
		$query = mysql_query("SELECT * FROM CourseUser  WHERE CourseID = '$courseID' AND UserID = '$loggedUser'");
		if(mysql_num_rows($query) >= 1) 
			$enrolled = true;
		return $enrolled;
	}




	// Enroll user to a course

	public function EnrollToACourse($loggedUser, $courseID) {
		$loggedUser = mysql_real_escape_string($loggedUser);
		$courseID = mysql_real_escape_string($courseID);
		mysql_query("INSERT INTO CourseUser VALUES ('$courseID', '$loggedUser')");
	}




	// Delete course

	public function DeleteCourse($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		mysql_query("DELETE FROM Course WHERE CourseID = '$courseID'");
		mysql_query("DELETE FROM CourseUser WHERE CourseID = '$courseID'");
		mysql_query("DELETE FROM Assessment WHERE CourseID = '$courseID'");
		mysql_query("DELETE FROM Question WHERE AssessmentID IN (SELECT AssessmentID FROM Assessment WHERE CourseID = '$courseID')");
		mysql_query("DELETE FROM Calendar WHERE ID = '$courseID' AND FromWhat = 'Course'");
		mysql_query("DELETE FROM Post WHERE ID = '$courseID' AND FromWhat = 'Course'");
		mysql_query("DELETE FROM Notification WHERE ActionID = '$courseID'");
		$path = "courses/".$courseID;
		echo $path;
		if(file_exists($path))
			$this->deleteDir($path);
	}




	// Delete course directory from server

	public function deleteDir($dirPath) {
		$dirPath = $dirPath;
		if(!is_dir($dirPath)) {
			throw new InvalidArgumentException("error");
		}
		if(substr($dirPath, strlen($dirPath)-1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach($files as $file) {
			if(is_dir($file)) {
				$this->deleteDir($file);
			} else {
				unlink($file);
			}
		}
		rmdir($dirPath);
	}




	// Check if user sent a request

	public function IsRequested($courseID, $userID) {
		$requested = false;
		$query = mysql_query("SELECT * FROM CourseRequest WHERE CourseID = $courseID AND UserID = $userID");
		if(mysql_num_rows($query) >= 1)
			$requested = true;
		return $requested;
	}




	// Get all requests

	public function GetRequest($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$x = array();
		$query = mysql_query("SELECT UserID FROM CourseRequest WHERE CourseID = '$courseID'");
		while($row = mysql_fetch_array($query)) {
			$x[] = $row['UserID'];
		}
		return $x;
	}




	// Get all requests to approve

	public function GetRequestToApprove($courseID, $loggedUser, $library, $skin) {
		$query = mysql_query("SELECT UserID FROM CourseRequest WHERE CourseID = '$courseID'");
		while($row = mysql_fetch_array($query)) {
			$userID = $row['UserID'];
			$dp = $library['user']->GetProfilePicture($userID,50);
			$name = $library['user']->GetName($userID);
			$type = $library['user']->GetType($userID);
			$hash = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2) . rand(1000,9999) . $userID;
			if(strlen($name) > 20)
				$name = substr($name, 0, 20).'...';
?>
				<script>
				$(document).ready(function () {
					$('#btnAcceptRequest_<?php echo $hash;?>').click(function () {
						$('#lstRequest_<?php echo $hash;?>').css({
							"display": "none"
						});
						$('#msgRequest_<?php echo $hash;?>').text('has been accepted to this course.');
						$('#lstRequest_<?php echo $hash;?>_click').css({
							"display": "block"
						});
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=joinpeople",
							data: {id: <?php echo $courseID; ?>, fromwhat: 'Course', userid: <?php echo $userID; ?>},
							success: function(html) {
								setTimeout(function() {
									$refreshlistRequestCourses = 1;
								}, 2000);
							}
						});
					});
					$('#btnRejectRequest_<?php echo $hash;?>').click(function () {
						$('#lstRequest_<?php echo $hash;?>').css({
							"display": "none"
						});
						$('#msgRequest_<?php echo $hash;?>').text('has been rejected to this course.');
						$('#lstRequest_<?php echo $hash;?>_click').css({
							"display": "block"
						});
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=deletejoinrequest",
							data: {id: <?php echo $courseID; ?>, fromwhat: 'Course', userid: <?php echo $userID; ?>},
							success: function(html) {
								setTimeout(function() {
									$refreshlistRequestCourses = 1;
								}, 2000);
							}
						});
					});
				});
				</script>
				<li id="lstRequest_<?php echo $hash;?>"><table class="results"><tr valign="top">
					<td><a href="index.php?-_-=<?php echo $username; ?>" title="<?php echo $library['user']->GetName($userID); ?>"><div style="background-image: url(<?php echo $dp; ?>)" class="profilepicture"></div></a></td>
					<td><a href="index.php?-_-='.$username.'" title="<?php echo $library['user']->GetName($userID); ?>"><?php echo $name; ?></a><br><small><?php echo $type; ?></small></td>
					<td class="button"><a id="btnAcceptRequest_<?php echo $hash;?>" class="inputbutton">Accept</a></td>
					<td class="button"><a id="btnRejectRequest_<?php echo $hash;?>" class="inputbutton">Reject</a></td>
				</tr></table></li>
				<li id="lstRequest_<?php echo $hash;?>_click" style="display: none;"><table class="results"><tr valign="top">
					<td><a href="index.php?-_-=<?php echo $username; ?>" title="<?php echo $library['user']->GetName($userID); ?>"><div style="background-image: url(<?php echo $dp; ?>)" class="profilepicture"></div></a></td>
					<td><a href="index.php?-_-='.$username.'" title="<?php echo $library['user']->GetName($userID); ?>"><?php echo $library['user']->GetName($userID); ?></a> <span id="msgRequest_<?php echo $hash;?>"></span></td>
				</tr></table></li>
<?php
		}
		if(mysql_num_rows($query) == 0) {
			echo '<center><p class="text">There are no people who requested to enroll this course.</p></center>';
		}
	}




	// Checks if course members posted this post

	public function CheckIfCoursePost($courseID, $postID) {
		$query = mysql_query("SELECT * FROM Post WHERE FromWhat = 'Course' AND ID = $courseID AND PostID = $postID");
		$userpost = false;
		if(mysql_num_rows($query) >= 1)
			$userpost = true;
		return $userpost;
	}




	// Force enroll people to group

	public function AcceptRequest($courseID, $userID) {
		mysql_query("INSERT INTO CourseUser (CourseID, UserID) VALUES ($courseID, $userID)");
		mysql_query("DELETE FROM CourseRequest WHERE CourseID = $courseID AND UserID = $userID");
	}




	// Reject request

	public function RejectRequest($courseID, $userID) {
		mysql_query("DELETE FROM CourseRequest WHERE CourseID = $courseID AND UserID = $userID");
	}




	// Force enroll people to course

	public function DeleteRequest($groupID, $userID) {
		mysql_query("INSERT INTO GroupUser (GroupID, UserID, Type) VALUES ($groupID, $userID, 'Member')");
		mysql_query("DELETE FROM GroupBlockRequest WHERE GroupID = $groupID AND UserID = $userID");
	}




	// Gets people on course

	public function GetPeopleOnCourse($courseID, $loggedUser, $library, $skin) {
?>
		<p class="text"><b>Professors</b></p>
		<hr>
		<ul class="contentlist">
			<?php
			$row = $library['course']->GetAllProfessors($courseID);
			for($i = 0; $i < sizeof($row); $i++) {
				$dp = $library['user']->GetProfilePicture($row[$i],50);
				echo '<a href="index.php?-_-='.$library['user']->GetUsername($row[$i]).'"><div class="profpic" style="background-image: url('.$dp.');"></div>'.$library['user']->GetName($row[$i]).'<br><small>'.$library['user']->GetUsername($row[$i]).'</small></a>';
			}
			if(sizeof($row) == 0) 
				echo '<a>There are no professors handling this course.</a>';
			?>
		</ul>
		<p class="text"><b>Students</b></p>
		<hr>
		<ul class="contentlist">
			<?php
			$row = $library['course']->GetAllStudents($courseID);
			for($i = 0; $i < sizeof($row); $i++) {
				$dp = $library['user']->GetProfilePicture($row[$i],50);
				echo '<a href="index.php?-_-='.$library['user']->GetUsername($row[$i]).'"><div class="profpic" style="background-image: url('.$dp.');"></div>'.$library['user']->GetName($row[$i]).'<br><small>'.$library['user']->GetUsername($row[$i]).'</small></a>';
			}
			if(sizeof($row) == 0) 
				echo '<a>There are no students enrolled.</a>';
			?>
		</ul>
<?php
	}




	// Gets people to invite on course

	public function GetPeopleToInvite($searchQuery, $courseID, $loggedUser, $library, $skin) {
		$query = mysql_query("SELECT * FROM Account WHERE ID NOT IN (SELECT UserID FROM CourseUser WHERE CourseID = $courseID) AND (FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR CONCAT(FirstName, ' ', LastName) LIKE '%$searchQuery%') LIMIT 5");
		while($row = mysql_fetch_array($query)) {
			$userid = $row['ID'];
			$hash = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2) . rand(1000,9999) . $userid;
			$fullname = $row['FirstName']. ' ' . $row['LastName'];
			$title = $fullname;
			if(strlen($fullname) > 20)
				$fullname = substr($fullname, 0, 20).'...';
			$type = $row['Type'];
			$dp = $library['user']->GetProfilePicture($userid, 50);
		?>
		<script>
		$(document).ready(function () {
			$('#InvitationResult_<?php echo $hash; ?>').click(function() {
				$text = $('#InvitationResult_<?php echo $hash; ?>').text();
				if($text != "<?php echo $title; ?> is now enrolled to this course.") {
					$('#InvitationResult_<?php echo $hash; ?>').css({
						"display": "none"
					});
					$('#InvitationResult_<?php echo $hash; ?>_success').css({
						"display": "block"
					});
					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=joinpeople",
						data: {id: <?php echo $courseID; ?>, fromwhat: 'Course', userid: <?php echo $userid ?>, request: 1},
						success: function() {
							$refreshlistPeopleCourses = 1;
							$refreshlistRequestCourses = 1;
						}
					});
				}
			});
		});
		</script>
		<a id="InvitationResult_<?php echo $hash; ?>" class="hoverit" title="<?php echo $title; ?>"><div class="profpic" style="background-image: url(<?php echo $dp; ?>);"></div><?php echo $fullname; ?><br><small><?php echo $type; ?></small></a>
		<a id="InvitationResult_<?php echo $hash; ?>_success" class="hoverit" style="display: none;" title="<?php echo $title; ?> is now enrolled to this course."><div class="profpic" style="float: left; width: 40px; height: 40px; background-image: url(<?php echo $dp; ?>);"></div><small><?php echo $title; ?> is now enrolled to this course.</small></a>
		<?php
		}
		if(mysql_num_rows($query) == 0) {
			echo '<a><center><small><i>No user found.</i></small></center></a>';
		}
	}
}
?>