<?php
require_once('library/User.php');
require_once('library/Course.php');
require_once('library/Group.php');
require_once('library/Calendar.php');
require_once('library/Resources.php');
require_once('library/Assessment.php');
require_once('library/ImageHandling.php');
require_once('library/Newsfeed.php');

	function GetConnection() {
		$mysql_host = "localhost";
		$mysql_user = "root";
		$mysql_password = "";
		$mysql_database = "paideia_db";
		/*$mysql_host = "mysql8.000webhost.com";
		$mysql_database = "a7193628_lms";
		$mysql_user = "a7193628_lms";
		$mysql_password = "04jU10v1A9r96";*/
		mysql_connect($mysql_host, $mysql_user, $mysql_password);
		mysql_select_db($mysql_database);
	}
	function ResetDBConnection() {
		mysqli_close();
	}
	function Logout() {
		session_destroy();
	}
	function echoBoolean($bool) {
		$x = 0;
		if($bool)
			$x = 1;
		return $x;
	}

GetConnection();

session_start();
date_default_timezone_set('Asia/Manila');
if(isset($_SESSION['golem_username'])) {
	$status = "LoggedIn";
	$library = array(
		'user' => new User(),
		'course' => new Course(),
		'group' => new Group(),
		'calendar' => new Calendar(),
		'assessment' => new Assessment(),
		'resources' => new Resources(),
		'imageh' => new ImageHandling(),
		'newsfeed' => new Newsfeed()
	);
	$loggedUser = $library['user']->GetID($_SESSION['golem_username']);
	$userType = $library['user']->GetType($loggedUser);
}
else $status = "Login";

$skin = "float-transparency";
?>