<?php
class Assessment {




	// Get assessments of a course

	public function GetCourseID($assessID) {
		$x = "";
		$query =mysql_query("SELECT CourseID FROM Assessment WHERE AssessmentID = '$assessID'");
		while($row = mysql_fetch_array($query))
			$x = $row['CourseID'];
		return $x;
	}




	// Get courseid of the assessment

	public function GetCourseAssessment($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$x = array();
		$query =mysql_query("SELECT AssessmentID FROM Assessment WHERE CourseID = '$courseID'");
		while($row = mysql_fetch_array($query))
			$x[] = $row['AssessmentID'];
		return $x;
	}




	// Get active assessments of a course

	public function GetCourseActiveAssessment($courseID) {
		$courseID = mysql_real_escape_string($courseID);
		$x = array();
		$query =mysql_query("SELECT AssessmentID FROM Assessment WHERE Status = 1 AND CourseID = '$courseID'");
		while($row = mysql_fetch_array($query))
			$x[] = $row['AssessmentID'];
		return $x;
	}




	// Get assessment name

	public function GetAssessmentName($assessID) {
		$assessID = mysql_real_escape_string($assessID);
		$query = mysql_query("SELECT Name FROM Assessment WHERE AssessmentID = '$assessID'");
		$row = mysql_fetch_array($query);
		$x = $row['Name'];
		return $x;
	}




	// Get assessment status

	public function GetAssessmentStatus($assessID) {
		$assessID = mysql_real_escape_string($assessID);
		$x = "Inactive";
		$query = mysql_query("SELECT Status FROM Assessment WHERE AssessmentID = '$assessID'");
		$row = mysql_fetch_array($query);
		if($row['Status'] == 1)
			$x = "Active";
		return $x;
	}




	// Get assessment maximum attempts

	public function GetAssessmentAttempts($assessID) {
		$assessID = mysql_real_escape_string($assessID);
		$x = 0;
		$query = mysql_query("SELECT Attempts FROM Assessment WHERE AssessmentID = '$assessID'");
		$row = mysql_fetch_array($query);
		$x = $row['Attempts'];
		return $x;
	}




	// Get assessment overall points

	public function CheckAssessmentIDAndCourse($courseID, $assessID) {
		$exists = false;
		$query = mysql_query("SELECT * FROM Assessment WHERE AssessmentID = $assessID AND CourseID = $courseID");
		if(mysql_num_rows($query) >= 1) 
			$exists = true;
		return $exists;
	}




	// Get start date

	public function GetAssessmentStartDate($assessID) {
		$date = "";
		$query = mysql_query("SELECT DateFrom FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$date = $row['DateFrom']; 
		}
		return $date;
	}




	// Get end date

	public function GetAssessmentDueDate($assessID) {
		$date = "";
		$query = mysql_query("SELECT DateTo FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$date = $row['DateTo']; 
		}
		return $date;
	}




	// Get start time

	public function GetAssessmentStartTime($assessID) {
		$time = "";
		$query = mysql_query("SELECT TimeFrom FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$time = $row['TimeFrom']; 
		}
		return $time;
	}




	// Get end time

	public function GetAssessmentDueTime($assessID) {
		$time = "";
		$query = mysql_query("SELECT TimeTo FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$time = $row['TimeTo']; 
		}
		return $time;
	}




	// Get timer

	public function GetAssessmentTimer($assessID) {
		$timer = "";
		$query = mysql_query("SELECT Timer FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$timer = $row['Timer']; 
		}
		return $timer;
	}




	// Get instructions

	public function GetAssessmentInstruction($assessID) {
		$instruction = "";
		$query = mysql_query("SELECT Instructions FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$instruction = $row['Instructions']; 
		}
		return $instruction;
	}




	// Get instructions

	public function GetAssessmentDateCreated($assessID) {
		$date = "";
		$query = mysql_query("SELECT DateCreated FROM Assessment WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$date = $row['DateCreated']; 
		}
		return $date;
	}




	// Check if due date/time and start date/time is in proper interval
	// -- meaning due date/time > start date/time

	// For PHP ver 5.2 and below

	public function CheckProperInterval($startdate, $starttime, $duedate, $duetime) {
		$proper = false;
		date_default_timezone_set('Asia/Manila');
		$date1 = new DateTime($startdate);
		$date2 = new DateTime($duedate);
		$dateInterval_d = round(($date2->format('U') - $date1->format('U')) / (60*60*24));
		if($dateInterval_d > 0) {
			$proper = true;
		} elseif($dateInterval_d == 0) {
			$time1 = strtotime($starttime);
			$time2 = strtotime($duetime);
			$timeInterval_h = ($time2 - $time1) / (60*60);
			$timeInterval_i = ($time2 - $time1) / (60);
			if($timeInterval_h > 0 ) {
				$proper = true;
			} elseif($timeInterval_h == 0) {
				if($timeInterval_i > 0) {
					$proper = true;
				}
			}
		}
		return $proper;
	}




	// Generates hash for question to execute commands specifically on the question

	public function GeneratePostHash($questionid) {
		return substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2) . rand(1000,9999) . $questionid;
	}




	// Get questions to assessment

	public function GetQuestionsToAssessment($courseID, $assessID, $skin) {
		$query = mysql_query("SELECT * FROM Question WHERE AssessmentID = $assessID");
		$qNumber = 1;
		while($row = mysql_fetch_array($query)) {
			$questionid = $row['QuestionID'];
			$qtype = $row['QuestionType'];
			$question = nl2br(htmlentities($row['Question']));
			$choices = $row['Choices'];
			$points = $row['Points'];
			if($points == 1)
				$points = "$points point";
			else
				$points = "$points points";
			$questionHashID = $this->GeneratePostHash($questionid);
?>
			<script>
			$(document).ready(function() {
				$('#<?php echo $questionHashID; ?> .deleteq').click(function() {
					var yes = confirm("Are you sure you want to delete this question?\n\nNote: You cannot undo this action.");
					if(yes) {
						$('#<?php echo $questionHashID; ?>').html("<center><br>Deleting question...<br></center>");
						$.ajax({
							type: "POST",
							cache: false,
							url: "process.php?action=deletequestion",
							data: {aid: <?php echo $assessID; ?>, courseID: <?php echo $courseID; ?>, qid: <?php echo $questionid; ?>},
							success: function() {
								$('#<?php echo $questionHashID; ?>').html("<center><br>Question deleted.<br></center>");
								setTimeout(function() {
									$.ajax({
										type: "POST",
										cache: false,
										url: "process.php?action=refreshquestions",
										data: {aid: <?php echo $assessID; ?>, courseID: <?php echo $courseID; ?>},
										success: function(html) {
											$('#QuestionsTo_<?php echo $assessID; ?>').html(html);
										}
									});
								}, 1000);
							}
						});
					}
				});
				$('#<?php echo $questionHashID; ?> .editq').click(function() {
					$('#<?php echo $questionHashID; ?> .showQuestion').css({
						"display": "none"
					});
					$('#<?php echo $questionHashID; ?> .editQuestion').css({
						"display": "block"
					});
					<?php if($qtype == "Multiple Choice") { ?>

					$.ajax({
						type: "POST",
						cache: false,
						url: "process.php?action=loadchoices",
						data: {questionid: <?php echo $questionid; ?>},
						success: function() {
							$.ajax({
								type: "POST",
								cache: false,
								url: "process.php?action=refreshchoices",
								data: {qid: <?php echo $questionid; ?>},
								success: function(html) {
									$('#popupChoices_<?php echo $questionid; ?>').html(html)
								}
							});
						}
					});

					<?php } ?>
				});
			});
			</script>
<?php
			if($qtype == "True or False") {
?>
			<div id="<?php echo $questionHashID; ?>" class="qlist" isview="0">
				<div class="showQuestion">
					<div class="qsettings">
						<div class="editq" title="Edit Question"></div>
						<div class="deleteq" title="Delete Question"></div>
					</div>

					<b>Question <?php echo $qNumber; ?>:</b> <?php echo $question; ?><br><br>
					<b>Question Type:</b> <?php echo $qtype; ?><br><br>
					<b>Correct Answer:</b> <?php echo $choices; ?><br><br>
					<b>Points:</b> <?php echo $points; ?>
				</div>
				<div class="editQuestion" style="display: none">
					<form action="process.php?action=editquestion_trueorfalse" method="POST">
					<table class="form">
						<tr valign="top">
							<td colspan="2"><b>Question <?php echo $qNumber; ?></b></td>
						</tr>
						<tr valign="top">
							<td>Question:</td>
							<td><textarea name="question" style="resize: none; height: 100px;" maxlength="500" required><?php echo $row['Question']; ?></textarea></td>
						</tr>
						<tr valign="top">
							<td>Answer:</td>
							<td>
							<select name="answer">
								<?php
								$choices_tor = array("True", "False");
								for($i = 0; $i < sizeof($choices_tor); $i++) {
									$selected = "";
									if($choices_tor[$i] == $choices)
										$selected = " selected=\"selected\"";
									echo '<option value="'.$choices_tor[$i].'"'.$selected.'>'.$choices_tor[$i].'</option>';
								}
								?>
							</select>
							</td>
						</tr>
						<tr valign="top">
							<td>Points:</td>
							<td><input type="number" min="1" max="100" name="points" placeholder="Default value: 1 point" value="<?php echo $row['Points']; ?>" required></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="submit" value="Save"></td>
						</tr>
					</table>
					<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
					<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
					<input type="hidden" name="questionID" value="<?php echo $questionid; ?>">
					</form>
				</div>
			</div>
<?php
			} // end true or false
			elseif($qtype == "Multiple Choice") {
?>
			<div id="<?php echo $questionHashID; ?>" class="qlist" isview="0">
				<div class="showQuestion">
					<div class="qsettings">
						<div class="editq" title="Edit Question"></div>
						<div class="deleteq" title="Delete Question"></div>
					</div>

					<b>Question <?php echo $qNumber; ?>:</b> <?php echo $question; ?><br><br>
					<b>Question Type:</b> Multiple Choice<br><br>
					<b>Choices:</b>
					<ul class="choices">
					<?php
					$choices = substr($choices, 4);
					$choices = substr($choices, 0, -3);
					$choices = explode("}); t.({", $choices);
					for($i = 0; $i < sizeof($choices); $i++) {
						$correct = "";
						if($i == 0) {
							$correct = " class=\"correct\"";
						}
						echo '<li'.$correct.'>'.$choices[$i].'</li>';
					}
					?>
					</ul>
					<b>Points:</b> <?php echo $points; ?>
				</div>
				<div class="editQuestion" style="display: none">
					<form action="process.php?action=editquestion_multiplechoice" method="post">
					<table class="form">
						<tr valign="top">
							<td width="95px">Question:</td>
							<td><textarea name="question" style="resize: none; height: 100px;" maxlength="500" required><?php echo $question; ?></textarea></td>
						</tr>
						<tr>
							<td>Add Choice:</td>
							<td>
							<center><img src="images/skin/<?php echo $skin; ?>/bg/loading.gif" class="loadingGif" id="loadingAddChoice_<?php echo $questionid; ?>" style="display: none; margin-top: 8px;"></center>
							<input type="text" name="addchoice" id="txtAddChoice_<?php echo $questionid; ?>" placeholder="Press 'Enter' to add choice">
							<script>
							$(document).ready(function() {
								$('#txtAddChoice_<?php echo $questionid; ?>').keypress(function(event) {
									if(event.keyCode == 13) {
										$text = $(this).val();
										if($text != "") {
											$('#loadingAddChoice_<?php echo $questionid; ?>').css({"display": "block"});
											$(this).css({"display": "none"});
											$(this).val('');
											$.ajax({
												type: "POST",
												cache: false,
												url: "process.php?action=addchoice",
												data: {choice: $text, qid: <?php echo $questionid; ?>},
												success: function(html) {
													$.ajax({
														type: "POST",
														cache: false,
														url: "process.php?action=refreshchoices",
														data: {qid: <?php echo $questionid; ?>},
														success: function(html) {
															$('#popupChoices_<?php echo $questionid; ?>').html(html);
															$('#loadingAddChoice_<?php echo $questionid; ?>').css({"display": "none"});
															$('#txtAddChoice_<?php echo $questionid; ?>').css({"display": "block"});
															$('#txtAddChoice_<?php echo $questionid; ?>').focus();
														}
													});
												}
											});
										}
										event.preventDefault();
										return false;
									}
								});
							});
							</script>
							</td>
						</tr>
						<tr valign="top">
							<td>Choices:<br><br><small>*<i>Select the correct answer</i></small></td>
							<td id="popupChoices_<?php echo $questionid; ?>">
							<center><img src="images/skin/<?php echo $skin;?>/bg/loading.gif" class="loadingGif" style="margin-top: 30px;"></center>
							</td>
						</tr>
						<tr>
							<td>Points:</td>
							<td>
							<input type="number" min="1" max="100" name="points"  value="<?php echo $row['Points']; ?>" placeholder="Default value: 1 point" required></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" id="btnSubmit_SaveQuestion_<?php echo $questionid; ?>" name="submit" value="Save"></td>
						</tr>
					</table>
					<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
					<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
					<input type="hidden" name="questionID" value="<?php echo $questionid; ?>">
					</form>
					<script>
					$(document).ready(function() {
						$('#btnSubmit_SaveQuestion_<?php echo $questionid; ?>').click(function(event) {
							$val = $('#choicestoanswer_<?php echo $questionid; ?>').val();
							if($val == "no") {
								alert("Please add choices to question.");
								event.preventDefault();
								return false;
							}
						});
					});
					</script>
				</div>
			</div>
<?php
			} elseif($qtype == "Essay") {
?>
			<div id="<?php echo $questionHashID; ?>" class="qlist" isview="0">
				<div class="showQuestion">
					<div class="qsettings">
						<div class="editq" title="Edit Question"></div>
						<div class="deleteq" title="Delete Question"></div>
					</div>

					<b>Question <?php echo $qNumber; ?>:</b> <?php echo $question; ?><br><br>
					<b>Question Type:</b> <?php echo $qtype; ?><br><br>
					<b>Minimum characters:</b> <?php echo $choices; ?> characters<br><br>
					<b>Points:</b> <?php echo $points; ?>
				</div>
				<div class="editQuestion" style="display: none">
					<form action="process.php?action=editquestion_essay" method="POST">
					<table class="form">
						<tr valign="top">
							<td width="95px">Question:</td>
							<td><textarea name="question" maxlength="500" style="resize: none; height: 100px;" required><?php echo $question; ?></textarea></td>
						</tr>
						<tr>
							<td>Minimum Characters:</td>
							<td>
							<input type="number" min="1" max="5000" name="chars" value="<?php echo $choices; ?>" placeholder="Default value: 100 characters" required>
							</td>
						</tr>
						<tr>
							<td>Points:</td>
							<td>
							<input type="number" min="1" max="100" name="points" value="<?php echo $row['Points']; ?>" placeholder="Default value: 1 point" required></td>
						</tr>
						<tr>
							<td colspan="2"><input type="submit" name="submit" value="Save"></td>
						</tr>
					</table>
					<input type="hidden" name="assessID" value="<?php echo $assessID; ?>">
					<input type="hidden" name="courseID" value="<?php echo $courseID; ?>">
					<input type="hidden" name="questionID" value="<?php echo $questionid; ?>">
					</form>
				</div>
			</div>
<?php
			}
			$qNumber++;
		}
?>
			<script>
			$(document).ready(function() {
				$('[type="number"]').keydown(function (e) {
					if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
						(e.keyCode == 65 && e.ctrlKey === true) || 
						(e.keyCode >= 35 && e.keyCode <= 39)) {
						return;
					}
					if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
						e.preventDefault();
					}
				});
				$('html').click(function() {
					$(".qlist").attr('isview', '0');
					$(".qlist").find(".showQuestion .qsettings").css({"display": "none"});
					$(".qlist").find(".showQuestion").css({"display": "block"});
					$(".qlist").find(".editQuestion").css({"display": "none"});
				});
				$(".qlist").click(function(event) {
					event.stopPropagation();
					$view = $(this).attr('isview');
					if($view == 0) {
						$(this).attr('isview', '1');
						$(this).css({
						});
						$(this).find(".showQuestion .qsettings").css({"display":"block"});
					}
					$(".qlist").not(this).attr('isview', '0');
					$(".qlist").not(this).find(".showQuestion .qsettings").css({"display": "none"});
					$(".qlist").not(this).find(".showQuestion").css({"display": "block"});
					$(".qlist").not(this).find(".editQuestion").css({"display": "none"});
				});
			});
			</script>
<?php
		if(mysql_num_rows($query) == 0) {
			echo '<center><p class="text"><i><br><br>There are no questions set.<br><br><br></i></p></center><hr>';
		}
	}




	// Get assessment overall points

	public function GetOverallPoints($assessID) {
		$points = 0;
		$query = mysql_query("SELECT SUM(Points) AS Points FROM Question WHERE AssessmentID = $assessID");
		while($row = mysql_fetch_array($query)) {
			$points = $row['Points'];
		}
		return $points;
	}




	// Get number of questions

	public function GetNumberOfQuestions($assessID) {
		$query = mysql_query("SELECT QuestionID FROM Question WHERE AssessmentID = $assessID");
		return mysql_num_rows($query);
	}
}
?>