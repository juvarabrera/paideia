<?php
$toRoot = "";
require_once('library/Config.php');
if($status == "Login") {
	header("Location: index.php");
}
$bTitle = "Schoolbook - De La Salle University Dasmari&ntilde;as";
if(isset($_GET['id']) && isset($_GET['show'])) {
	$show = $_GET['show'];
	$id = $_GET['id'];

	if($library['course']->CheckIfCourseExistWithCourseID($id)) {
		if($library['course']->IsEnrolled($loggedUser, $id)) {
			$isGuest = false;
		} else {
			$isGuest = true;
		}
		if($isGuest) {
			$pages = array('People');
			if(!in_array($show, $pages))
				header("Location: courses.php?id=$id");
		} else {
			$pages = array('Assessment', 'Resources', 'People', 'Calendar', 'Request', 'Gradesheet');
			// 0 - student
			// 1 - faculty
			// 2 - both
			$page_authorization = array(
				"Assessment" => 2,
				"Resources" => 2,
				"People" => 2,
				"Calendar" => 2,
				"Request" => 1,
				"Gradesheet" => 1
			);
			if($show == "Assessment") {
				if(isset($_GET['aid'])) {
					$aid = $_GET['aid'];
					if(!$library['assessment']->CheckAssessmentIDAndCourse($id, $aid)) {
						header("Location: courses.php?id=$id&show=Assessment");
					}
				}
			}
			if(in_array($show, $pages)) {
				if($page_authorization[$show] == 0) {
					if($userType != "Student")
						header("Location: courses.php?id=$id");
				} elseif ($page_authorization[$show] == 1) {
					if($userType != "Faculty")
						header("Location: courses.php?id=$id");
					if($library['course']->GetCourseType($id) != "Open" && $show == "Request")
						header("Location: courses.php?id=$id");
				} elseif ($page_authorization[$show] == 2) {
					//
				}
			} else {
				header("Location: courses.php?id=$id");
			}
		}
	} else {
		header("Location: courses.php");
	}
	$bTitle = $library['course']->GetCourseName($id);
} elseif(isset($_GET['id'])) {
	$id = $_GET['id'];
	if($library['course']->CheckIfCourseExistWithCourseID($id)) {
		if($library['course']->IsEnrolled($loggedUser, $id)) {
			$isGuest = false;
			if(isset($_GET['post']))  {
				$postID = $_GET['post'];
				if(!$library['course']->CheckIfCoursePost($id, $postID)) {
					header("Location: courses.php?id=$id");
				}
			}
		} else {
			$isGuest = true;
		}
	} else {
		header("Location: courses.php");
	}
	$bTitle = $library['course']->GetCourseName($id);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $bTitle; ?></title>
	<link rel="stylesheet" href="styles/skin/<?php echo $skin; ?>/style.php">
	<?php require_once('scripts/skin/'.$skin.'/scripts.php'); ?>
	<?php
	if($status == "LoggedIn") 
		if(file_exists("users/$loggedUser/cover.jpg"))
			echo '<style type="text/css">#bodyBg {background-image: url(users/'.$loggedUser.'/cover.jpg)}</style>';
	?>
</head>
<body onload="init();">
<div id="bodyBg"></div>
<div id="container">
	<div id="main">
		<?php 
		if(isset($_GET['id']) && isset($_GET['show'])) {
			$show = $_GET['show'];
			require_once($toRoot.'template/Courses/body_CoursesPage.php');
		} elseif(isset($_GET['id'])) {
			require_once($toRoot.'template/Courses/body_CoursesIndex.php');
		} else {
			require_once($toRoot.'template/Courses/body_'.$userType.'Courses.php');
		}
		?>
	</div>
</div>
<div id="top">
	<div class="base">
		<a href="index.php" class="logo"></a>
		<?php
		require_once('template/top_'.$status.'.php');
		?>
	</div>
</div>
<?php 
/*
if($userType == "Faculty") {
	require_once($toRoot.'template/Courses/popup_CreateCourse.php');
	require_once($toRoot.'template/Courses/popup_CreateAssessment.php');
	require_once($toRoot.'template/Courses/popup_EditCourse.php');
	require_once($toRoot.'template/Courses/popup_AddEvent.php');
	require_once($toRoot.'template/Courses/popup_AddFolder.php');
	require_once($toRoot.'template/Courses/popup_AddFile.php');
	require_once($toRoot.'template/Courses/popup_EnrollCourse.php');
	if(isset($aid)) {
		require_once($toRoot.'template/Courses/popup_CreateQuestion_TrueOrFalse.php');
		require_once($toRoot.'template/Courses/popup_CreateQuestion_MultipleChoice.php');
		require_once($toRoot.'template/Courses/popup_CreateQuestion_Essay.php');
	}
} 
else {
	require_once($toRoot.'template/Courses/popup_EnrollCourse.php');
}*/
require_once($toRoot.'popup.php'); 
if(isset($_GET['id']))
echo '<script>

$(function(){
	$("#txtSearch").val("'.$library['course']->GetCourseName($_GET['id']).'");
});

</script>';
?>
</body>
</html>