<?php
$toRoot = "";
require_once('library/Config.php');
if($status == "Login") {
	header("Location: index.php");
}
$bTitle = "Schoolbook - De La Salle University Dasmari&ntilde;as";

$searchQuery = "";
$categories = array("People", "Professors", "Students", "Courses", "Groups");

if(isset($_GET['q'])) {
	$searchQuery = $_GET['q'];
	if(isset($_GET['cat']) && $searchQuery != "") {
		$cat = $_GET['cat'];
		if(in_array($cat, $categories)) {
			if ($cat == "People") {
				$query = mysql_query("SELECT * FROM Account WHERE FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR CONCAT(FirstName, ' ', LastName) LIKE '%$searchQuery%'");
			} elseif ($cat == "Professors") {
				$query = mysql_query("SELECT * FROM Account WHERE Type = 'Faculty' AND (FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR CONCAT(FirstName, ' ', LastName) LIKE '%$searchQuery%')");
			} elseif ($cat == "Students") {
				$query = mysql_query("SELECT * FROM Account WHERE Type = 'Student' AND (FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR CONCAT(FirstName, ' ', LastName) LIKE '%$searchQuery%')");
			} elseif ($cat == "Courses") {
				$query = mysql_query("SELECT * FROM Course WHERE Name LIKE '%$searchQuery%'");
			} elseif ($cat == "Groups") {
				$query = mysql_query("SELECT * FROM GroupBlock WHERE Name LIKE '%$searchQuery%'");
			}
		}
	} else {
		$cat = "People";
		$query = mysql_query("SELECT * FROM Account WHERE FirstName LIKE '%$searchQuery%' OR LastName LIKE '%$searchQuery%' OR FirstName + ' ' + LastName LIKE '%$searchQuery%'");
	}
	if(isset($query)) {
		if(mysql_num_rows($query) == 0) {
			$nextkey = array_search($cat, $categories) + 1;
			if($nextkey == sizeof($categories)) 
				$nextkey = 0;
			if(!isset($_GET['stop'])) {
				if(isset($_GET['t'])) {
					$t = $_GET['t'];
					if($t < sizeof($categories)) {
						$t++;
						header("Location: search.php?q=$searchQuery&cat=".$categories[$nextkey]."&t=$t");
					} else {
						header("Location: search.php?q=$searchQuery&cat=".$categories[$nextkey]."&stop");
					}
				} else
					header("Location: search.php?q=$searchQuery&cat=".$categories[$nextkey]."&t=1");
			}
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $bTitle; ?></title>
	<link rel="stylesheet" href="styles/skin/<?php echo $skin; ?>/style.php">
	<?php require_once('scripts/skin/'.$skin.'/scripts.php'); ?>
	<?php
	if($status == "LoggedIn") 
		if(file_exists("users/$loggedUser/cover.jpg"))
			echo '<style type="text/css">#bodyBg {background-image: url(users/'.$loggedUser.'/cover.jpg)}</style>';
	?>
</head>
<body onload="init();">
<div id="bodyBg"></div>
<div id="container">
	<div id="main">
		<div id="sb1left">
			<div class="content">
				<table class="title"><tr>
					<td><h5>Filter</h5></td>
					<td></td>
				</tr></table>
				<hr>
				<ul class="contentlist">
					<?php 
					$catselected = $categories[0];
					if(isset($_GET['cat'])) {
						if(in_array($_GET['cat'], $categories))
							$catselected = $_GET['cat'];
					}
					for($i = 0; $i < sizeof($categories); $i++) {
						$selected = "";
						if($categories[$i] == $catselected)
							$selected = " class=\"selected\"";
						echo '<li><a href="search.php?q='.$searchQuery.'&cat='.$categories[$i].'&stop"'.$selected.'>'.$categories[$i].'</a></li>';
					}
					?>
				</ul>
			</div>
		</div>
		<div id="body">
		<?php
		if(isset($query)) {
		?>
			<div class="content">
				<table class="title"><tr>
					<td><h5>Search for '<?php echo $searchQuery; ?>'</h5></td>
					<td><?php $result = mysql_num_rows($query); echo $result; if($result == 1) echo ' result'; else echo ' results'; ?></td>
				</tr></table>
				<hr>
				<ul class="results">
					<?php 
					if ($cat == "People" || $cat == "Professors" || $cat == "Students") {
						while($row = mysql_fetch_array($query)) {
							$id = $row['ID'];
							$username = $row['Username'];
							$fullname = $row['FirstName'] . ' ' . $row['LastName'];
							$dp = $library['user']->GetProfilePicture($id,50);
							echo '
					<li><table class="results"><tr valign="top">
						<td><a href="index.php?-_-='.$username.'"><div style="background-image: url('.$dp.')" class="profilepicture"></div></a></td>
						<td><a href="index.php?-_-='.$username.'">'.$fullname.'</a><br><small>'.$username.'</small></td>
						<td class="button"><a href="index.php?-_-='.$username.'" class="inputbutton">View Profile</a></td>
					</tr></table></li>';
						}
						if(mysql_num_rows($query) == 0) {
							echo '<li><table class="results"><tr valign="top">
						<td><center>No results</td>
					</tr></table></li>';
						}




					} elseif ($cat == "Courses") {
						while($row = mysql_fetch_array($query)) {
							$id = $row['CourseID'];
							$name = $row['Name'];
							$desc = $row['Description'];
							if($desc == "")
								$desc = "<i>No description available.</i>";
							if(strlen($desc) >= 40)
								$desc = substr($desc, 0, 39).'...';
							$dp = $library['course']->GetProfilePicture($id, 50);
							echo '
					<li><table class="results"><tr valign="top">
						<td><a href="courses.php?id='.$id.'"><div style="background-image: url('.$dp.')" class="profilepicture"></div></a></td>
						<td><a href="courses.php?id='.$id.'">'.$name.'</a><br><small>'.$desc.'</small></td>
						<td class="button"><a href="courses.php?id='.$id.'" class="inputbutton">View Course</a></td>
					</tr></table></li>';
						}
						if(mysql_num_rows($query) == 0) {
							echo '<li><table class="results"><tr valign="top">
						<td><center>No results</td>
					</tr></table></li>';
						}
					} elseif ($cat == "Groups") {
						while($row = mysql_fetch_array($query)) {
							$id = $row['GroupID'];
							$name = $row['Name'];
							$desc = $row['Description'];
							if($desc == "")
								$desc = "<i>No description available.</i>";
							if(strlen($desc) >= 40)
								$desc = substr($desc, 0, 39).'...';
							$dp = $library['group']->GetProfilePicture($id, 50);
							echo '
					<li><table class="results"><tr valign="top">
						<td><a href="groups.php?id='.$id.'"><div style="background-image: url('.$dp.')" class="profilepicture"></div></a></td>
						<td><a href="groups.php?id='.$id.'">'.$name.'</a><br><small>'.$desc.'</small></td>
						<td class="button"><a href="groups.php?id='.$id.'" class="inputbutton">View Group</a></td>
					</tr></table></li>';
						}
						if(mysql_num_rows($query) == 0) {
							echo '<li><table class="results"><tr valign="top">
						<td><center>No results</td>
					</tr></table></li>';
						}
					}
					?>
				</ul>
			</div>
		<?php
		} else {
		?>
			<div class="content">
				<table class="title">
					<tr>
						<td><h5>Search</h5></td>
						<td></td>
					</tr>
				</table>
				<p class="text">
					Search people, courses, and groups. Use the textfield above.
				</p>
			</div>
		<?php
		}
		?>
		</div>
		<div id="sb2">
			<div class="content">
				<table class="title"><tr>
					<td><h5>People you may know</h5></td>
					<td></td>
				</tr></table>
			</div>
		</div>
	</div>
</div>
<div id="top">
	<div class="base">
		<a href="index.php" class="logo"></a>
		<?php
		require_once('template/top_'.$status.'.php');
		?>
	</div>
</div>
</body>
</html>
<?php
if(isset($searchQuery))
echo '<script>

$(function(){
	$("#txtSearch").val("'.$searchQuery.'");
});

</script>';
?>